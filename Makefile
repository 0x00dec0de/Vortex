.PHONY: makemigrations migrate build rebuild-and-restart shell bash test run-for-loadtest run deploy pull

migrations:
	docker compose run kapibara-monolith ./manage.py makemigrations

migrate:
	docker compose exec kapibara-monolith ./manage.py migrate

build:
	docker compose build --no-cache

rebuild-and-restart:
	docker compose down --rmi all && docker compose up -d --force-recreate

shell:
	docker compose exec kapibara-monolith ./manage.py shell_plus

bash:
	docker compose exec kapibara-monolith bash

test:
	docker compose run kapibara-monolith pytest -n auto

run-for-loadtest:
	docker compose -f docker-compose.yml -f docker-compose.loadtest.yml up

run:
	docker compose up

pull:
	git reset --hard && git fetch origin && git checkout main && git pull

deploy: pull rebuild-and-restart migrate
