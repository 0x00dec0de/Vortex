from contextlib import contextmanager
from contextvars import ContextVar
from typing import Any, Iterator

from pydantic import BaseModel

_init_blocks_context_var = ContextVar("_init_blocks_context_var", default=None)


@contextmanager
def init_blocks_context(value: dict[str, Any]) -> Iterator[None]:
    token = _init_blocks_context_var.set(value)
    try:
        yield
    finally:
        _init_blocks_context_var.reset(token)


class ContentBaseModel(BaseModel):
    def __init__(self, /, **data: Any) -> None:
        self.__pydantic_validator__.validate_python(
            data,
            self_instance=self,
            context=_init_blocks_context_var.get() or {},
        )
