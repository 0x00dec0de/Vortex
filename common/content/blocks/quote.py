from typing import Literal

from django.conf import settings
from pydantic import Field, field_validator
from pydantic_core.core_schema import ValidationInfo

from common.content.blocks.context import ContentBaseModel
from common.helpers import sanitize_html_content


class BlockQuoteData(ContentBaseModel):
    text: str = Field(..., max_length=settings.CONTENT_BLOCK_MAX_LENGTH_BYTES)
    caption: str = Field("", max_length=settings.CONTENT_CAPTION_MAX_LENGTH_BYTES)
    alignment: Literal["left", "right", "center", "justify"] = "left"

    @field_validator("text", "caption")
    @classmethod
    def check_value(cls, value: str, info: ValidationInfo) -> str:
        if info.context.get("sanitize", True):
            return sanitize_html_content(value)
        return value


class BlockQuote(ContentBaseModel):
    id_: str = Field(None, alias="id")
    type_: Literal["quote"] = Field("quote", alias="type")
    data: BlockQuoteData
