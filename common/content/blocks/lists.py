from typing import Literal

from django.conf import settings
from pydantic import Field, conlist, constr, field_validator
from pydantic_core.core_schema import ValidationInfo

from common.content.blocks.context import ContentBaseModel
from common.helpers import sanitize_html_content


class BlockListData(ContentBaseModel):
    style: Literal["ordered", "unordered"]
    items: conlist(
        constr(max_length=settings.CONTENT_LIST_ITEM_MAX_LENGTH_BYTES),
        max_length=settings.CONTENT_MAX_LIST_ITEMS,
    )

    @field_validator("items")
    @classmethod
    def check_value(cls, value: list[str], info: ValidationInfo) -> list[str]:
        if info.context.get("sanitize", True):
            return [sanitize_html_content(item) for item in value]
        return value


class BlockList(ContentBaseModel):
    id_: str = Field(None, alias="id")
    type_: Literal["list"] = Field("list", alias="type")
    data: BlockListData
