from typing import Literal

from django.conf import settings
from pydantic import Field, field_validator
from pydantic_core.core_schema import ValidationInfo

from common.content.blocks.context import ContentBaseModel
from common.helpers import sanitize_html_content


class BlockVideoData(ContentBaseModel):
    service: Literal[
        "youtube",
        "coub",
        "vimeo",
        "vk",
        "rutube",
        "ok",
        "yappi",
        "telegram",
        "instagram",
        "facebook",
        "test",
    ]

    source: str = Field(..., max_length=200)
    embed: str = Field(..., max_length=200)
    width: int = 580
    height: int = 320
    caption: str = Field("", max_length=settings.CONTENT_CAPTION_MAX_LENGTH_BYTES)

    @field_validator("caption")
    @classmethod
    def check_value(cls, value: str, info: ValidationInfo) -> str:
        if info.context.get("sanitize", True):
            return sanitize_html_content(value)
        return value


class BlockVideo(ContentBaseModel):
    id_: str = Field(None, alias="id")
    type_: Literal["embed"] = Field("embed", alias="type")
    data: BlockVideoData
