from typing import Literal

from django.conf import settings
from pydantic import Field, field_validator
from pydantic_core.core_schema import ValidationInfo

from common.content.blocks.context import ContentBaseModel
from common.helpers import sanitize_html_content


class BlockParagraphData(ContentBaseModel):
    text: str = Field(..., max_length=settings.CONTENT_BLOCK_MAX_LENGTH_BYTES)

    @field_validator("text")
    @classmethod
    def check_value(cls, value: str, info: ValidationInfo) -> str:
        checked_value = value
        if info.context.get("sanitize", True):
            checked_value = sanitize_html_content(value)
        return checked_value


class BlockParagraph(ContentBaseModel):
    id_: str = Field(None, alias="id")
    type_: Literal["paragraph"] = Field("paragraph", alias="type")
    data: BlockParagraphData
