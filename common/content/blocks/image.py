from typing import Literal

from django.conf import settings
from pydantic import Field, field_validator
from pydantic_core import PydanticCustomError
from pydantic_core.core_schema import ValidationInfo

from common.content.blocks.context import ContentBaseModel
from common.helpers import is_url_from_media_storage, sanitize_html_content


class BlockImageDataFile(ContentBaseModel):
    url: str = Field(..., max_length=150)

    @field_validator("url")
    def check_valid_url(cls, value):
        if not is_url_from_media_storage(value):
            raise PydanticCustomError(
                "wrong_file_url", "Нельзя загружать файлы с таким URL"
            )
        return value


class BlockImageData(ContentBaseModel):
    file_: BlockImageDataFile = Field(..., alias="file")
    caption: str = Field("", max_length=settings.CONTENT_CAPTION_MAX_LENGTH_BYTES)
    with_border: bool = Field(False, alias="withBorder")
    stretched: bool = False
    with_background: bool = Field(False, alias="withBackground")

    @field_validator("caption")
    @classmethod
    def check_value(cls, value: str, info: ValidationInfo) -> str:
        if info.context.get("sanitize", True):
            return sanitize_html_content(value)
        return value


class BlockImage(ContentBaseModel):
    id_: str = Field(None, alias="id")
    type_: Literal["image"] = Field("image", alias="type")
    data: BlockImageData
