import uuid

from django.conf import settings
from drf_spectacular.utils import OpenApiParameter

INTERNAL_TOKEN = OpenApiParameter(
    name=settings.INTERNAL_TOKEN_HEADER,
    type=str,
    required=True,
    location=OpenApiParameter.HEADER,
    description="Токен для авторизации запросов приходящих из других сервисов.",
)

POST_SLUG = OpenApiParameter(
    name="post",
    type=str,
    required=True,
    location=OpenApiParameter.QUERY,
    description="Слаг поста для которого нужно вывести комментарии.",
)
PARENT_COMMENT_UUID = OpenApiParameter(
    name="parent",
    type=uuid.UUID,
    required=False,
    location=OpenApiParameter.QUERY,
    description="UUID комментария к которому нужно вывести дерево комментариев.",
)

PART_PAGE_URL = OpenApiParameter(
    name="uri",
    type=str,
    required=True,
    location=OpenApiParameter.QUERY,
    description="Часть урла после kapi.bar для превьюшки. Начиная со слеша(/).",
)
IS_JSON = OpenApiParameter(
    name="is_json",
    type=bool,
    required=False,
    location=OpenApiParameter.QUERY,
    default=True,
    description="True- если в ответе нужен json, False - если html.",
)
EMAIL = OpenApiParameter(
    name="email",
    type=str,
    required=True,
    location=OpenApiParameter.QUERY,
    # default=True,
    description="Email пользователя для отписки от рассылок.",
)
