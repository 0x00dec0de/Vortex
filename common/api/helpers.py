import re
from urllib.parse import unquote, urlparse

from django.conf import settings

from posts.models import Post
from posts.selectors import get_user_restricted_tag_names
from uploads.models import Image
from users.choices import UserGender
from users.models import UserPublic


def get_formatted_error_response(code: str, message: str):
    return {"detail": [{"msg": message.strip(), "type": code.strip()}]}


def remove_html_a_tags(text: str) -> str:
    """Удаляет все теги, включая ссылки, а также служебные теги."""
    # Удаляет все HTML-теги
    clean_text = re.sub(r"<.*?>", " ", text)
    return clean_text


def find_text_in_content(blocks: list[dict]) -> str:
    """Формирует description-метатег для страницы поста.
    Удаляет ссылки и служебные теги."""

    text_for_desc = ""
    for block in blocks:
        if len(text_for_desc) >= settings.DEFAULT_MAX_LENGTH_OG_DESCRIPTIONS:
            text_for_desc = (
                text_for_desc[: settings.DEFAULT_MAX_LENGTH_OG_DESCRIPTIONS - 3] + "..."
            )
            break
        elif block.get("type") == "paragraph":
            text_block = block.get("data", {}).get("text", "")
            text_for_desc += remove_html_a_tags(text_block)
        elif block.get("type") == "image":
            break
    if len(text_for_desc) >= settings.DEFAULT_MAX_LENGTH_OG_DESCRIPTIONS:
        text_for_desc = (
            text_for_desc[: settings.DEFAULT_MAX_LENGTH_OG_DESCRIPTIONS - 3] + "..."
        )
    return text_for_desc


def find_image_in_content(blocks: list[dict]) -> str:
    """Ищет изображение для OG-метатега."""

    url_image = settings.DEFAULT_PATH_IMAGE
    for block in blocks:
        if block.get("type") == "image":
            url_image = (
                block.get("data", {})
                .get("file", {})
                .get("url", settings.DEFAULT_PATH_IMAGE)
            )
            break
    return url_image


def find_image_size_og_image_width(image_url: str) -> Image:
    """
    Возвращает объект изображения из БД по URL.
    """
    part_names = image_url.split("uploads/")
    if part_names != 2:
        return None
    image_obj = Image.objects.filter(image=part_names[1]).first()
    return image_obj or None


def get_post_meta(post: Post) -> dict[str, str]:
    """Заполняет метатеги для страниц постов."""

    content_blocks = post.content.get("blocks")
    # og_descr = find_text_in_content(content_blocks)
    og_image = find_image_in_content(content_blocks)
    image_info = find_image_size_og_image_width(og_image) if og_image else None
    description = (
        f"{post.title} — пост "
        f"{'Капибарышни' if post.user.gender == UserGender.FEMALE else 'Капибарина'} "
        f"{post.user.username}. Комментариев - {post.comments_count}, "
        f"сохранений - {post.get_count_saved()}. "
        f"Что вы думаете об этом посте?"
    )
    data = {
        "title": f"{post.title} | Kapi.bar",
        "description": description[: settings.DEFAULT_MAX_LENGTH_DESCRIPTIONS - 3]
        + "...",
        "keywords": f"{post.title}, {', '.join(post.get_tag_names())}"[:199],
        "h1": post.title,
        "canonical": post.get_absolute_url(),
        "og_title": f"{post.user.username}: {post.title}",
        # "og_description": og_descr,
        "og_description": description[: settings.DEFAULT_MAX_LENGTH_OG_DESCRIPTIONS - 3]
        + "...",
        "twitter_description": description[
            : settings.DEFAULT_MAX_LENGTH_OG_DESCRIPTIONS - 3
        ]
        + "...",
        "og_image": og_image,
        "twitter_image": og_image,
        "og_type": "article",
        "og_url": post.get_absolute_url(),
        "twitter_url": post.get_absolute_url(),
        "og_article_published_time": post.created_at,
        "og_article_author": post.user.get_absolute_url(),
        "og_article_publisher": post.user.get_absolute_url(),
        "og_article_tag": post.get_tag_names(),
        "twitter_title": f"{post.title} : '| Kapi.Bar'",
        "og_image_alt": f"{post.title[:84]} : '| Kapi.Bar'",
        "og_image_width": image_info.width if image_info else None,
        "og_image_height": image_info.height if image_info else None,
        "post_comments": f"{post.comments_count}",
        "post_saves": f"{post.post.get_count_saved()}",
    }
    return data


def get_user_meta_data(user: UserPublic) -> dict[str, str]:
    """Заполняет метатеги для страниц профилей пользователей."""

    if not user:
        return {}
    title = str(f"{user.username} | Kapi.bar")
    description = str(
        f"{user.get_post_count()} постов, "
        f"{user.get_comment_count()} комментариев, "
        f"{user.get_subscription_count()} подписчиков. "
        f"{'Капибарышня' if user.gender == UserGender.FEMALE else 'Капибарин'} "
        f"c {user.created_at.date()}."
        f"{user.bio[:82] if user.bio else ''} "
    )
    image_info = find_image_size_og_image_width(
        user.avatar or settings.DEFAULT_PATH_IMAGE
    )

    data = {
        "title": title,
        "description": description,
        "canonical": user.get_absolute_url(),
        "og_title": f"Профиль {user.username} на kapi.bar",
        "og_description": description,
        "twitter_description": description,
        "og_image_alt": description[:100],
        "og_image": user.avatar or settings.DEFAULT_PATH_IMAGE,
        "twitter_image": user.avatar or settings.DEFAULT_PATH_IMAGE,
        "og_type": "profile",
        "og_url": user.get_absolute_url(),
        "twitter_url": user.get_absolute_url(),
        "og_profile_last_name": str(
            f"{'Капибарышня' if user.gender == UserGender.FEMALE else 'Капибарин'}"
        ),
        "og_profile_first_name": user.username,
        "og_profile_username": user.username,
        "og_profile_gender": str(
            f"{'female' if user.gender == UserGender.FEMALE else 'male'}"
        ),
        "og_article_author": user.get_absolute_url(),
        "og_article_publisher": user.get_absolute_url(),
        "twitter_title": f"{user.username} : '| Kapi.Bar'",
        "og_image_width": image_info.width if image_info else None,
        "og_image_height": image_info.height if image_info else None,
    }
    return data


def get_meta_data_feeds(uri: str) -> dict:
    """Заполняет метатеги для лент."""

    mapping = {
        "/new": settings.DEFAULT_DESCRIPTION_NEW,
        "/top": settings.DEFAULT_DESCRIPTION_TOP,
        "/discussed": settings.DEFAULT_DESCRIPTION_DISCUSSED,
        "/copyright": settings.DEFAULT_DESCRIPTION_DISCUSSED,
    }
    for feet_type, descr in mapping.items():
        if uri.startswith(feet_type):
            return {
                "title": descr,
                "description": descr,
                "og_description": descr,
                "twitter_description": descr,
                "og_url": f"{settings.FRONTEND_BASE_DOMAIN}{uri}",
                "twitter_url": f"{settings.FRONTEND_BASE_DOMAIN}{uri}",
                "canonical": f"{settings.FRONTEND_BASE_DOMAIN}{uri}",
            }
    return {}


def get_tags_meta_data(uri: str) -> dict:
    """Заполняет метатеги для страниц поиска по тегам."""
    parsed_url = urlparse(f"{settings.FRONTEND_BASE_DOMAIN}{uri}")
    decoded_value = unquote(parsed_url.query)
    tags = decoded_value.split("selectedTags=")[-1].split(",")
    if not tags:
        description = settings.DEFAULT_DESCRIPTION_TAGS.replace(
            "по тегам", f"по тегам {','.join(tags)}"
        )
        return {
            "title": description,
            "description": description,
            "og_description": description,
            "twitter_description": description,
            "og_url": f"{settings.FRONTEND_BASE_DOMAIN}{uri}",
            "twitter_url": f"{settings.FRONTEND_BASE_DOMAIN}{uri}",
        }
    return {
        "description": settings.DEFAULT_DESCRIPTION_TAGS,
        "og_description": settings.DEFAULT_DESCRIPTION_TAGS,
        "twitter_description": settings.DEFAULT_DESCRIPTION_TAGS,
        "og_url": f"{settings.FRONTEND_BASE_DOMAIN}{uri}",
        "twitter_url": f"{settings.FRONTEND_BASE_DOMAIN}{uri}",
        "twitter_title": f"{','.join(tags)} : '| Kapi.Bar'",
    }


def is_restricted_post(post: Post) -> bool:
    """Проверяет общедоступный ли это пост по тегам."""
    return post.tags.filter(name__in=get_user_restricted_tag_names()).exists()


def get_meta_data(uri: str) -> dict:
    """Заполняет метатеги для страниц страниц: профиль, пост, ленты, теги."""
    if uri.startswith("/post/"):
        post_slug = uri.split("/")[-1]
        post = Post.objects.prefetch_related("tags").filter(slug=post_slug).first()
        return get_post_meta(post) if not is_restricted_post(post) else {}
    elif uri.startswith("/profile/"):
        username = uri.split("/")[-1]
        user = UserPublic.objects.filter(username__iexact=username).first()
        return get_user_meta_data(user)

    elif uri.startswith(("/new", "/top", "/discussed", "/copyright")):
        return get_meta_data_feeds(uri)
    elif uri.startswith("/tags"):
        return get_tags_meta_data(uri)
    return {}
