from django.conf import settings
from rest_framework import serializers


class MetaTagSerializer(serializers.Serializer):
    title = serializers.CharField(
        max_length=200, default=settings.DEFAULT_TITLE, required=False, allow_blank=True
    )
    description = serializers.CharField(
        max_length=200, default=settings.DEFAULT_TITLE, required=False, allow_blank=True
    )
    keywords = serializers.CharField(
        max_length=200,
        default=settings.DEFAULT_KEYWORDS,
        required=False,
        allow_blank=True,
    )
    h1 = serializers.CharField(
        max_length=200, default=settings.DEFAULT_H1, required=False, allow_blank=True
    )
    canonical = serializers.CharField(max_length=200, required=False, allow_blank=True)
    og_title = serializers.CharField(
        max_length=200, default=settings.DEFAULT_TITLE, required=False, allow_blank=True
    )
    og_description = serializers.CharField(
        max_length=settings.DEFAULT_MAX_LENGTH_OG_DESCRIPTIONS,
        default=settings.DEFAULT_DESCRIPTION,
        required=False,
        allow_blank=True,
    )
    og_image = serializers.URLField(default=settings.DEFAULT_PATH_IMAGE, required=False)
    og_type = serializers.CharField(
        max_length=50, default="website", required=False, allow_blank=True
    )
    og_site_name = serializers.CharField(
        max_length=100,
        default=settings.FRONTEND_BASE_DOMAIN,
        required=False,
        allow_blank=True,
    )
    og_locale = serializers.CharField(
        max_length=100,
        default="ru-RU",
        required=False,
        allow_blank=True,
    )
    og_locale_alternate = serializers.CharField(
        max_length=100,
        default="en-US",
        required=False,
        allow_blank=True,
    )
    og_url = serializers.CharField(
        max_length=100,
        default=settings.FRONTEND_BASE_DOMAIN,
        required=False,
        allow_blank=True,
    )
    og_article_published_time = serializers.DateTimeField(
        required=False, default=None, allow_null=True
    )
    og_article_author = serializers.CharField(
        max_length=100, required=False, default="", allow_blank=True, allow_null=True
    )
    og_article_publisher = serializers.CharField(
        max_length=100, required=False, default="", allow_blank=True, allow_null=True
    )
    og_profile_first_name = serializers.CharField(
        max_length=100, required=False, default="", allow_blank=True, allow_null=True
    )
    og_profile_last_name = serializers.CharField(
        max_length=100, required=False, default="", allow_blank=True, allow_null=True
    )
    og_profile_username = serializers.CharField(
        max_length=100, required=False, default="", allow_blank=True, allow_null=True
    )
    og_profile_gender = serializers.CharField(
        max_length=100, default="", required=False, allow_blank=True, allow_null=True
    )
    og_image_type = serializers.CharField(
        max_length=100,
        default="image/webp",
        required=False,
        allow_blank=True,
        allow_null=True,
    )
    twitter_card = serializers.CharField(
        max_length=100,
        default="summary_large_image",
        required=False,
        allow_blank=True,
        allow_null=True,
    )
    twitter_site = serializers.CharField(
        max_length=100,
        default="@My_Kapi_Bar",
        required=False,
        allow_blank=True,
        allow_null=True,
    )
    twitter_title = serializers.CharField(
        max_length=100, default="", required=False, allow_blank=True, allow_null=True
    )
    twitter_url = serializers.CharField(
        max_length=100, default="", required=False, allow_blank=True, allow_null=True
    )
    twitter_domain = serializers.CharField(
        max_length=100,
        default="kapi.bar",
        required=False,
        allow_blank=True,
        allow_null=True,
    )
    twitter_description = serializers.CharField(
        max_length=settings.DEFAULT_MAX_LENGTH_OG_DESCRIPTIONS,
        default=settings.DEFAULT_DESCRIPTION,
        required=False,
        allow_blank=True,
        allow_null=True,
    )
    og_image_width = serializers.CharField(
        max_length=100, default="", required=False, allow_blank=True, allow_null=True
    )
    og_image_height = serializers.CharField(
        max_length=100, default="", required=False, allow_blank=True, allow_null=True
    )
    og_image_alt = serializers.CharField(
        max_length=100, default="", required=False, allow_blank=True, allow_null=True
    )
    post_comments = serializers.CharField(
        max_length=100, default="", required=False, allow_blank=True, allow_null=True
    )
    post_saves = serializers.CharField(
        max_length=100, default="", required=False, allow_blank=True, allow_null=True
    )
    robots = serializers.CharField(
        max_length=100,
        default="max-image-preview:large",
        required=False,
        allow_blank=True,
        allow_null=True,
    )
    twitter_image = serializers.URLField(
        default=settings.DEFAULT_PATH_IMAGE, required=False
    )

    def to_internal_value(self, data):
        data = super().to_internal_value(data)
        for field_name in self.fields:
            if field_name not in data or data[field_name] == "":
                data[field_name] = self.fields[field_name].default
        return data
