import re

import nh3
from django.conf import settings
from slugify import CYRILLIC, Slugify
from urlextract import URLExtract

from common.selectors import get_internal_token_from_request, get_valid_internal_tokens

COMMENT_REGEX = re.compile(
    rf"^({settings.FRONTEND_BASE_DOMAIN}/post/[^\s?#]+)\?comment=([^\s#]+)$"
)
POST_REGEX = re.compile(rf"^({settings.FRONTEND_BASE_DOMAIN}/post/[^\s?#]+)$")
PROFILE_REGEX = re.compile(rf"^({settings.FRONTEND_BASE_DOMAIN}/profile/[^\s?#]+)$")


def slugify_function(text: str) -> str:
    """Slugify text, also works with cyrillic letters."""
    slugify = Slugify(pretranslate=CYRILLIC)
    return slugify(text).lower()


def is_prod() -> bool:
    """Check if running in production environment."""
    return settings.ENVIRONMENT == "production"


def is_request_signed_with_valid_internal_token(request) -> bool:
    """Check if request has valid internal token in headers."""
    token = get_internal_token_from_request(request)
    return token in get_valid_internal_tokens()


def kapi_replacement(url: str) -> str:
    """
    Возвращает HTML-тег <a> для URL, ведущего на сайт kapi.bar,
    с учетом специфических путей сайта.

    Parameters:
    url (str): Найденный URL, который нужно преобразовать.

    Returns:
    str: Строка с HTML-тегом <a> для соответствующего URL.
    """
    from posts.models import Post

    # Проверка и обработка URL на комментарий
    comment_match = COMMENT_REGEX.search(url)
    if comment_match:
        post_url, comment_id = comment_match.groups()
        new_url = f'<a href="{url}">comment#' f"{comment_id[:6]}</a>"
        return new_url

    # Проверка и обработка URL на пост
    post_match = POST_REGEX.search(url)

    if post_match:
        post_id = post_match.group(1).split("/")[-1]
        post = Post.objects.filter(slug=post_id).first()
        return f'<a href="{url}">Пост: {post.title if post else post_id}</a>'

    # Проверка и обработка URL на профиль
    profile_match = PROFILE_REGEX.search(url)
    if profile_match:
        profile_id = profile_match.group(1).split("/")[-1]
        # TODO: вызов метода отправки нотификации
        new_url = f'<a href="{url}" target="_blank">@{profile_id}</a>'
        return new_url
    # Обработка всех остальных URL
    return f'<a href="{url}">{url}</a>'


def replace_non_anchor_urls(text: str) -> str:
    """
    Заменяет все URL-адреса в предоставленном тексте на кликабельные HTML-ссылки,
    предполагая, что текст не содержит тегов <a>.

    Parameters:
    text (str): Текст для обработки.

    Returns:
    str: Текст с замененными на HTML-ссылки URL.
    """
    extractor = URLExtract()
    urls = extractor.find_urls(text)
    for url in urls:
        text = re.sub(
            re.escape(url),
            lambda match: kapi_replacement(match.group(0)),
            text,
            flags=re.IGNORECASE,
        )
    return text


def replace_urls(text: str) -> str:
    """
    Обрабатывает и заменяет URL в предоставленном тексте на кликабельные HTML-ссылки,
    игнорируя URL, которые уже являются частью тега <a>.

    Parameters:
    text (str): Текст для обработки.

    Returns:
    str: Текст с замененными на HTML-ссылки URL.
    """
    # Разделяем текст на части с тегами <a> и без
    parts = re.split(r"(<a .*?>.*?</a>)", text)

    # Применяем функцию замены URL только к тем частям, которые не содержат тег <a>
    for i in range(len(parts)):
        if not re.match(r"<a .*?>.*?</a>", parts[i]):
            parts[i] = replace_non_anchor_urls(parts[i])

    # Собираем обратно весь текст
    return "".join(parts)


def sanitize_html_content(html):
    html_with_links = replace_urls(html)
    sanitized_html = nh3.clean(
        html_with_links,
        tags=settings.ALLOWED_TAGS_IN_HTML_CONTENT,
        link_rel="noopener noreferrer nofollow",
        url_schemes={"http", "https"},
        set_tag_attribute_values={
            "a": {
                "target": "_blank",
            }
        },
    )
    return sanitized_html


def is_url_from_media_storage(
    url: str, allowed_extensions: list[str] | None = None
) -> bool:
    """Make sure that URL is in our media storage and have allowed extensions too."""
    if not url.startswith(
        f"{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/"
    ) and not url.startswith(f"https://{settings.CDN_BASE_DOMAIN}"):
        return False
    if allowed_extensions:
        _, extension = url.rsplit(".", 1)
        return extension and extension.lower() in allowed_extensions
    return True
