import uuid

from django.db import models

from common.constants import SiteMapUrlCategories, TaskStatus


class Timestamped(models.Model):
    """Abstract model to be inherited by model classes to have times or create/update."""

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class SitemapTask(models.Model):
    task_uid = models.UUIDField(default=uuid.uuid4, unique=True)
    status = models.CharField(
        max_length=50, choices=TaskStatus.choices, default=TaskStatus.INITIAL
    )
    feeds = models.BooleanField(default=True)
    profiles = models.BooleanField(default=True)
    posts = models.BooleanField(default=True)
    tags = models.BooleanField(default=True)
    all_time = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    finished_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Sitemap Task"
        verbose_name_plural = "Sitemap Tasks"


class SitemapEntry(models.Model):
    url = models.URLField(unique=True)
    category = models.CharField(max_length=20, choices=SiteMapUrlCategories.choices)
    last_visited = models.DateTimeField(auto_now=True)
    task_uid = models.CharField(max_length=255, null=True)
    exist_data = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Sitemap Entry"
        verbose_name_plural = "Sitemap Entries"
        constraints = [
            models.UniqueConstraint(
                fields=["url"], name="unique_url_constraint"
            ),  # Уникальный констрейнт для URL
        ]
