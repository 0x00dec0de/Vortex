from django.db.models import TextChoices


class SiteMapUrlCategories(TextChoices):
    COMMON = "common", "Общие"
    USER = "user", "Пользователь"
    POST = "post", "Пост"
    FEED = "feed", "Лента"
    TAG = "tag", "Тэг"
    OTHER = "other", "Другое"


class TaskStatus(TextChoices):
    INITIAL = "initial", "Создана"
    RUNNING = "running", "Выполняется"
    COMPLETED = "completed", "Завершена"
    ERROR = "error", "Ошибка"


class FeedsURL(TextChoices):
    NEW = "new", "Новое"
    TRENDS = "trends", "Тренды"
    TOP = "top", "Топ"
    DISCUSSED = "discussed", "Обсуждаемое"
    COPYRIGHT = "copyright", "Авторское"


SiteMapMaxEntries = 10000
