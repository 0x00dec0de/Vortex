import pydantic
from pydantic_core import ErrorDetails

CUSTOM_MESSAGES = {
    "string_too_long": "Длина текста должна быть не более {max_length} символов",
    "list_type": "Неверный формат данных, ожидается список.",
    "too_short": (
        "Число элементов в списке должно быть как минимум {min_length}, "
        "а не {actual_length}"
    ),
    "too_long": (
        "Число элементов в списке должно быть не более {max_length}, а не {actual_length}"
    ),
    "less_than_equal": "Значение должно быть не более {le}",
}


def convert_errors(
    pydantic_error: pydantic.ValidationError,
    include_url: bool = True,
    include_input: bool = True,
    custom_messages: dict[str, str] | None = None,
) -> list[ErrorDetails]:
    custom_messages = custom_messages or CUSTOM_MESSAGES
    new_errors: list[ErrorDetails] = []
    for error in pydantic_error.errors(
        include_url=include_url, include_input=include_input
    ):
        custom_message = custom_messages.get(error["type"])
        if custom_message:
            ctx = error.get("ctx")
            error["msg"] = custom_message.format(**ctx) if ctx else custom_message
        new_errors.append(error)
    return new_errors
