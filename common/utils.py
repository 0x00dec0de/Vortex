import string

from django.utils.crypto import get_random_string


def generate_random_string(length: int = 6) -> str:
    return get_random_string(6, allowed_chars=string.digits + string.ascii_lowercase)
