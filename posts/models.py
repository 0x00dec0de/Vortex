import uuid

import reversion
from django.conf import settings
from django.db import models
from django.db.models import UniqueConstraint
from django.utils import timezone

from common.helpers import slugify_function
from common.models import Timestamped
from posts.choices import PostStatus, PostVoteChoice
from posts.fields import KapibaraPostAutoSlugField
from posts.managers import PostsManager, PublishedPostsManager


@reversion.register(ignore_duplicates=True, exclude=("updated_at",))
class Post(Timestamped):
    """Model represents Post entity."""

    uuid = models.UUIDField(
        default=uuid.uuid4, unique=True, db_index=True, editable=False
    )
    user = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="posts"
    )
    title = models.CharField(max_length=200, default="")
    slug = KapibaraPostAutoSlugField(
        populate_from=["title"],
        null=True,
        default=None,
        editable=True,
        slugify_function=slugify_function,
        allow_duplicates=False,
        db_index=True,
        overwrite=True,
        unique=True,
        max_unique_query_attempts=10,
    )
    content = models.JSONField(default=dict)
    tags = models.ManyToManyField("tags.Tag", blank=True)
    views_count = models.IntegerField(default=0)
    comments_count = models.PositiveIntegerField(default=0)
    votes_up_count = models.PositiveIntegerField(default=0)
    votes_down_count = models.PositiveIntegerField(default=0)
    rating = models.IntegerField(default=0)
    status = models.CharField(
        max_length=9, choices=PostStatus.choices, default=PostStatus.DRAFT
    )

    trending_date = models.DateTimeField(null=True, blank=True)
    top_date = models.DateTimeField(null=True, blank=True)

    published_at = models.DateTimeField(null=True, blank=True)

    objects = PostsManager()
    published = PublishedPostsManager()
    is_anonym = models.BooleanField(default=False)
    is_answer = models.BooleanField(default=False)
    no_vote = models.BooleanField(default=False)
    no_comments = models.BooleanField(default=False)
    # series = models.ForeignKey(
    #     "posts.PostSeries",
    #     on_delete=models.SET_NULL,
    #     related_name="posts",
    #     null=True,
    #     blank=True,
    #     verbose_name="Серия",
    #     help_text="Серия, к которой принадлежит пост",
    # )

    class Meta:
        verbose_name = "Пост"
        verbose_name_plural = "Посты"
        indexes = [
            models.Index(fields=["user", "status"]),
            models.Index(fields=["published_at", "status"]),
        ]

    def __str__(self):
        return f"<{self.pk}: {self.title}>"

    def get_absolute_url(self):
        return f"{settings.FRONTEND_BASE_DOMAIN}/post/{self.slug}"

    def get_count_saved(self):
        """Возвращает количество сохранённых постов."""
        return self.saved.count()

    def get_tag_names(self):
        """Возвращает список имен тегов поста в формате [
        "Своё", "Авторское", "Шарлотка"]."""
        return [tag.name for tag in self.tags.all()]

    def get_post_series(self):
        """Возвращает информацию о серии, к которой относится пост."""
        membership = (
            PostSeriesMembership.objects.filter(post_id=self.pk)
            .select_related("series")
            .first()
        )
        if membership:
            series = membership.series
            return {
                "id": series.id,
                "name": series.name,
                "avatar": series.avatar,
                "created_at": series.created_at,
                "user": series.user.username,  # Имя автора серии
            }
        return None


class PostAnonymMapping(Timestamped):
    """Model to mapping autor of anonim post"""

    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    autor = models.ForeignKey("users.UserPublic", on_delete=models.CASCADE)


class PostVote(Timestamped):
    """Model to store post votes."""

    user = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="post_votes"
    )
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="votes")
    value = models.SmallIntegerField(choices=PostVoteChoice.choices, db_index=True)

    class Meta:
        constraints = [
            UniqueConstraint(fields=("user", "post"), name="user_post_vote"),
        ]
        indexes = [
            models.Index(fields=["post", "user", "value"]),
        ]

    def __str__(self):
        return f"<{self.post.title}: {self.value}"


class PostView(models.Model):
    user = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="post_views"
    )
    post = models.ForeignKey("posts.Post", on_delete=models.CASCADE, related_name="views")
    viewed_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            UniqueConstraint(fields=("user", "post"), name="user_post_view"),
        ]


class PostSave(models.Model):
    user = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="post_saver"
    )
    post = models.ForeignKey("posts.Post", on_delete=models.CASCADE, related_name="saved")
    saved_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            UniqueConstraint(fields=("user", "post"), name="user_post_save"),
        ]


class Announcement(models.Model):
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name="featured_posts"
    )
    intro_text = models.CharField(
        max_length=255, default="", help_text="Вводный текст для объявления"
    )
    priority = models.IntegerField(
        default=1, help_text="Приоритет объявления, чем меньше число, тем выше приоритет"
    )
    start_date = models.DateTimeField(
        default=timezone.now, help_text="Дата начала актуальности поста"
    )
    end_date = models.DateTimeField(
        null=True, blank=True, help_text="Дата окончания актуальности поста"
    )
    is_active = models.BooleanField(default=True, help_text="Активно ли объявление")

    class Meta:
        verbose_name = "Актуальный пост"
        verbose_name_plural = "Актуальные посты"
        ordering = ["priority", "start_date"]

    def __str__(self):
        return f"{self.post.title} (Приоритет: {self.priority})"

    def deactivate(self):
        self.is_active = False
        self.save()

    @property
    def is_currently_active(self):
        return self.is_active and self.start_date <= timezone.now() < (
            self.end_date or timezone.now()
        )


class PostBayanVote(Timestamped):
    """Модель для хранения голосований пользователей за тег 'Баян'."""

    user = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="bayan_votes"
    )
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="bayan_votes")

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["user", "post"], name="unique_user_post_bayan_vote"
            )
            # Ограничение на уникальность голосования одного пользователя за один пост
        ]
        indexes = [
            models.Index(fields=["post", "user"]),
        ]

    def __str__(self):
        return f"Post {self.post_id} - User {self.user_id}"


class PostSeries(Timestamped):
    """Model represents a series of posts."""

    user = models.ForeignKey(
        "users.UserPublic",
        on_delete=models.CASCADE,
        related_name="post_series",
        verbose_name="Автор",
    )
    name = models.CharField(
        max_length=100,
        verbose_name="Название серии",
        help_text="Название серии постов, заданное автором",
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name="Дата создания",
        help_text="Дата и время создания серии",
    )
    avatar = models.URLField(blank=True, default="", verbose_name="Аватар серии")

    class Meta:
        verbose_name = "Серия постов"
        verbose_name_plural = "Серии постов"
        constraints = [
            models.UniqueConstraint(
                fields=["user", "name"], name="unique_series_name_per_user"
            )
        ]
        ordering = ["created_at", "name"]

    def __str__(self):
        return f"Серия '{self.name}' (Автор: {self.user})"


class PostSeriesMembership(models.Model):
    """Связь между постами и сериями."""

    series = models.ForeignKey(
        PostSeries,
        on_delete=models.CASCADE,
        related_name="memberships",
        verbose_name="Серия",
    )
    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        related_name="memberships",
        verbose_name="Пост",
        null=True,  # Делаем поле необязательным
        blank=True,  # Позволяем оставлять поле пустым в формах
    )
    added_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата добавления")

    class Meta:
        verbose_name = "Связь поста с серией"
        verbose_name_plural = "Связи постов с сериями"
        constraints = [
            models.UniqueConstraint(
                fields=["series", "post"], name="unique_post_in_series"
            ),
        ]
        indexes = [
            models.Index(fields=["post"], name="idx_post_id"),
            models.Index(fields=["series", "post"], name="idx_series_post_id"),
        ]
        ordering = ["added_at"]

    def __str__(self):
        return f"Пост {self.post.title} в серии {self.series.name}"
