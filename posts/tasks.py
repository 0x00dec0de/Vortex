import contextlib
import logging

from celery import shared_task
from django.db import IntegrityError, transaction
from django.db.models import Count, F, OuterRef, Q, Subquery, Sum
from django.utils import timezone

from comments.services import update_post_comments_count
from posts.choices import PostVoteChoice
from posts.helpers import is_top_ratio, is_trend_ratio
from posts.models import Post, PostView, PostVote
from posts.types import BulkPostViewData
from users.models import UserPublic
from users.tasks import (
    sync_bayan_vote_to_post,
    sync_user_rating_task,
    sync_user_votes_count_task,
)

logger = logging.getLogger(__name__)


@shared_task
def record_post_view_task(user_id: int, post_id: int):
    with contextlib.suppress(IntegrityError), transaction.atomic():
        PostView.objects.create(user_id=user_id, post_id=post_id)
        Post.objects.filter(pk=post_id).select_for_update().update(
            views_count=F("views_count") + 1
        )


@shared_task
def record_bulk_views_task(bulk_views_data: BulkPostViewData, user_id: int):
    post_views_to_create = []
    post_ids_to_sync = []
    for data in bulk_views_data:
        try:
            post = Post.published.get(slug=data["slug"])
            post_ids_to_sync.append(post.pk)
            post_views_to_create.append(
                PostView(
                    user=UserPublic.objects.get(pk=user_id),
                    post=post,
                    viewed_at=data.get("viewed_at") or timezone.now(),
                )
            )
        except Post.DoesNotExist:
            continue
    PostView.objects.bulk_create(post_views_to_create, ignore_conflicts=True)
    sync_post_views_task(post_ids_to_sync)


@shared_task
def sync_post_views_task(post_ids: list[int] | None):
    posts_qs = (
        Post.published.alias(real_views_count=Count("views"))
        .exclude(views_count=F("real_views_count"))
        .filter(real_views_count__gt=0)
    )
    if post_ids is not None:
        posts_qs = posts_qs.filter(pk__in=post_ids)

    posts_qs.update(
        views_count=Subquery(
            PostView.objects.filter(post_id=OuterRef("pk"))
            .annotate(views_sum=Count("pk"))
            .values("views_sum")[:1]
        )
    )


@shared_task
def update_data_on_post_vote_task(post_id: int, voter_user_id: int):
    """Обновляет количество голосов у поста и у проголосовавшего, рейтинг автора."""
    sync_post_rating_and_votes_counts_task(post_id=post_id)
    sync_user_votes_count_task(user_id=voter_user_id)
    sync_bayan_vote_to_post(post_id)


@shared_task
@transaction.atomic
def sync_post_rating_and_votes_counts_task(post_id: int):
    post = Post.objects.select_for_update().get(pk=post_id)
    rating_data = PostVote.objects.filter(post_id=post_id).aggregate(
        rating=Sum("value"),
        votes_up_count=Count("pk", Q(value=PostVoteChoice.UPVOTE)),
        votes_down_count=Count("pk", Q(value=PostVoteChoice.DOWNVOTE)),
    )
    post.rating = rating_data["rating"] or 0
    post.votes_up_count = rating_data["votes_up_count"]
    post.votes_down_count = rating_data["votes_down_count"]
    if is_trend_ratio(
        rating_data["votes_up_count"], rating_data["votes_down_count"]
    ) and (not post.trending_date or post.trending_date == post.published_at):
        post.trending_date = timezone.now()
    elif is_top_ratio(
        rating_data["votes_up_count"], rating_data["votes_down_count"]
    ) and (not post.top_date or post.top_date == post.published_at):
        post.top_date = timezone.now()

    post.save(
        update_fields=[
            "rating",
            "votes_up_count",
            "votes_down_count",
            "trending_date",
            "top_date",
        ]
    )
    sync_user_rating_task(post.user_id)


@shared_task
def sync_all_posts_rating_comments_and_votes_counts_cronjob():
    for post in Post.objects.iterator():
        logger.info("Syncing post %s", post)
        sync_post_rating_and_votes_counts_task(post.pk)
        update_post_comments_count(post.pk)
