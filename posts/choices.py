from django.db import models


class PostStatus(models.TextChoices):
    DRAFT = "draft", "Черновик"
    PUBLISHED = "published", "Опубликован"
    DELETED = "deleted", "Удален"


class PostVoteChoice(models.IntegerChoices):
    UPVOTE = 1
    DOWNVOTE = -1


class PostFeedType(models.TextChoices):
    NEW = "new", "Новое"
    COMMENTED = "commented", "Комментируемые"
    RATING = "rating", "Топ"
    TRENDS = "trends", "Тренды"
    VIEWED = "viewed", "Просмотренные"
    SUBSCRIPTIONS = "subscriptions", "Подписки"
    SAVED = "saved", "Сохраненное"
    COPYRIGHT = "copyright", "Авторское"


class TimePeriod(models.TextChoices):
    DAY = "day", "День"
    WEEK = "week", "Неделя"
    MONTH = "month", "Месяц"
    ALL = "all", "Все время"
