import pydantic_core
from django.conf import settings
from django.utils import timezone
from drf_spectacular.utils import OpenApiExample, extend_schema_serializer
from rest_framework import serializers
from rest_framework.relations import SlugRelatedField

from common.content.blocks.common import PostContentBlocks
from common.errors import convert_errors
from posts.choices import PostVoteChoice
from posts.models import (
    Announcement,
    Post,
    PostBayanVote,
    PostSeries,
    PostSeriesMembership,
    PostVote,
)
from posts.selectors import can_edit_post, get_post_images_count, get_post_videos_count
from tags.models import Tag
from users.api.serializers import UserPublicMinimalSerializer


class PostSerializer(serializers.ModelSerializer):
    """Serializer to represent Post instance."""

    author = UserPublicMinimalSerializer(source="user")
    tags = serializers.SlugRelatedField("name", many=True, read_only=True)
    comments_count = serializers.IntegerField()
    votes_up_count = serializers.IntegerField()
    votes_down_count = serializers.IntegerField()
    rating = serializers.FloatField()
    voted = serializers.ChoiceField(
        choices=PostVoteChoice.choices, allow_null=True, source="_voted"
    )
    images_count = serializers.SerializerMethodField()
    videos_count = serializers.SerializerMethodField()
    can_edit = serializers.SerializerMethodField()
    is_anonym = serializers.BooleanField(required=False, default=False)
    is_answer = serializers.BooleanField(required=False, default=False)
    no_vote = serializers.BooleanField(read_only=True)  # Только для чтения
    no_comments = serializers.BooleanField(read_only=True)  # Только для чтения
    published_at = serializers.DateTimeField(required=False, allow_null=True)
    series = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = (
            "uuid",
            "author",
            "title",
            "slug",
            "content",
            "tags",
            "views_count",
            "comments_count",
            "votes_up_count",
            "votes_down_count",
            "rating",
            "voted",
            "status",
            "images_count",
            "videos_count",
            "published_at",
            "can_edit",
            "is_anonym",
            "is_answer",
            "no_vote",
            "no_comments",
            "series",
        )

    def get_can_edit(self, obj: Post):
        request = self.context.get("request")
        user = request and request.user
        return can_edit_post(user, obj)

    def get_images_count(self, obj: Post) -> int:
        return get_post_images_count(obj)

    def get_videos_count(self, obj: Post) -> int:
        return get_post_videos_count(obj)

    def get_series(self, obj: Post):
        """Возвращает информацию о серии, к которой принадлежит пост."""
        membership = (
            PostSeriesMembership.objects.filter(post=obj).select_related("series").first()
        )  # Находим первую связь через PostSeriesMembership
        if membership and membership.series:
            series = membership.series
            return {
                "id": series.id,
                "name": series.name,
                "avatar": series.avatar,
            }
        return None

    def validate_published_at(self, value):
        if value and value <= timezone.now():
            raise serializers.ValidationError("Дата публикации должна быть в будущем.")
        return value


class PostRatingOnlySerializer(serializers.ModelSerializer):
    """Serializer to return only rating related data."""

    votes_up_count = serializers.IntegerField()
    votes_down_count = serializers.IntegerField()
    rating = serializers.FloatField()
    voted = serializers.ChoiceField(
        choices=PostVoteChoice.choices, allow_null=True, source="_voted"
    )

    class Meta:
        model = Post
        fields = (
            "uuid",
            "slug",
            "votes_up_count",
            "votes_down_count",
            "rating",
            "voted",
        )


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            name="Создание поста",
            value={
                "title": "Мой супер-пупер-заголовок!",
                "content": {
                    "blocks": [{"type": "paragraph", "data": {"text": "А это сам пост"}}],
                    "version": "2.28.2",
                },
                "is_anonym": False,
                "tags": ["Первый пост", "Тест", "Капибара"],
            },
            request_only=True,
        ),
    ]
)
class PostCreateSerializer(serializers.ModelSerializer):
    """Serializer to accept and validate data to create Post."""

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    author = UserPublicMinimalSerializer(source="user", read_only=True)
    tags = SlugRelatedField(
        slug_field="name",
        queryset=Tag.objects.all(),
        many=True,
        required=False,
        allow_null=True,
    )
    is_anonym = serializers.BooleanField(required=False, default=False)
    is_answer = serializers.BooleanField(required=False, default=False)
    no_vote = serializers.BooleanField(read_only=True)
    no_comments = serializers.BooleanField(read_only=True)
    published_at = serializers.DateTimeField(required=False, allow_null=True)

    class Meta:
        model = Post
        fields = (
            "uuid",
            "user",
            "author",
            "title",
            "slug",
            "content",
            "tags",
            "status",
            "published_at",
            "is_anonym",
            "is_answer",
            "no_vote",
            "no_comments",
        )
        read_only_fields = (
            "uuid",
            "author",
            "slug",
            "status",
            # "published_at",
        )

    def validate_content(self, value):
        if not isinstance(value, dict):
            raise serializers.ValidationError("Неверный формат данных")
        try:
            value = PostContentBlocks(**{**value, "should_validate": False}).model_dump(
                by_alias=True, exclude_none=True
            )
            return value
        except pydantic_core.ValidationError as e:
            errors = convert_errors(e, include_input=False)
            raise serializers.ValidationError(errors[0]["msg"])


class PostPublishSerializer(PostCreateSerializer):
    tags = SlugRelatedField(
        slug_field="name",
        queryset=Tag.objects.all(),
        many=True,
        required=False,
        allow_null=True,
    )
    is_anonym = serializers.BooleanField(required=False, default=False)
    is_answer = serializers.BooleanField(required=False, default=False)
    no_vote = serializers.BooleanField(read_only=True)
    no_comments = serializers.BooleanField(read_only=True)

    class Meta(PostCreateSerializer.Meta):
        ...

    def validate_tags(self, value):
        tags = list(filter(None, value))
        tags_count = len(tags)
        if (
            tags_count < settings.MIN_TAGS_IN_POST
            or tags_count > settings.MAX_TAGS_IN_POST
        ):
            raise serializers.ValidationError(
                f"Введите от {settings.MIN_TAGS_IN_POST} до "
                f"{settings.MAX_TAGS_IN_POST} тегов"
            )
        return tags

    def validate_content(self, value):
        if not isinstance(value, dict):
            raise serializers.ValidationError("Неверный формат данных")
        try:
            validated_value = PostContentBlocks(**value)
            value = validated_value.model_dump(by_alias=True, exclude_none=True)
        except pydantic_core.ValidationError as e:
            errors = convert_errors(e, include_input=False)
            raise serializers.ValidationError(errors[0]["msg"])
        return value


class PostVoteCreateSerializer(serializers.ModelSerializer):
    """Serializer validating data submitted to cast a vote on a post."""

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = PostVote
        fields = (
            "user",
            "value",
        )


class PostVoteBayanCreateSerializer(serializers.ModelSerializer):
    """Serializer validating data submitted to cast a vote on a post."""

    post = serializers.SlugRelatedField(queryset=Post.objects.all(), slug_field="slug")
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = PostBayanVote
        fields = (
            "user",
            "post",
        )


class PostViewSerializer(serializers.Serializer):
    slug = serializers.SlugField()
    viewed_at = serializers.DateTimeField(required=False, allow_null=True)


class PostSaveSerializer(serializers.Serializer):
    slug = serializers.SlugField()
    saved_at = serializers.DateTimeField(required=False, allow_null=True)


class BulkPostViewSerializer(serializers.Serializer):
    views = PostViewSerializer(many=True)


class PostTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            "slug",
            "title",
            "uuid",
        )


class AnnouncementSerializer(serializers.ModelSerializer):
    post_title = serializers.CharField(source="post.title", read_only=True)
    post_url = serializers.SerializerMethodField()

    class Meta:
        model = Announcement
        fields = ["post_title", "post_url", "intro_text"]

    def get_post_url(self, obj):
        return obj.post.get_absolute_url()


class PostSeriesSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(read_only=True)
    avatar = serializers.URLField(required=False, allow_blank=True)
    posts_count = serializers.IntegerField(source="memberships.count", read_only=True)
    posts = serializers.SerializerMethodField()

    class Meta:
        model = PostSeries
        fields = ["id", "name", "avatar", "created_at", "posts_count", "posts"]
        read_only_fields = ["user", "created_at"]

    def get_posts(self, obj):
        """
        Возвращает список постов в серии с их заголовками, URL и именем серии.
        """
        posts = (
            Post.objects.filter(memberships__series=obj)
            .order_by("-memberships__added_at")
            .only("id", "title", "slug")
        )

        return [
            {
                "title": post.title,
                "url": post.get_absolute_url(),
                "series_name": obj.name,
            }
            for post in posts
        ]
