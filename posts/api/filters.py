from django.conf import settings
from django.db.models import Exists, OuterRef, Q
from django.utils import timezone
from django_filters import rest_framework as filters

from posts.choices import PostFeedType, PostStatus, TimePeriod
from posts.helpers import time_period_to_timedelta
from posts.selectors import (
    fetch_commented_posts,
    fetch_copyright_posts,
    fetch_my_posts,
    fetch_new_posts,
    fetch_rating_posts,
    fetch_saved_posts,
    fetch_subscribed_posts,
    fetch_trends_posts,
    fetch_user_posts,
    fetch_viewed_posts,
)
from tags.models import Tag
from users.models import UserPublic


class PostFilter(filters.FilterSet):
    username = filters.CharFilter(method="filter_by_user")
    tag = filters.BaseInFilter(method="filter_by_tag")
    tag_exclude = filters.BaseInFilter(method="filter_by_tag_exclude")
    feed_type = filters.ChoiceFilter(
        choices=PostFeedType.choices, method="filter_by_feed_type"
    )
    period = filters.ChoiceFilter(choices=TimePeriod.choices, method="filter_by_period")
    status = filters.ChoiceFilter(choices=PostStatus.choices, method="filter_by_status")
    is_viewed = filters.BooleanFilter(method="filter_by_viewed_status", required=False)
    min_displayed_rating = filters.NumberFilter(method="filter_by_min_rating")

    def filter_by_feed_type(self, queryset, name, value):
        user = self.request.user
        posts_methods = {
            PostFeedType.NEW: fetch_new_posts,
            PostFeedType.COMMENTED: fetch_commented_posts,
            PostFeedType.RATING: fetch_rating_posts,
            PostFeedType.TRENDS: fetch_trends_posts,
            PostFeedType.VIEWED: fetch_viewed_posts,
            PostFeedType.SUBSCRIPTIONS: fetch_subscribed_posts,
            PostFeedType.SAVED: fetch_saved_posts,
            PostFeedType.COPYRIGHT: fetch_copyright_posts,
        }
        if filter_method := posts_methods.get(value):
            # If we don't filter by period, then apply default filtering
            default_period = not self.data.get("period")
            return filter_method(
                viewer=user, queryset=queryset, default_period=default_period
            )
        return queryset

    def filter_by_period(self, queryset, name, value):
        if time_period := time_period_to_timedelta(value):
            from_datetime = timezone.now() - time_period
            return queryset.filter(published_at__gte=from_datetime)
        return queryset

    def filter_by_user(self, queryset, name, value):
        try:
            user = UserPublic.objects.get(username=value)
        except UserPublic.DoesNotExist:
            return queryset.none()

        if user == self.request.user:
            return fetch_my_posts(user)

        return fetch_user_posts(user=user, viewer=self.request.user, queryset=queryset)

    def filter_by_tag(self, queryset, name, value):
        tags = list(Tag.objects.filter(name__in=value))
        if not tags:
            return queryset.none()

        if len(tags) == 1:
            return queryset.filter(tags__in=tags).distinct().cache()

        condition = Q()
        tag_relationship_model = queryset.model.tags.through
        for tag in tags:
            new_condition = Q(
                Exists(
                    tag_relationship_model.objects.filter(post_id=OuterRef("pk"), tag=tag)
                )
            )
            condition &= new_condition

        return queryset.filter(condition).distinct().cache()

    def filter_by_tag_exclude(self, queryset, name, value):
        tags = list(Tag.objects.filter(name__in=value))
        if not tags:
            return queryset
        return (
            queryset.exclude(tags__name__in=[tag.name for tag in tags]).distinct().cache()
        )

    def filter_by_status(self, queryset, name, value):
        return queryset.filter(status=value)

    def filter_by_viewed_status(self, queryset, name, value):
        if (
            value is None
            or not self.request.user.is_authenticated
            or self.data.get("feed_type") == PostFeedType.VIEWED
        ):
            return queryset

        if value is True:
            return queryset.filter(views__user=self.request.user).distinct()
        elif value is False:
            return queryset.exclude(views__user=self.request.user).distinct()

        return queryset

    def filter_by_min_rating(self, queryset, name, value):
        # Если передан username, игнорируем фильтр по минимальному рейтингу
        if "username" in self.data or self.data.get("feed_type") in [
            PostFeedType.SUBSCRIPTIONS,
            PostFeedType.VIEWED,
        ]:
            return queryset

        min_rating_value = value or settings.MIN_DISPLAYED_POST_RATING
        return queryset.filter(rating__gte=min_rating_value)


class PostFilterBackend(filters.DjangoFilterBackend):
    def get_ordering(self, request, queryset, view):
        feed_type = request.query_params.get("feed_type")
        feed_mapping = {
            PostFeedType.COMMENTED: ("-comments_count", "-rating", "-published_at"),
            PostFeedType.RATING: (
                "-published_at",
                "-votes_up_count",
                "-_positive_ratio",
            ),
            PostFeedType.TRENDS: (
                "-trending_date",
                "-published_at",
                "-votes_up_count",
                "-_positive_ratio",
            ),
            PostFeedType.VIEWED: "-viewed_at",
        }
        return feed_mapping.get(feed_type, "-published_at")
