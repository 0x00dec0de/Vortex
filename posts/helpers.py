import datetime

from django.conf import settings

from posts.choices import TimePeriod


def time_period_to_timedelta(time_period: TimePeriod) -> datetime.timedelta | None:
    time_periods_map = {
        TimePeriod.DAY: datetime.timedelta(days=1),
        TimePeriod.WEEK: datetime.timedelta(days=7),
        TimePeriod.MONTH: datetime.timedelta(days=30),
        TimePeriod.ALL: None,
    }
    return time_periods_map.get(time_period)


def is_trend_ratio(votes_up_count: int, votes_down_count: int):
    if not votes_up_count and not votes_down_count:
        return False
    positive_ratio = votes_up_count / (votes_up_count + votes_down_count)
    ratio_is_trend = (
        votes_up_count >= settings.POST_MIN_POSITIVE_VOTES_FOR_TRENDS_RATING_FEED
    )
    percentage_is_trend = (
        positive_ratio
        >= settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TRENDS_RATING_FEED / 100.0
    )
    return ratio_is_trend and percentage_is_trend


def is_top_ratio(votes_up_count: int, votes_down_count: int):
    if not votes_up_count and not votes_down_count:
        return False
    positive_ratio = votes_up_count / (votes_up_count + votes_down_count)
    ratio_is_top = votes_up_count >= settings.POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED
    percentage_is_top = (
        positive_ratio
        >= settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED / 100.0
    )
    return ratio_is_top and percentage_is_top
