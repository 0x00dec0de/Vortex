import json
import logging
import operator
import re

import reversion
from django.db import IntegrityError
from django.db.models import F
from django.utils import timezone

from comments.models import Comment
from notification.constants import NotificationCategories, NotificationTypes
from notification.models import UserNotification
from notification.tasks import (
    CreateNotificationException,
    create_comment_mention,
    create_post_mention,
    create_user_mention,
)
from posts.choices import PostStatus, PostVoteChoice
from posts.constants import ANONYMOUS_USER_USERNAME
from posts.evaluators import PostEvaluator
from posts.exceptions import PostDeleteException, PostPublishException
from posts.models import Post, PostAnonymMapping, PostBayanVote, PostSave, PostVote
from posts.selectors import get_post_vote_value_for_author
from posts.tasks import update_data_on_post_vote_task
from users.models import UserPublic
from users.selectors import can_moderate_posts

logger = logging.getLogger(__name__)
USER_MENTION_REGEX = re.compile(r"profile.*?\@(.*?)\<\/a\>")
POST_MENTION_REGEX = re.compile(r"post\/(.*?)\\\".*?<\/a\>")
COMMENT_MENTION_REGEX = re.compile(r"post\/.*?\?comment\=(.*?)\\\".*?<\/a\>")


def parse_user_mentions(
    flatten_content: str, post: Post = None, comment: Comment = None
) -> None:
    user_mentions = USER_MENTION_REGEX.findall(flatten_content) or []
    if user_mentions and (post or comment):
        url = post.get_absolute_url() if post else comment.get_absolute_url()
        for user_mention in set(user_mentions):
            create_user_mention(
                category=NotificationCategories.USER_MENTION,
                url=url,
                username=user_mention,
                post=post,
            )


def parse_post_mentions(
    flatten_content: str, post: Post = None, comment: Comment = None
) -> None:
    post_mentions = POST_MENTION_REGEX.findall(flatten_content) or []
    if post_mentions and (post or comment):
        url = post.get_absolute_url() if post else comment.get_absolute_url()
        for post_mention in set(post_mentions):
            create_post_mention(
                category=NotificationCategories.POST_MENTION,
                slug_post=post_mention,
                url=url,
                post=post,
                comment=comment,
            )


def parse_comment_mentions(
    flatten_content: str, post: Post = None, comment: Comment = None
) -> None:
    comment_mentions = COMMENT_MENTION_REGEX.findall(flatten_content) or []
    if comment_mentions and (post or comment):
        url = post.get_absolute_url() if post else comment.get_absolute_url()
        for comment_mention in set(comment_mentions):
            create_comment_mention(
                category=NotificationCategories.COMMENT_MENTION,
                url=url,
                comment_uuid=comment_mention,
                post=post,
                comment=comment,
            )


def parse_content_make_mentions(post: Post) -> None:
    # ? Replace with contextlib.suppress(AttributeError):
    try:
        flatten_content = " ".join(
            [
                json.dumps(block.get("data", ""))
                for block in post.content.get("blocks", [])
            ]
        )
        parse_user_mentions(flatten_content, post=post)
        parse_post_mentions(flatten_content, post=post)
        parse_comment_mentions(flatten_content, post=post)

    except CreateNotificationException as exc:
        logger.info(exc)


def parse_comment_content_make_mentions(comment_uuid: str, content: dict) -> None:
    # ? Replace with contextlib.suppress(AttributeError):
    try:
        comment = Comment.objects.filter(uuid=comment_uuid).first() or None
        flatten_content = " ".join(
            [json.dumps(block.get("data", "")) for block in content.get("blocks", [])]
        )
        parse_user_mentions(flatten_content, comment=comment)
        parse_post_mentions(flatten_content, comment=comment)
        parse_comment_mentions(flatten_content, comment=comment)

    except CreateNotificationException as exc:
        logger.info(exc)


def update_author_rating_on_post_vote(post_vote: PostVote, vote_cancelled: bool = False):
    """Update author rating when it's post gets vote."""
    operation = {
        True: operator.sub,
        False: operator.add,
    }[vote_cancelled]

    # TODO: обновить рейтинг автора только в течении Х дней после публикации поста
    author = post_vote.post.user
    author.rating = operation(
        F("rating"), get_post_vote_value_for_author(author, post_vote)
    )
    author.save(update_fields=["rating"])


def update_voter_votes_count_on_post_vote(
    post_vote: PostVote, vote_cancelled: bool = False
):
    """Update votes counts for voter."""
    voter: UserPublic = post_vote.user
    operation = {
        True: operator.sub,
        False: operator.add,
    }[vote_cancelled]
    if post_vote.value == PostVoteChoice.UPVOTE:
        voter.votes_up_count = operation(F("votes_up_count"), 1)
    else:
        voter.votes_down_count = operation(F("votes_down_count"), 1)
    voter.save(update_fields=["votes_up_count", "votes_down_count"])


def record_vote_for_post(post: Post, actor: UserPublic, vote: PostVoteChoice):
    """Record vote for a post and trigger updates of post and post's author."""
    post_vote = PostVote.objects.filter(post=post, user=actor).first()
    if post_vote:
        # Здесь если при наличии голоса, пользователь проголосовал еще раз,
        # отменяем рейтинг и удаляем PostVote
        post_vote.delete()
    else:
        try:
            PostVote.objects.create(post=post, user=actor, value=vote)
        except IntegrityError:
            # Если не дает создать такой голос, значит он уже в базе и будет посчитан,
            # скорее всего это rage-clicks и можно проигнорировать
            logger.info(
                "User %s trying to vote with same value %s for post %s", actor, vote, post
            )
    update_data_on_post_vote_task.delay(post_id=post.pk, voter_user_id=actor.pk)


def record_vote_bayan_for_post(post: Post, actor: UserPublic):
    """Record vote for a post and trigger updates of post and post's author."""

    try:
        PostBayanVote.objects.create(user=actor, post=post)
    except IntegrityError:
        # Если не дает создать такой голос, значит он уже в базе и будет посчитан,
        # скорее всего это rage-clicks и можно проигнорировать
        logger.info(
            "User %s trying to vote bayan with same value %s for post %s", actor, post
        )
    update_data_on_post_vote_task.delay(post_id=post.pk, voter_user_id=actor.pk)


def record_saving_for_post(post: Post, actor: UserPublic):
    """Record saving for a post"""
    post_saved = PostSave.objects.filter(post=post, user=actor).first()
    if post_saved:
        # Здесь если при наличии сохранения поста, пользователь нажал еще раз,
        # удаляем сохранение
        post_saved.delete()
    else:
        try:
            PostSave.objects.create(post=post, user=actor)
        except IntegrityError:
            # Если не дает создать такое сохранение, значит он уже в базе,
            # скорее всего это rage-clicks и можно проигнорировать
            logger.info(
                "User %s trying to save with same value %s for post %s", actor, post
            )


def publish_post(post: Post, actor: UserPublic) -> Post:
    """Publish the post.

    Raises
    ------
    PostPublishException
        In case user trying to publish not own post, or deleted post
    """
    if post.user != actor:
        raise PostPublishException("Нельзя публиковать чужой пост!")

    if post.status == PostStatus.DELETED:
        raise PostPublishException("Нельзя публиковать удаленный пост!")
    if post.is_anonym:
        PostAnonymMapping.objects.create(post=post, autor=post.user)
        post.user = UserPublic.objects.get(username=ANONYMOUS_USER_USERNAME)
    if post.status == PostStatus.DRAFT:
        logger.info("Post %s published by %s", post, actor)
        with reversion.create_revision():
            PostEvaluator(post).run()
            post.status = PostStatus.PUBLISHED
            post.published_at = timezone.now()
            post.save()
            if post.is_anonym:
                notif = UserNotification.objects.create(
                    user=actor,
                    message=f"Ваш пост опубликован анонимно. "
                    f"Вам не будут приходить оповещения об ответах в этом посте. "
                    f"Ссылка на пост: {post.get_absolute_url()}",
                    notification_type=NotificationTypes.WEB,
                    category=NotificationCategories.USER_EVENT,
                    post=post,
                )
                notif.save()
            reversion.set_user(actor)
            reversion.set_comment("Пост опубликован")
            parse_content_make_mentions(post)

    return post


def delete_post(post: Post, actor: UserPublic):
    """Mark post as deleted.

    Raises
    ------
    PostDeleteException
        When user tries to delete someone else's post
    """
    if post.user != actor and not can_moderate_posts(actor):
        raise PostDeleteException("Нельзя удалять чужие посты!")

    if post.status == PostStatus.DELETED:
        return

    logger.info("Post %s deleted by %s", post, actor)
    with reversion.create_revision():
        post.status = PostStatus.DELETED
        post.save()
        reversion.set_user(actor)
        reversion.set_comment("Пост удален")
