import factory
from django.utils import timezone
from factory.django import DjangoModelFactory

from posts.choices import PostStatus
from posts.constants import ANONYMOUS_USER_USERNAME
from posts.models import PostAnonymMapping
from tags.models import Tag
from tags.tests.factories import TagFactory
from users.models import UserPublic
from users.tests.factories import UserPublicFactory


class PostFactory(DjangoModelFactory):
    user = factory.SubFactory(UserPublicFactory)
    title = factory.Faker("text")
    content = {"blocks": [{"type": "paragraph", "data": {"text": "Пост из PostFactory"}}]}

    class Meta:
        model = "posts.Post"

    @factory.post_generation
    def populate_start_and_end_date(self, create, extracted, **kwargs):
        """Populate the start and end date if not populated."""
        if not create:
            return
        if self.status == PostStatus.PUBLISHED and not self.published_at:
            self.published_at = timezone.now()
            self.save(update_fields=["published_at"])

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted is None:
            return

        tags = []
        for tag in extracted:
            if isinstance(tag, str):
                tags.append(TagFactory(name=tag))
            elif isinstance(tag, Tag):
                tags.append(tag)
        self.tags.set(tags)

    @factory.post_generation
    def bind_published_post_to_anonymous_user(
        self, create: bool, extracted: None, **kwargs
    ) -> None:
        """Emulate publishing anonymous post from `posts.services.publish_post`.
        User with username `ANONYMOUS_USER_USERNAME` should be created
        before calling a Factory.
        """
        if not create or not self.is_anonym or self.status != PostStatus.PUBLISHED:
            return

        PostAnonymMapping.objects.create(post=self, autor=self.user)
        self.user = UserPublic.objects.get(username=ANONYMOUS_USER_USERNAME)
        self.save(update_fields=["user"])


class PostVoteFactory(DjangoModelFactory):
    user = factory.SubFactory(UserPublicFactory)
    post = factory.SubFactory(PostFactory)

    class Meta:
        model = "posts.PostVote"


class PostViewFactory(DjangoModelFactory):
    user = factory.SubFactory(UserPublicFactory)
    post = factory.SubFactory(PostFactory)

    class Meta:
        model = "posts.PostView"
