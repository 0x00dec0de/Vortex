import pytest
from funcy import first
from rest_framework import status

from posts.choices import PostStatus
from posts.constants import ANONYMOUS_USER_USERNAME
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestFilterByUser(PostsApiActionsMixin):
    def setup_method(self):
        self.poster = UserPublicFactory()
        self.published_post = PostFactory(user=self.poster, status=PostStatus.PUBLISHED)
        self.draft_post = PostFactory(user=self.poster, status=PostStatus.DRAFT)
        self.other_user_post = PostFactory(status=PostStatus.PUBLISHED)

    def test_can_be_filtered_by_user(self, anon_api_client):
        result = self._fetch_posts_feed(
            anon_api_client(), {"username": self.poster.username}
        )
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]
        assert len(posts) == 1
        assert first(posts)["slug"] == self.published_post.slug

    def test_can_be_filtered_by_anonym(self, anon_api_client):
        _ = UserPublicFactory(username=ANONYMOUS_USER_USERNAME)
        anonym_post = PostFactory(
            user=self.poster, status=PostStatus.PUBLISHED, is_anonym=True
        )

        result = self._fetch_posts_feed(
            anon_api_client(),
            {"username": ANONYMOUS_USER_USERNAME},
        )

        response = result.json()
        posts = response["results"]
        assert result.status_code == status.HTTP_200_OK
        assert len(posts) == 1
        assert first(posts)["slug"] == anonym_post.slug

    @pytest.mark.parametrize("username,expected_count", (("not-found", 0), ("", 2)))
    def test_empty_result_when_user_not_found_or_empty(
        self, username, expected_count, anon_api_client
    ):
        result = self._fetch_posts_feed(anon_api_client(), {"username": username})
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]
        assert len(posts) == expected_count
