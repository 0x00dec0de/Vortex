import pytest
from django.conf import settings
from django.test import override_settings
from funcy import lpluck
from rest_framework import status

from posts.choices import PostStatus, PostVoteChoice
from posts.tasks import sync_post_rating_and_votes_counts_task
from posts.tests.factories import PostFactory, PostVoteFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestFilterByMinDisplayedRating(PostsApiActionsMixin):
    def setup_method(self):
        self.old_min_displayed_rating = settings.MIN_DISPLAYED_POST_RATING
        self.min_displayed_rating = -2
        settings.MIN_DISPLAYED_POST_RATING = self.min_displayed_rating

        # Создаем посты с разными рейтингами
        self.some_user = UserPublicFactory()
        self.low_rating_post = PostFactory(
            status=PostStatus.PUBLISHED, user=self.some_user
        )
        self.borderline_rating_post = PostFactory(
            status=PostStatus.PUBLISHED, user=self.some_user
        )
        self.high_rating_post = PostFactory(
            status=PostStatus.PUBLISHED, user=self.some_user
        )

        PostVoteFactory.create_batch(
            size=int(abs(settings.MIN_DISPLAYED_POST_RATING) + 1),  # -16
            value=PostVoteChoice.DOWNVOTE,
            post=self.low_rating_post,
        )
        PostVoteFactory.create_batch(
            size=int(abs(settings.MIN_DISPLAYED_POST_RATING)),
            value=PostVoteChoice.DOWNVOTE,
            post=self.borderline_rating_post,
        )
        PostVoteFactory.create_batch(
            size=int(abs(settings.MIN_DISPLAYED_POST_RATING) - 1),  # -14
            value=PostVoteChoice.DOWNVOTE,
            post=self.high_rating_post,
        )
        for post in (
            self.low_rating_post,
            self.borderline_rating_post,
            self.high_rating_post,
        ):
            sync_post_rating_and_votes_counts_task(post.pk)

    def teardown_method(self):
        settings.MIN_DISPLAYED_POST_RATING = self.old_min_displayed_rating

    @override_settings(MIN_DISPLAYED_POST_RATING=-2)
    def test_excludes_posts_below_min_rating(self, anon_api_client):
        # Проверяем, что посты с рейтингом ниже
        # минимального не возвращаются
        result = self._fetch_posts_feed(
            anon_api_client(),
            {"min_displayed_rating": settings.MIN_DISPLAYED_POST_RATING},
        )
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        post_slugs = lpluck("slug", posts)
        assert self.low_rating_post.slug not in post_slugs
        assert self.borderline_rating_post.slug in post_slugs
        assert self.high_rating_post.slug in post_slugs

    @override_settings(MIN_DISPLAYED_POST_RATING=-2)
    def test_ignores_filter_if_username_provided(self, anon_api_client):
        # Проверяем, что фильтр по рейтингу игнорируется, если указан username
        result = self._fetch_posts_feed(
            anon_api_client(),
            {
                "username": self.some_user.username,
                "min_displayed_rating": settings.MIN_DISPLAYED_POST_RATING,
            },
        )
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        post_slugs = lpluck("slug", posts)
        assert self.low_rating_post.slug in post_slugs
        assert self.borderline_rating_post.slug in post_slugs
        assert self.high_rating_post.slug in post_slugs
        # Проверяем, что в ответе присутствуют посты,
        # даже если их рейтинг ниже минимального (логика фильтрации
        # по username может отличаться)

    # TODO: turn on when subscribtion will be ready

    # def test_ignores_filter_for_specific_feed_types(self, authed_api_client):
    #     # Проверяем, что фильтр по рейтингу игнорируется для
    #     # специфических типов фида (SUBSCRIPTIONS, VIEWED)
    #     result = self._fetch_posts_feed(
    #         authed_api_client(self.some_user),
    #         {
    #             "feed_type": PostFeedType.SUBSCRIPTIONS.value,
    #             "min_displayed_rating": settings.MIN_DISPLAYED_POST_RATING,
    #         },
    #     )
    #     assert result.status_code == status.HTTP_200_OK
    #     response = result.json()
    #     posts = response["results"]
    #     assert self.low_rating_post.slug in [post["slug"] for post in posts]
    #     assert self.borderline_rating_post.slug in [post["slug"] for post in posts]
    #     assert self.high_rating_post.slug in [post["slug"] for post in posts]
    #     # Проверяем, что в ответе присутствуют посты, даже если их
    #     # рейтинг ниже минимального (логика фильтрации по feed_type может отличаться)
