import datetime
from unittest.mock import patch

import pytest
from django.conf import settings
from freezegun import freeze_time
from rest_framework import status
from rest_framework.reverse import reverse

from posts.choices import PostStatus, PostVoteChoice
from posts.constants import ANONYMOUS_USER_USERNAME
from posts.tests.factories import PostFactory, PostVoteFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestRecordVoteForPost(PostsApiActionsMixin):
    def setup_method(self):
        self.post = PostFactory(status=PostStatus.PUBLISHED)

        self.voter = UserPublicFactory()

    @pytest.mark.parametrize(
        "vote_value,expected_votes_count,expected_post_rating,expected_post_user_rating",
        [
            (PostVoteChoice.UPVOTE, 1, 1, 1),
            (PostVoteChoice.DOWNVOTE, 1, -1, -1),
        ],
    )
    def test_voting_first_time(
        self,
        authed_api_client,
        vote_value,
        expected_votes_count,
        expected_post_rating,
        expected_post_user_rating,
    ):
        client = authed_api_client(self.voter)
        self._vote_for_post(client, self.post, vote_value)

        self.post.refresh_from_db()
        self.post.user.refresh_from_db()
        self.voter.refresh_from_db()

        assert self.post.votes_up_count == (
            1 if vote_value == PostVoteChoice.UPVOTE else 0
        )
        assert self.post.votes_down_count == (
            1 if vote_value == PostVoteChoice.DOWNVOTE else 0
        )
        assert self.post.rating == expected_post_rating

        assert self.voter.votes_up_count == (
            1 if vote_value == PostVoteChoice.UPVOTE else 0
        )
        assert self.voter.votes_down_count == (
            1 if vote_value == PostVoteChoice.DOWNVOTE else 0
        )

        assert self.post.user.rating == (1 if vote_value == PostVoteChoice.UPVOTE else -1)

    @pytest.mark.parametrize(
        "vote_value", (PostVoteChoice.UPVOTE, PostVoteChoice.DOWNVOTE)
    )
    def test_voting_for_own_post(self, authed_api_client, vote_value):
        client = authed_api_client(self.post.user)
        result = self._vote_for_post(client, self.post, vote_value)
        assert result.status_code == status.HTTP_201_CREATED

    @pytest.mark.parametrize(
        "vote_value, field_name, rating",
        (
            (PostVoteChoice.UPVOTE, "votes_up_count", 1.0),
            (PostVoteChoice.DOWNVOTE, "votes_down_count", -1.0),
        ),
        ids=("Upvote", "Downvote"),
    )
    def test_vote_for_anonymous_post(
        self, authed_api_client, vote_value, field_name, rating
    ):
        anonymous_system_user = UserPublicFactory(username=ANONYMOUS_USER_USERNAME)
        anonymous_post = PostFactory(status=PostStatus.PUBLISHED, is_anonym=True)
        client = authed_api_client(self.voter)

        result = self._vote_for_post(client, anonymous_post, vote_value)

        self.post.user.refresh_from_db()
        anonymous_system_user.refresh_from_db()
        assert result.status_code == status.HTTP_201_CREATED
        assert result.data[field_name] == 1
        assert self.post.user.rating == 0.0
        assert anonymous_system_user.rating == rating

    def test_forbid_to_vote_for_post_as_autobanned_user(self, authed_api_client):
        """Forbid user with rating less than `RATING_FOR_AUTOBAN` to vote for post."""
        banned_user = UserPublicFactory()
        downvoted_post = PostFactory(
            status=PostStatus.PUBLISHED, user=banned_user, tags=["one", "nine"]
        )
        PostVoteFactory.create_batch(
            size=4,
            value=PostVoteChoice.DOWNVOTE,
            post=downvoted_post,
        )
        post = PostFactory(
            status=PostStatus.PUBLISHED, user=banned_user, tags=["one", "two"]
        )

        with patch.object(settings, "RATING_FOR_AUTOBAN", -3.0):
            result = self._vote_for_post(
                authed_api_client(banned_user), post, PostVoteChoice.UPVOTE
            )

        assert (
            result.status_code == status.HTTP_429_TOO_MANY_REQUESTS
        ), result.content.decode()
        assert (
            result.data["detail"][0]["msg"]
            == "Вы не можете голосовать за посты из-за низкого рейтинга"
        )
        assert result.data["detail"][0]["type"] == "rating_too_low"

    @pytest.mark.parametrize(
        "vote_value",
        (PostVoteChoice.UPVOTE, PostVoteChoice.DOWNVOTE),
        ids=("upvote", "downvote"),
    )
    def test_voting_for_own_anonymous_post(self, authed_api_client, vote_value):
        _ = UserPublicFactory(username=ANONYMOUS_USER_USERNAME)
        anonym_post = PostFactory(status=PostStatus.PUBLISHED, is_anonym=True)
        client = authed_api_client(self.post.user)

        result = self._vote_for_post(client, anonym_post, vote_value)

        assert result.status_code == status.HTTP_201_CREATED

    @pytest.mark.parametrize(
        "second_vote",
        (PostVoteChoice.UPVOTE, PostVoteChoice.DOWNVOTE),
        ids=("upvote", "downvote"),
    )
    @pytest.mark.parametrize(
        "first_vote", (PostVoteChoice.UPVOTE, PostVoteChoice.DOWNVOTE)
    )
    def test_undo_vote(self, authed_api_client, first_vote, second_vote):
        client = authed_api_client(self.voter)
        self._vote_for_post(client, self.post, first_vote)
        result = self._vote_for_post(client, self.post, second_vote)

        assert result.data == {
            "slug": self.post.slug,
            "uuid": str(self.post.uuid),
            "votes_up_count": 0,
            "votes_down_count": 0,
            "rating": 0,
            "voted": None,
        }
        voter_result = self._get_user(client, self.voter)
        voter_data = voter_result.data
        assert voter_data["votes_up_count"] == 0
        assert voter_data["votes_down_count"] == 0

        author_result = self._get_user(client, self.post.user)
        author_result = author_result.data
        assert author_result["rating"] == 0

    def test_cant_vote_as_anonymous(self, anon_api_client):
        result = self._vote_for_post(anon_api_client(), self.post, PostVoteChoice.UPVOTE)
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_cant_vote_with_wrong_values(self, authed_api_client):
        client = authed_api_client(self.voter)
        result = self._vote_for_post(client, self.post, 10)
        assert result.status_code == status.HTTP_400_BAD_REQUEST

    def test_cant_vote_non_active(self, authed_api_client):
        voter = UserPublicFactory(is_active=False)
        client = authed_api_client(voter)
        result = self._vote_for_post(client, self.post, PostVoteChoice.UPVOTE)
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    @pytest.mark.parametrize(
        "vote", (None, PostVoteChoice.UPVOTE, PostVoteChoice.DOWNVOTE)
    )
    def test_vote_choice_shown_for_current_user_in_posts(
        self, vote, authed_api_client, anon_api_client
    ):
        if vote:
            PostVoteFactory(post=self.post, user=self.voter, value=vote)
        result = self._fetch_posts_feed(authed_api_client(self.voter))
        assert result.status_code == status.HTTP_200_OK
        post = result.data["results"][0]
        assert post["voted"] == vote

        # For another user voted should be always None
        another_user = UserPublicFactory()
        result = self._fetch_posts_feed(authed_api_client(another_user))
        assert result.status_code == status.HTTP_200_OK
        post = result.data["results"][0]
        assert post["voted"] is None

        # For anon user voted should be always None
        result = self._fetch_posts_feed(anon_api_client())
        assert result.status_code == status.HTTP_200_OK
        post = result.data["results"][0]
        assert post["voted"] is None

    @pytest.mark.parametrize(
        "is_verified,over_limit,expected_status_code,expected_error_code",
        (
            (
                False,
                True,
                status.HTTP_429_TOO_MANY_REQUESTS,
                "limit_reached_confirm_telegram",
            ),
            (False, False, status.HTTP_201_CREATED, None),
            (True, True, status.HTTP_429_TOO_MANY_REQUESTS, "limit_reached"),
            (True, False, status.HTTP_201_CREATED, None),
        ),
    )
    def test_post_votes_limit(  # noqa: CFQ002
        self,
        is_verified,
        over_limit,
        expected_status_code,
        expected_error_code,
        authed_api_client,
        settings,
    ):
        settings.LIMIT_VOTES_DEFAULT = 5
        settings.LIMIT_VOTES_FOR_UNVERIFIED = 1
        pre_create_votes = (
            settings.LIMIT_VOTES_DEFAULT
            if is_verified
            else settings.LIMIT_VOTES_FOR_UNVERIFIED
        )
        user = UserPublicFactory(is_verified=is_verified)
        if over_limit:
            PostVoteFactory.create_batch(
                size=pre_create_votes, user=user, value=PostVoteChoice.UPVOTE
            )
        post = PostFactory(status=PostStatus.PUBLISHED)

        result = self._vote_for_post(authed_api_client(user), post, PostVoteChoice.UPVOTE)

        assert result.status_code == expected_status_code
        if expected_error_code:
            data = result.json()
            assert data["detail"][0]["type"] == expected_error_code

        with freeze_time(
            datetime.datetime.now()
            + datetime.timedelta(hours=settings.LIMIT_TIMEFRAME_HOURS + 1)
        ):
            post = PostFactory(status=PostStatus.PUBLISHED)
            result = self._vote_for_post(
                authed_api_client(user), post, PostVoteChoice.DOWNVOTE
            )
            assert result.status_code == status.HTTP_201_CREATED

    def _get_user(self, client, user):
        return client.get(
            reverse(
                "v1:users:users-detail",
                kwargs={"username": user.username},
            ),
        )
