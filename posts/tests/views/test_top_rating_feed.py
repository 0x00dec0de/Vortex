import datetime

import pytest
from django.conf import settings
from django.utils import timezone
from rest_framework import status

from posts.choices import PostFeedType, PostStatus, PostVoteChoice
from posts.constants import ANONYMOUS_USER_USERNAME
from posts.selectors import get_min_positive_votes_for_top_rating_feed
from posts.tasks import sync_post_rating_and_votes_counts_task
from posts.tests.factories import PostFactory, PostVoteFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestTopFeed(PostsApiActionsMixin):
    # TODO: separate test to hide old post
    def setup_method(self):
        self.old_min_positive_votes = settings.POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED
        self.old_min_percentage = (
            settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED
        )
        settings.POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED = 5
        settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED = 80
        _ = UserPublicFactory(username=ANONYMOUS_USER_USERNAME)

        now = timezone.now()
        self.post_total_upvotes_100_percent = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=8)
        )
        # This post shouldn't be present in the API output due to default filtering
        self.old_post_total_upvotes_100_percent = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=100)
        )
        self.post_total_upvotes_90_percent = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=2)
        )
        self.post_total_upvotes_50_percent = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=1)
        )
        self.anonymous_post_with_rating_4 = PostFactory(
            status=PostStatus.PUBLISHED,
            published_at=now - datetime.timedelta(days=5),
            is_anonym=True,
        )

        min_positive_votes = get_min_positive_votes_for_top_rating_feed()

        PostVoteFactory.create_batch(
            size=min_positive_votes,
            value=PostVoteChoice.UPVOTE,
            post=self.post_total_upvotes_100_percent,
        )
        PostVoteFactory.create_batch(
            size=min_positive_votes,
            value=PostVoteChoice.UPVOTE,
            post=self.old_post_total_upvotes_100_percent,
        )

        PostVoteFactory.create_batch(
            size=min_positive_votes + 5,
            value=PostVoteChoice.UPVOTE,
            post=self.post_total_upvotes_90_percent,
        )
        PostVoteFactory.create_batch(
            size=int((min_positive_votes + 5) * 0.1),
            value=PostVoteChoice.DOWNVOTE,
            post=self.post_total_upvotes_90_percent,
        )

        PostVoteFactory.create_batch(
            size=min_positive_votes,
            value=PostVoteChoice.UPVOTE,
            post=self.post_total_upvotes_50_percent,
        )
        PostVoteFactory.create_batch(
            size=min_positive_votes,
            value=PostVoteChoice.DOWNVOTE,
            post=self.post_total_upvotes_50_percent,
        )
        PostVoteFactory.create_batch(
            size=min_positive_votes,
            value=PostVoteChoice.UPVOTE,
            post=self.anonymous_post_with_rating_4,
        )
        PostVoteFactory.create_batch(
            size=int(min_positive_votes * 0.2),
            value=PostVoteChoice.DOWNVOTE,
            post=self.anonymous_post_with_rating_4,
        )

        for post in (
            self.old_post_total_upvotes_100_percent,
            self.post_total_upvotes_50_percent,
            self.post_total_upvotes_90_percent,
            self.post_total_upvotes_100_percent,
            self.anonymous_post_with_rating_4,
        ):
            sync_post_rating_and_votes_counts_task(post.pk)

    def teardown_method(self):
        settings.POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED = self.old_min_positive_votes
        settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED = (
            self.old_min_percentage
        )

    def test_top_rating_feed(self, anon_api_client):
        result = self._fetch_posts_feed(
            anon_api_client(), {"feed_type": PostFeedType.RATING}
        )

        data = result.json()
        results = data["results"]
        ordering_from_api = [post["uuid"] for post in results]

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert ordering_from_api == [
            str(self.post_total_upvotes_90_percent.uuid),
            str(self.anonymous_post_with_rating_4.uuid),
            str(self.post_total_upvotes_100_percent.uuid),
        ]
