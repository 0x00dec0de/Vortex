from datetime import timedelta

import pytest
from freezegun import freeze_time
from funcy import first, where
from rest_framework import status

from posts.choices import PostStatus
from posts.constants import ANONYMOUS_USER_USERNAME
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestModeratorPostActions(PostsApiActionsMixin):
    def setup_method(self):
        self.moderator = UserPublicFactory(is_staff=True)

    @freeze_time(timedelta(days=-300))
    @pytest.mark.parametrize("post_status", [PostStatus.DRAFT, PostStatus.PUBLISHED])
    def test_moderator_can_edit_any_post(self, post_status, authed_api_client):
        post = PostFactory(title="post title", status=post_status)
        new_title = "edited title"
        result = self._edit_post(
            authed_api_client(self.moderator), post, {"title": new_title}
        )

        post.refresh_from_db()
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert result.json()["title"] == new_title
        assert post.title == new_title

    @freeze_time(timedelta(days=-300))
    def test_moderator_can_edit_anonymous_post(self, authed_api_client):
        _ = UserPublicFactory(username=ANONYMOUS_USER_USERNAME)
        post = PostFactory(
            title="post title", status=PostStatus.PUBLISHED, is_anonym=True
        )
        new_title = "edited title"

        result = self._edit_post(
            authed_api_client(self.moderator), post, {"title": new_title}
        )

        post.refresh_from_db()
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert result.json()["title"] == new_title
        assert post.title == new_title

    @pytest.mark.parametrize("post_status", [PostStatus.DRAFT, PostStatus.PUBLISHED])
    def test_moderator_can_delete_any_post(self, post_status, authed_api_client):
        post = PostFactory(status=post_status)

        result = self._delete_post(authed_api_client(self.moderator), post)

        post.refresh_from_db()
        assert result.status_code == status.HTTP_204_NO_CONTENT, result.content.decode()
        assert post.status == PostStatus.DELETED

    def test_moderator_can_delete_anonymous_post(self, authed_api_client):
        _ = UserPublicFactory(username=ANONYMOUS_USER_USERNAME)
        post = PostFactory(status=PostStatus.PUBLISHED, is_anonym=True)

        result = self._delete_post(authed_api_client(self.moderator), post)

        post.refresh_from_db()
        assert result.status_code == status.HTTP_204_NO_CONTENT, result.content.decode()
        assert post.status == PostStatus.DELETED

    @freeze_time(timedelta(days=-300))
    def test_can_edit_flag_on_post_is_true_for_moderator(self, authed_api_client):
        post = PostFactory(status=PostStatus.PUBLISHED)
        result = self._fetch_posts_feed(authed_api_client(self.moderator))
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        post_from_api = first(where(result.json()["results"], uuid=str(post.uuid)))
        assert post_from_api["can_edit"] is True
