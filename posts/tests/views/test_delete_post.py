import pytest
from rest_framework import status
from reversion.models import Version

from posts.choices import PostStatus
from posts.constants import ANONYMOUS_USER_USERNAME
from posts.models import Post
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestDeletePost(PostsApiActionsMixin):
    def setup(self):
        self.post = PostFactory(status=PostStatus.PUBLISHED)

    def test_anonymous_cant_delete_post(self, anon_api_client):
        result = self._delete_post(anon_api_client(), self.post)
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_cant_delete_not_own_post(self, authed_api_client):
        user = UserPublicFactory()
        client = authed_api_client(user)
        result = self._delete_post(client, self.post)

        assert result.status_code == status.HTTP_403_FORBIDDEN
        post = Post.objects.filter(pk=self.post.pk).first()
        assert post.status == PostStatus.PUBLISHED

    @pytest.mark.parametrize(
        "post_status",
        (PostStatus.PUBLISHED, PostStatus.DRAFT),
        ids=("published", "deleted"),
    )
    def test_can_delete_own_post(self, post_status, authed_api_client):
        post = PostFactory(status=post_status)
        client = authed_api_client(post.user)

        result = self._delete_post(client, post)

        post.refresh_from_db()
        assert result.status_code == status.HTTP_204_NO_CONTENT
        assert post.status == PostStatus.DELETED
        # Проверка создания версии поста
        versions = Version.objects.get_for_object(post)
        assert versions.count() == 1
        version = versions.first()
        assert version.revision.user_id == post.user_id
        assert version.revision.comment == "Пост удален"

    @pytest.mark.parametrize(
        "is_anonymous", (True, False), ids=("anonymous", "not anonymous")
    )
    def test_cannot_delete_deleted_post(self, is_anonymous, authed_api_client):
        post = PostFactory(status=PostStatus.DELETED, is_anonym=is_anonymous)
        client = authed_api_client(post.user)

        result = self._delete_post(client, post)

        post.refresh_from_db()
        assert result.status_code == status.HTTP_404_NOT_FOUND
        assert post.status == PostStatus.DELETED

    def test_can_delete_own_draft_anonymous_post(self, authed_api_client):
        post = PostFactory(status=PostStatus.DRAFT, is_anonym=True)
        client = authed_api_client(post.user)

        result = self._delete_post(client, post)

        post.refresh_from_db()
        assert result.status_code == status.HTTP_204_NO_CONTENT
        assert post.status == PostStatus.DELETED

    # ! User cannot edit anonymous post
    # (test_forbid_to_edit_anonymous_post_after_publication)
    # but it's possible to delete the post
    def test_cannot_delete_own_published_anonymous_post(self, authed_api_client):
        _ = UserPublicFactory(username=ANONYMOUS_USER_USERNAME)
        post = PostFactory(status=PostStatus.PUBLISHED, is_anonym=True)
        client = authed_api_client(post.user)

        result = self._delete_post(client, post)

        post.refresh_from_db()
        assert result.status_code == status.HTTP_204_NO_CONTENT
        assert post.status == PostStatus.DELETED
