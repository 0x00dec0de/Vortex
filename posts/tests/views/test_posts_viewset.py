import datetime
from unittest.mock import patch

import pytest
from django.conf import settings
from django.utils.timezone import make_aware
from rest_framework import status

from common.api.pagination import KapibaraCursorPagination
from common.helpers import sanitize_html_content
from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin


@pytest.mark.django_db
class TestPostsViewSet(PostsApiActionsMixin):
    def setup_method(self):
        self.old_page_size = KapibaraCursorPagination.page_size
        KapibaraCursorPagination.page_size = 1

    def teardown_method(self):
        KapibaraCursorPagination.page_size = self.old_page_size

    @pytest.mark.parametrize("images_count", (0, 3))
    @patch(
        "common.content.blocks.image.sanitize_html_content",
        wraps=sanitize_html_content,
    )
    def test_post_has_proper_images_count(
        self, spy_sanitize_html_content, images_count, anon_api_client
    ):
        image_content = {
            "type": "image",
            "data": {
                "file": {
                    "url": f"https://{settings.CDN_BASE_DOMAIN}/img.jpeg",
                },
                "caption": "Image caption",
                "withBorder": True,
                "stretched": False,
                "withBackground": True,
            },
        }
        content = {
            "blocks": [
                *[image_content] * images_count,
                {"type": "paragraph", "data": {"text": "Тест создания поста"}},
            ]
        }
        PostFactory(status=PostStatus.PUBLISHED, content=content)
        result = self._fetch_posts_feed(anon_api_client())
        data = result.json()

        assert data["results"][0]["images_count"] == images_count
        spy_sanitize_html_content.assert_not_called()

    @pytest.mark.parametrize("videos_count", (0, 3))
    @patch(
        "common.content.blocks.video.sanitize_html_content",
        wraps=sanitize_html_content,
    )
    def test_post_has_proper_videos_count(
        self, spy_sanitize_html_content, videos_count, anon_api_client
    ):
        video_content = {
            "type": "embed",
            "data": {
                "service": "youtube",
                "source": "https://youtube.com",
                "embed": "embed",
                "width": 580,
                "height": 320,
                "caption": "Описание",
            },
        }
        content = {
            "blocks": [
                *[video_content] * videos_count,
                {"type": "paragraph", "data": {"text": "Тест создания поста"}},
            ]
        }
        PostFactory(status=PostStatus.PUBLISHED, content=content)
        result = self._fetch_posts_feed(anon_api_client())
        data = result.json()

        assert data["results"][0]["videos_count"] == videos_count
        spy_sanitize_html_content.assert_not_called()

    def test_cursor_pagination(self, anon_api_client):
        post1 = PostFactory(
            status=PostStatus.PUBLISHED,
            published_at=make_aware(datetime.datetime(2021, 1, 1)),
        )
        post2 = PostFactory(
            status=PostStatus.PUBLISHED,
            published_at=make_aware(datetime.datetime(2022, 1, 1)),
        )
        post3 = PostFactory(
            status=PostStatus.PUBLISHED,
            published_at=make_aware(datetime.datetime(2023, 1, 1)),
        )

        client = anon_api_client()

        result = self._fetch_posts_feed(client)

        prev_cursor, next_cursor = self._assert_paginated_result_and_return_nav(
            post3, result
        )
        assert prev_cursor is None

        result = self._fetch_posts_feed(client, {"cursor": next_cursor})
        prev_cursor, next_cursor = self._assert_paginated_result_and_return_nav(
            post2, result
        )

        result = self._fetch_posts_feed(client, {"cursor": next_cursor})
        prev_cursor, next_cursor = self._assert_paginated_result_and_return_nav(
            post1, result
        )
        assert next_cursor is None

    @pytest.mark.parametrize("is_logged_in", (True, False))
    def test_deleted_post_gives_404(
        self, is_logged_in, authed_api_client, anon_api_client
    ):
        post = PostFactory(status=PostStatus.DELETED)
        client = authed_api_client(post.user) if is_logged_in else anon_api_client()
        result = self._get_post(client, post)
        assert result.status_code == status.HTTP_404_NOT_FOUND, result.content.decode()

    def _assert_paginated_result_and_return_nav(self, post, result):
        assert result.status_code == status.HTTP_200_OK
        data = result.json()
        assert len(data["results"]) == 1
        assert data["results"][0]["uuid"] == str(post.uuid)
        return data["previous"], data["next"]
