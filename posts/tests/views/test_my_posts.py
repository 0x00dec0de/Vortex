import datetime
from unittest.mock import ANY, patch

import pytest
from django.conf import settings
from django.utils import timezone
from freezegun import freeze_time
from rest_framework import status
from reversion.models import Version

from posts.choices import PostStatus, PostVoteChoice
from posts.constants import ANONYMOUS_USER_USERNAME
from posts.models import Post
from posts.selectors import get_tag_for_telegram
from posts.tests.factories import PostFactory, PostVoteFactory
from posts.tests.mixins import PostsApiActionsMixin
from tags.tests.factories import TagFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestMyPosts(PostsApiActionsMixin):
    def setup_method(self):
        self.tags_objects = TagFactory.create_batch(size=25)
        self.title = "Заголовок"
        self.expected_slug = "zagolovok"
        self.tags = [*[tag.name for tag in self.tags_objects[:5]]]
        self.content_html = {"type": "paragraph", "data": {"text": "Тест создания поста"}}
        self.content_image = {
            "type": "image",
            "data": {
                "file": {
                    "url": f"https://{settings.CDN_BASE_DOMAIN}/img.jpeg",
                },
                "caption": "Image caption",
                "withBorder": True,
                "stretched": False,
                "withBackground": True,
            },
        }
        self.content_youtube = {
            "type": "embed",
            "data": {
                "service": "youtube",
                "source": "https://youtube.com",
                "embed": "embed",
                "width": 580,
                "height": 320,
                "caption": "Описание",
            },
        }
        self.content_coub = {
            "type": "embed",
            "data": {
                "service": "coub",
                "source": "https://coub.com",
                "embed": "embed",
                "width": 580,
                "height": 320,
                "caption": "Описание",
            },
        }
        self.content_vimeo = {
            "type": "embed",
            "data": {
                "service": "vimeo",
                "source": "https://vimeo.com",
                "embed": "embed",
                "width": 580,
                "height": 320,
                "caption": "Описание",
            },
        }
        self.content_vk = {
            "type": "embed",
            "data": {
                "service": "vk",
                "source": "https://vk.com",
                "embed": "embed",
                "width": 580,
                "height": 320,
                "caption": "Описание",
            },
        }
        self.content_rutube = {
            "type": "embed",
            "data": {
                "service": "rutube",
                "source": "https://rutube.com",
                "embed": "embed",
                "width": 580,
                "height": 320,
                "caption": "Описание",
            },
        }
        self.content = {
            "blocks": [
                self.content_html,
                self.content_image,
                self.content_youtube,
                self.content_coub,
                self.content_vimeo,
                self.content_vk,
                self.content_rutube,
            ]
        }
        self.data = {
            "title": self.title,
            "content": self.content,
            "tags": self.tags,
        }
        self.user = UserPublicFactory()

    def test_user_can_save_as_draft_without_validation_errors(self, authed_api_client):
        result = self._create_post(
            authed_api_client(self.user),
            data={},
        )

        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        data = result.data
        assert data["title"] == ""
        assert data["slug"] is None
        assert data["content"] == {}
        assert data["is_anonym"] is False

        # Проверка создания версии поста
        versions = Version.objects.get_for_object(Post.objects.get(uuid=data["uuid"]))
        assert versions.count() == 1
        version = versions.first()
        assert version.revision.user_id == self.user.id
        assert version.revision.comment == "Пост создан"
        assert version.field_dict["title"] == ""
        assert version.field_dict["content"] == {}

    def test_create_draft_anonymous_post(self, authed_api_client):
        result = self._create_post(
            authed_api_client(self.user),
            data={"is_anonym": True},
        )

        data = result.data
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        assert data["title"] == ""
        assert data["slug"] is None
        assert data["content"] == {}
        assert data["is_anonym"] is True

    def test_slug_assigned_when_publishing_draft_post(self, authed_api_client):
        title = "Заголовок"
        slug = "zagolovok"
        post = PostFactory(status=PostStatus.DRAFT, title=title, tags=["one", "two"])
        assert post.slug is None

        result = self._publish_post(authed_api_client(post.user), post)

        assert result.status_code == status.HTTP_200_OK, result.content.decode()

        post.refresh_from_db()
        assert post.slug == slug

    def test_not_logged_in_user_cant_fetch_my_draft_or_deleted_posts(
        self, anon_api_client
    ):
        author = UserPublicFactory()
        PostFactory(status=PostStatus.DRAFT, user=author)
        PostFactory(status=PostStatus.DELETED, user=author)
        result = self._fetch_my_posts(anon_api_client(), author.username)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert len(result.data["results"]) == 0

    @pytest.mark.parametrize("slug", ("", "zhopa"))
    def test_can_fetch_my_draft_post(self, slug, authed_api_client):
        post = PostFactory(user=self.user, status=PostStatus.DRAFT, slug=slug)
        result = self._get_post(authed_api_client(self.user), post)
        assert result.status_code == status.HTTP_200_OK
        data = result.data

        assert data["title"] == post.title

    def test_can_fetch_my_draft_anonymous_post(self, authed_api_client):
        post = PostFactory(user=self.user, status=PostStatus.DRAFT, is_anonym=True)

        result = self._get_post(authed_api_client(self.user), post)

        assert result.status_code == status.HTTP_200_OK

    @pytest.mark.parametrize("is_logged_in", (False, True))
    def test_cannot_fetch_someones_draft_post(
        self, is_logged_in, authed_api_client, anon_api_client
    ):
        client = authed_api_client(self.user) if is_logged_in else anon_api_client()
        post = PostFactory(status=PostStatus.DRAFT, slug="zhopa")
        result = self._get_post(client, post)
        assert result.status_code == status.HTTP_404_NOT_FOUND

    def test_publish_draft_post_integrity_error(self, authed_api_client):
        title = "zhopa"
        suffix = "test"
        PostFactory(title=title, status=PostStatus.PUBLISHED)
        PostFactory(title=f"{title} {suffix}", status=PostStatus.PUBLISHED)
        post = PostFactory(title=title, status=PostStatus.DRAFT, tags=["one", "two"])

        with patch("posts.fields.generate_random_string", return_value=suffix):
            result = self._publish_post(authed_api_client(post.user), post)

        assert result.status_code == status.HTTP_400_BAD_REQUEST
        data = result.json()
        assert data["detail"][0]["type"] == "slug_generation_error"
        assert (
            data["detail"][0]["msg"]
            == "Что-то пошло не так, попробуйте опубликовать еще раз."
        )

    @pytest.mark.skip("Flaky test")
    def test_slug_changed_when_editing_title_of_draft_post_which_has_slug(
        self,
        authed_api_client,
    ):
        title = "Жопаааа"
        slug = "zhopaaaa"
        post = PostFactory(status=PostStatus.DRAFT, slug="old-zhopa")
        assert post.slug == "old-zhopa"

        result = self._edit_post(authed_api_client(post.user), post, {"title": title})

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.json()

        assert data["title"] == title
        assert data["slug"] == slug

        post.refresh_from_db()
        assert post.title == title
        assert post.slug == slug

    def test_limit_of_draft_posts(self, authed_api_client):
        PostFactory.create_batch(size=settings.LIMIT_DRAFT_POSTS, user=self.user)
        result = self._create_post(authed_api_client(self.user), self.data)

        assert result.status_code == status.HTTP_429_TOO_MANY_REQUESTS
        data = result.json()
        assert data["detail"][0]["msg"] == "Вы достигли лимита черновиков."
        assert data["detail"][0]["type"] == "limit_reached"

    def test_active_user_can_create_new_draft_post(self, authed_api_client):
        result = self._create_post(
            authed_api_client(self.user),
            data={**self.data, "status": PostStatus.PUBLISHED},
        )

        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        data = result.data
        assert data["title"] == self.title
        assert data["content"] == {**self.content, "time": ANY, "version": ""}
        # Для черновиков мы не генерируем slug
        assert data["slug"] is None
        assert data["status"] == PostStatus.DRAFT
        assert sorted(data["tags"]) == sorted(list(filter(None, set(self.tags))))
        assert data["is_anonym"] is False

        # Проверка создания версии поста
        versions = Version.objects.get_for_object(Post.objects.get(uuid=data["uuid"]))
        assert versions.count() == 1
        version = versions.first()
        assert version.revision.user_id == self.user.id
        assert version.revision.comment == "Пост создан"
        assert version.field_dict["title"] == self.title
        assert version.field_dict["content"] == {
            **self.content,
            "time": ANY,
            "version": "",
        }

    def test_cannot_publish_post_with_missing_data(self, authed_api_client):
        post = PostFactory(content={})

        result = self._publish_post(authed_api_client(post.user), post)
        assert result.status_code == status.HTTP_400_BAD_REQUEST

    def test_cannot_publish_draft_post_with_images_over_limit(self, authed_api_client):
        images_content = [self.content_image] * (
            settings.CONTENT_POST_BLOCK_IMG_MAX_ITEMS + 1
        )
        post = PostFactory(
            status=PostStatus.DRAFT, content=[*self.content, *images_content]
        )
        result = self._publish_post(authed_api_client(post.user), post)
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()

    def test_cannot_publish_draft_post_with_videos_over_limit(self, authed_api_client):
        videos_content = [self.content_youtube] * (
            settings.CONTENT_POST_BLOCK_VIDEO_MAX_ITEMS + 1
        )
        post = PostFactory(
            status=PostStatus.DRAFT, content=[*self.content, *videos_content]
        )
        result = self._publish_post(authed_api_client(post.user), post)
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()

    def test_cannot_publish_draft_post_with_text_blocks_over_limit(
        self, authed_api_client
    ):
        html_content = [self.content_youtube] * (
            settings.CONTENT_POST_BLOCK_MAX_HTML_ITEMS + 1
        )
        post = PostFactory(
            status=PostStatus.DRAFT, content=[*self.content, *html_content]
        )
        result = self._publish_post(authed_api_client(post.user), post)
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()

    def test_slug_not_changed_when_editing_title_of_published_post(
        self, authed_api_client
    ):
        post = PostFactory(status=PostStatus.PUBLISHED, slug="old-zhopa")
        slug = post.slug
        title = "new one"

        self._edit_post(
            authed_api_client(post.user), post, {"title": title, "slug": "new-zhopa"}
        )

        post.refresh_from_db()
        assert post.title == title
        assert post.slug == slug

    def test_slug_will_get_random_suffix_if_already_exists_in_db(self, authed_api_client):
        title = "Жопа"
        slug = "zhopa"
        suffix = "test"
        existing_post = PostFactory(status=PostStatus.PUBLISHED, title=title)
        post = PostFactory(status=PostStatus.DRAFT, title=title, tags=["one", "two"])
        assert existing_post.slug == slug

        with patch("posts.fields.generate_random_string", return_value=suffix):
            result = self._publish_post(authed_api_client(post.user), post)

        assert result.status_code == status.HTTP_200_OK, result.content.decode()

        post.refresh_from_db()
        assert post.slug == f"{slug}-{suffix}"

    @pytest.mark.parametrize("tags_number", ("min", "max"))
    def test_cant_publish_with_invalid_tags(self, tags_number, authed_api_client):
        tags_number_map = {
            "min": settings.MIN_TAGS_IN_POST - 1,
            "max": settings.MAX_TAGS_IN_POST + 3,
        }
        tags_number = tags_number_map[tags_number]
        data = self.data.copy()
        data["tags"] = [tag.name for tag in self.tags_objects[:tags_number]]
        post = PostFactory(user=self.user, status=PostStatus.DRAFT, **data)
        result = self._publish_post(authed_api_client(self.user), post)
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        response = result.json()
        assert (
            response["tags"][0] == f"Введите от {settings.MIN_TAGS_IN_POST} до "
            f"{settings.MAX_TAGS_IN_POST} тегов"
        )

    def test_cant_create_with_nonexistent_tag(self, authed_api_client):
        data = self.data.copy()
        data["tags"] = ["рандомный тег 1", "рандомный тег 2"]
        result = self._create_post(authed_api_client(self.user), data=data)
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()

    def test_malicious_html_tags_in_post_content_gets_cleaned(self, authed_api_client):
        content_with_tags = (
            "Test <b>html</b>"
            "<script>alert(1);</script>"
            "<br/>"
            "<h1>h1 tag</h1>"
            "<h2>h2 tag</h2>"
            "<h3>h3 tag</h3>"
            "<h4>h4 tag</h4>"
            "<h5>h5 tag</h5>"
            "<h6>h6 tag</h6>"
            "<a href='https://kapi.bar'>link</a>"
            "<a href='ftp://kapi.bar'>link</a>"
        )
        expected_sanitized_content = (
            "Test <b>html</b>"
            "<br>"
            "h1 tag"
            "h2 tag"
            "h3 tag"
            "h4 tag"
            "h5 tag"
            "h6 tag"
            '<a href="https://kapi.bar" target="_blank" '
            'rel="noopener noreferrer nofollow">link</a>'
            '<a target="_blank" rel="noopener noreferrer nofollow">link</a>'
        )
        result = self._create_post(
            authed_api_client(self.user),
            {
                "title": "html",
                "content": {
                    "blocks": [
                        {"type": "paragraph", "data": {"text": content_with_tags}},
                        {
                            "type": "quote",
                            "data": {
                                "text": content_with_tags,
                                "caption": content_with_tags,
                            },
                        },
                        {
                            "type": "image",
                            "data": {
                                "file": {
                                    "url": f"https://{settings.CDN_BASE_DOMAIN}/img.jpeg"
                                },
                                "caption": content_with_tags,
                            },
                        },
                        {
                            "type": "embed",
                            "data": {
                                "service": "youtube",
                                "source": "source",
                                "embed": "embed",
                                "width": 100,
                                "height": 100,
                                "caption": content_with_tags,
                            },
                        },
                        {
                            "type": "list",
                            "data": {"style": "ordered", "items": [content_with_tags]},
                        },
                    ]
                },
                "tags": self.tags,
            },
        )
        data = result.data
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        assert data["content"]["blocks"][0]["data"]["text"] == expected_sanitized_content
        assert data["content"]["blocks"][1]["data"]["text"] == expected_sanitized_content
        assert (
            data["content"]["blocks"][1]["data"]["caption"] == expected_sanitized_content
        )
        assert (
            data["content"]["blocks"][2]["data"]["caption"] == expected_sanitized_content
        )
        assert (
            data["content"]["blocks"][3]["data"]["caption"] == expected_sanitized_content
        )
        assert (
            data["content"]["blocks"][4]["data"]["items"][0] == expected_sanitized_content
        )

    def test_not_active_user_cant_create_new_draft_post(self, authed_api_client):
        result = self._create_post(
            authed_api_client(UserPublicFactory(is_active=False)), data=self.data
        )

        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_not_logged_in_user_cant_create_new_post(self, anon_api_client):
        result = self._create_post(anon_api_client(), data=self.data)

        assert result.status_code == status.HTTP_401_UNAUTHORIZED, result.content.decode()

    def test_not_logged_in_user_cant_create_anonymous_post(self, anon_api_client):
        request_body = self.data.copy()
        request_body["is_anonym"] = True
        result = self._create_post(anon_api_client(), data=request_body)

        assert result.status_code == status.HTTP_401_UNAUTHORIZED, result.content.decode()

    @pytest.mark.parametrize(
        "field,value,error_message",
        (
            ("title", "", "Это поле не может быть пустым."),
            ("content", [], "Неверный формат данных"),
            ("content", {}, "Число элементов в списке должно быть как минимум 1, а не 0"),
        ),
    )
    def test_need_required_data_to_publish_post(
        self, field, value, error_message, authed_api_client
    ):
        data = self.data.copy()
        data[field] = value
        post = PostFactory(user=self.user, status=PostStatus.DRAFT, **data)
        result = self._publish_post(authed_api_client(self.user), post)

        assert result.status_code == status.HTTP_400_BAD_REQUEST
        response = result.json()
        assert response[field][0] == error_message

    @pytest.mark.parametrize(
        "content,expected_error",
        (
            ("test", "Неверный формат данных, ожидается список."),
            (
                {"type": "paragraph", "value": "comment"},
                "Неверный формат данных, ожидается список.",
            ),
            # FIXME: For some reason it fails, returns 201 status
            (
                [
                    {
                        "type": "image",
                        "data": {
                            "file": {"url": "https://external.com/img.jpeg"},
                        },
                    }
                ],
                "Нельзя загружать файлы с таким URL",
            ),
            (
                [
                    {
                        "type": "image",
                        "data": {
                            "file": {
                                "url": f"https://{settings.CDN_BASE_DOMAIN}/"
                                f"{'img' * 50}.jpeg"
                            }
                        },
                    }
                ],
                "Длина текста должна быть не более 150 символов",
            ),
        ),
    )
    def test_cant_create_draft_post_with_malformed_content(
        self, content, expected_error, authed_api_client
    ):
        data = self.data.copy()
        data["content"] = {"blocks": content}
        result = self._create_post(authed_api_client(self.user), data=data)

        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        response = result.json()
        assert response["content"][0] == expected_error

    @pytest.mark.parametrize("is_logged_in", (False, True))
    def test_can_fetch_any_published_post(
        self, is_logged_in, authed_api_client, anon_api_client
    ):
        client = authed_api_client(self.user) if is_logged_in else anon_api_client()
        post = PostFactory(status=PostStatus.PUBLISHED)
        result = self._get_post(client, post)
        assert result.status_code == status.HTTP_200_OK
        data = result.data

        assert data["title"] == post.title

    @pytest.mark.parametrize("is_logged_in", (False, True))
    def test_cannot_fetch_deleted_post(
        self, is_logged_in, authed_api_client, anon_api_client
    ):
        client = authed_api_client(self.user) if is_logged_in else anon_api_client()
        post = PostFactory(status=PostStatus.DELETED)
        result = self._get_post(client, post)
        assert result.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.parametrize(
        "post_status,expected_status_code",
        (
            (PostStatus.DRAFT, status.HTTP_200_OK),
            (PostStatus.PUBLISHED, status.HTTP_200_OK),
            (PostStatus.DELETED, status.HTTP_404_NOT_FOUND),
        ),
    )
    def test_can_edit_own_post(
        self, post_status, expected_status_code, authed_api_client
    ):
        post = PostFactory(user=self.user, status=post_status)
        tags = self.tags[:3]
        title = "edited post"
        content = {"blocks": [{"type": "paragraph", "data": {"text": "edited post"}}]}
        result = self._edit_post(
            authed_api_client(self.user),
            post,
            {"title": title, "tags": tags, "content": content},
        )

        assert result.status_code == expected_status_code, result.content.decode()
        if post_status == PostStatus.DELETED:
            return
        data = result.data
        assert data["title"] == title
        assert sorted(data["tags"]) == sorted(tags)
        assert data["content"] == {**content, "time": ANY, "version": ""}

        # Проверка создания версии поста
        versions = Version.objects.get_for_object(post)
        assert versions.count() == 1
        version = versions.first()
        assert version.revision.user_id == post.user_id
        assert version.revision.comment == f"Пост отредактирован (статус {post_status})"
        assert version.field_dict["title"] == title
        assert version.field_dict["content"] == {**content, "time": ANY, "version": ""}

    def test_forbid_to_edit_anonymous_post_after_publication(self, authed_api_client):
        _ = UserPublicFactory(username=ANONYMOUS_USER_USERNAME)
        post = PostFactory(user=self.user, status=PostStatus.PUBLISHED, is_anonym=True)

        result = self._edit_post(
            authed_api_client(self.user),
            post,
            {"title": "edited post"},
        )

        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    def test_cannot_edit_not_own_post(self, authed_api_client):
        post = PostFactory()
        result = self._edit_post(authed_api_client(self.user), post, {"title": "test"})
        assert result.status_code == status.HTTP_403_FORBIDDEN

    def test_cannot_edit_as_not_logged_in_user(self, anon_api_client):
        post = PostFactory()
        result = self._edit_post(anon_api_client(), post, {"title": "test"})
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_cannot_edit_as_not_active_user(self, authed_api_client):
        post = PostFactory(user=UserPublicFactory(is_active=False))
        result = self._edit_post(authed_api_client(post.user), post, {"title": "test"})
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_can_list_own_posts(self, authed_api_client):
        author = UserPublicFactory()
        my_posts = [
            PostFactory(status=PostStatus.PUBLISHED, user=author),
            PostFactory(status=PostStatus.DRAFT, user=author),
            # Anonymous post should be returned as well
            PostFactory(status=PostStatus.DRAFT, user=author, is_anonym=True),
        ]
        PostFactory(status=PostStatus.PUBLISHED)

        result = self._fetch_my_posts(
            client=authed_api_client(author), username=author.username
        )

        assert result.status_code == status.HTTP_200_OK, result.content.decode()

        posts_from_api = result.data["results"]
        assert len(posts_from_api) == 3
        api_post_uuids = {post["uuid"] for post in posts_from_api}
        post_uuids = {str(post.uuid) for post in my_posts}
        assert api_post_uuids == post_uuids

    def test_cant_publish_not_own_post(self, authed_api_client):
        post = PostFactory(status=PostStatus.DRAFT)
        user = UserPublicFactory()

        client = authed_api_client(user)

        result = self._publish_post(client, post)

        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    @pytest.mark.parametrize(
        "original_status,expected_status_code",
        (
            (PostStatus.DRAFT, status.HTTP_200_OK),
            (PostStatus.PUBLISHED, status.HTTP_200_OK),
            (PostStatus.DELETED, status.HTTP_404_NOT_FOUND),
        ),
    )
    @freeze_time(datetime.datetime(2023, 8, 28, 10, 45, 58))
    def test_can_publish_certain_statuses(
        self, authed_api_client, original_status, expected_status_code
    ):
        post = PostFactory(status=original_status, tags=["one", "two", "zhopa"])
        client = authed_api_client(post.user)
        result = self._publish_post(client, post)
        assert result.status_code == expected_status_code, result.content.decode()
        if expected_status_code != status.HTTP_404_NOT_FOUND:
            post.refresh_from_db()
            now = timezone.now()
            assert result.data["status"] == PostStatus.PUBLISHED
            if original_status == PostStatus.DRAFT:
                assert result.data["published_at"] == now.strftime("%Y-%m-%dT%H:%M:%SZ")
            else:
                return
            # Проверка создания версии поста
            versions = Version.objects.get_for_object(post)
            assert versions.count() == 1
            version = versions.first()
            assert version.revision.user_id == post.user_id
            assert version.revision.comment == "Пост опубликован"

    def test_can_publish_anonymous_post(self, authed_api_client):
        """Change Post's User to Anonymous user after publication."""
        anonymous_user = UserPublicFactory(username=ANONYMOUS_USER_USERNAME)
        post = PostFactory(
            user=self.user,
            status=PostStatus.DRAFT,
            tags=["one", "two"],
            is_anonym=True,
        )
        client = authed_api_client(post.user)

        result = self._publish_post(client, post)

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert result.data["status"] == PostStatus.PUBLISHED
        assert result.data["author"]["username"] == ANONYMOUS_USER_USERNAME
        assert self.user.posts.filter(status=PostStatus.PUBLISHED).count() == 0
        assert anonymous_user.posts.filter(status=PostStatus.PUBLISHED).count() == 1

    @pytest.mark.parametrize(
        "post_status,expected_status_code",
        (
            (PostStatus.DRAFT, status.HTTP_200_OK),
            (PostStatus.PUBLISHED, status.HTTP_403_FORBIDDEN),
        ),
    )
    def test_edit_post_after_specific_time(
        self, post_status, expected_status_code, authed_api_client
    ):
        post = PostFactory(
            status=post_status, published_at=timezone.now(), tags=self.tags_objects[:3]
        )
        now = timezone.now()
        with freeze_time(
            now + datetime.timedelta(minutes=settings.POST_EDITABLE_WINDOW_MINUTES + 1)
        ):
            result = self._edit_post(
                authed_api_client(post.user),
                post,
                data={"title": "new title"},
            )

        assert result.status_code == expected_status_code, result.content.decode()

    @pytest.mark.parametrize(
        "link,should_have_tag",
        (
            ("https://t.me/abc", True),
            ("https://abc.t.me/", True),
            ("https://abc.t.me", True),
            ("https://telegram.dog/abc", True),
            ("https://telegram.me/abc", True),
            ("https://cat.me/abc", False),
            ("https://telegram.doge/username", False),
        ),
        ids=(
            "short link with sub path",
            "short link with sub domain",
            "short link without backslash",
            "link with .dog TLD",
            "link with .me TLD",
            "link with invalid domain",
            "link with invalid TLD",
        ),
    )
    def test_auto_assign_telegram_tag_to_post_with_link_during_publishing(
        self, link, should_have_tag, authed_api_client
    ):
        telegram_tag = get_tag_for_telegram()
        tags = self.tags_objects[:3]
        post = PostFactory(
            status=PostStatus.DRAFT,
            content={
                "blocks": [
                    {
                        "type": "paragraph",
                        "data": {
                            "text": f"some text with potential <a href='{link}'>link</a>"
                        },
                    }
                ]
            },
            tags=tags,
        )
        client = authed_api_client(post.user)

        result = self._publish_post(client, post)

        assert post.tags.filter(name=telegram_tag.name).exists() is should_have_tag
        assert result.status_code == status.HTTP_200_OK, result.content.decode()

    def test_do_not_auto_assign_telegram_tag_to_post_with_telegram_tag(
        self, authed_api_client
    ):
        telegram_tag = get_tag_for_telegram()
        tags = [telegram_tag, *self.tags_objects[:3]]
        post = PostFactory(
            status=PostStatus.DRAFT,
            content={
                "blocks": [
                    {
                        "type": "paragraph",
                        "data": {
                            "text": (
                                "some text with potential "
                                "<a href='https://t.me/abc'>link</a>"
                            )
                        },
                    }
                ]
            },
            tags=tags,
        )
        client = authed_api_client(post.user)

        result = self._publish_post(client, post)

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert post.tags.filter(name=telegram_tag.name).count() == 1

    @pytest.mark.parametrize(
        "is_verified,over_limit,expected_status_code,expected_error_code",
        (
            (
                False,
                True,
                status.HTTP_429_TOO_MANY_REQUESTS,
                "limit_reached_confirm_telegram",
            ),
            (False, False, status.HTTP_200_OK, None),
            (True, True, status.HTTP_429_TOO_MANY_REQUESTS, "limit_reached"),
            (True, False, status.HTTP_200_OK, None),
        ),
    )
    def test_posts_limit(
        self,
        is_verified,
        over_limit,
        expected_status_code,
        expected_error_code,
        authed_api_client,
    ):
        settings.LIMIT_POSTS_DEFAULT = 5
        settings.LIMIT_POSTS_FOR_UNVERIFIED = 1
        pre_create_posts = (
            settings.LIMIT_VOTES_DEFAULT
            if is_verified
            else settings.LIMIT_VOTES_FOR_UNVERIFIED
        )

        author = UserPublicFactory(is_verified=is_verified)

        if over_limit:
            PostFactory.create_batch(
                size=pre_create_posts, status=PostStatus.PUBLISHED, user=author
            )

        post = PostFactory(
            status=PostStatus.DRAFT, user=author, tags=["one", "two", "zhopa"]
        )

        result = self._publish_post(authed_api_client(author), post)

        assert result.status_code == expected_status_code, result.content.decode()
        if expected_error_code:
            data = result.json()
            assert data["detail"][0]["type"] == expected_error_code

        with freeze_time(
            datetime.datetime.now()
            + datetime.timedelta(hours=settings.LIMIT_TIMEFRAME_HOURS + 1)
        ):
            post = PostFactory(
                status=PostStatus.DRAFT, user=author, tags=["one", "two", "zhopa"]
            )
            result = self._publish_post(authed_api_client(author), post)
            assert result.status_code == status.HTTP_200_OK, result.content.decode()

    def test_forbid_to_publish_anonymous_post_as_unverified_user(self, authed_api_client):
        _ = UserPublicFactory(username=ANONYMOUS_USER_USERNAME)
        author = UserPublicFactory(is_verified=False)

        post = PostFactory(
            status=PostStatus.DRAFT, user=author, tags=["one", "two"], is_anonym=True
        )

        with patch.object(settings, "LIMIT_POSTS_FOR_UNVERIFIED", 0):
            result = self._publish_post(authed_api_client(author), post)

        assert (
            result.status_code == status.HTTP_429_TOO_MANY_REQUESTS
        ), result.content.decode()
        assert result.data["detail"][0]["msg"] == (
            "Вы достигли лимита постов в сутки для неверифицированного "
            "пользователя. Подтвердите свой Телеграм для снятия всех лимитов."
        )
        assert result.data["detail"][0]["type"] == "limit_reached_confirm_telegram"

    def test_forbid_to_publish_post_as_autobanned_user(self, authed_api_client):
        banned_user = UserPublicFactory()
        downvoted_post = PostFactory(
            status=PostStatus.PUBLISHED, user=banned_user, tags=["one", "nine"]
        )
        PostVoteFactory.create_batch(
            size=4,
            value=PostVoteChoice.DOWNVOTE,
            post=downvoted_post,
        )

        post = PostFactory(status=PostStatus.DRAFT, user=banned_user, tags=["one", "two"])

        with patch.object(settings, "RATING_FOR_AUTOBAN", -3.0):
            result = self._publish_post(authed_api_client(banned_user), post)

        assert (
            result.status_code == status.HTTP_429_TOO_MANY_REQUESTS
        ), result.content.decode()
        assert (
            result.data["detail"][0]["msg"]
            == "Вы не можете публиковать посты из-за низкого рейтинга"
        )
        assert result.data["detail"][0]["type"] == "rating_too_low"

    def test_can_create_draft_with_images_over_the_limit(self, authed_api_client):
        images_content = [self.content_image] * (
            settings.CONTENT_POST_BLOCK_IMG_MAX_ITEMS + 1
        )
        data = self.data.copy()
        data["content"] = {"blocks": [*data["content"]["blocks"], *images_content]}
        result = self._create_post(authed_api_client(self.user), data)
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        response = result.json()
        assert response["status"] == PostStatus.DRAFT

    def test_can_create_draft_with_videos_over_the_limit(self, authed_api_client):
        videos_content = [self.content_vimeo] * (
            settings.CONTENT_POST_BLOCK_VIDEO_MAX_ITEMS + 1
        )
        data = self.data.copy()
        data["content"] = {"blocks": [*data["content"]["blocks"], *videos_content]}
        result = self._create_post(authed_api_client(self.user), data)
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        response = result.json()
        assert response["status"] == PostStatus.DRAFT

    def test_can_create_draft_with_text_blocks_over_the_limit(self, authed_api_client):
        html_content = [self.content_html] * (
            settings.CONTENT_POST_BLOCK_MAX_HTML_ITEMS + 1
        )
        data = self.data.copy()
        data["content"] = {"blocks": [*data["content"]["blocks"], *html_content]}
        result = self._create_post(authed_api_client(self.user), data)
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()

        response = result.json()
        assert response["status"] == PostStatus.DRAFT

    def test_cannot_create_draft_with_text_length_over_the_limit(self, authed_api_client):
        html_content = {
            "type": "paragraph",
            "data": {"text": "*" * (settings.CONTENT_BLOCK_MAX_LENGTH_BYTES + 1)},
        }
        data = self.data.copy()
        data["content"] = {"blocks": [*data["content"]["blocks"], html_content]}
        result = self._create_post(authed_api_client(self.user), data)
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        response = result.json()
        assert (
            response["content"][0] == f"Длина текста должна быть не более "
            f"{settings.CONTENT_BLOCK_MAX_LENGTH_BYTES} символов"
        )

    def test_cannot_create_draft_with_list_items_over_the_limit(self, authed_api_client):
        list_block = {
            "type": "list",
            "data": {
                "style": "ordered",
                "items": ["item"] * (settings.CONTENT_MAX_LIST_ITEMS + 1),
            },
        }
        data = self.data.copy()
        data["content"] = {"blocks": [*data["content"]["blocks"], list_block]}
        result = self._create_post(authed_api_client(self.user), data)
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        response = result.json()
        assert (
            response["content"][0] == f"Число элементов в списке должно быть не "
            f"более {settings.CONTENT_MAX_LIST_ITEMS}, "
            f"а не {len(list_block['data']['items'])}"
        )
