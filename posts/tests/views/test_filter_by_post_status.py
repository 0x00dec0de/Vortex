import pytest
from funcy import lpluck, lpluck_attr
from rest_framework import status

from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestFilterByPostStatus(PostsApiActionsMixin):
    def setup_method(self):
        self.poster = UserPublicFactory()
        self.published_post = PostFactory(user=self.poster, status=PostStatus.PUBLISHED)
        self.draft_post = PostFactory(user=self.poster, status=PostStatus.DRAFT)
        self.deleted_post = PostFactory(user=self.poster, status=PostStatus.DELETED)

    @pytest.mark.parametrize(
        "status_param", ("", PostStatus.PUBLISHED, PostStatus.DRAFT, PostStatus.DELETED)
    )
    @pytest.mark.parametrize("logged_in_as_poster", (None, False, True))
    def test_user_can_filter_own_posts_by_status(
        self, logged_in_as_poster, status_param, anon_api_client, authed_api_client
    ):
        match logged_in_as_poster:
            case None:
                client = anon_api_client()
            case True:
                client = authed_api_client(self.poster)
            case _:
                client = authed_api_client(UserPublicFactory())

        posts_mapping = {
            "": [self.published_post, self.draft_post, self.deleted_post],
            PostStatus.PUBLISHED: [self.published_post],
            PostStatus.DRAFT: [self.draft_post],
            PostStatus.DELETED: [self.deleted_post],
        }
        result = self._fetch_posts_feed(
            client, {"username": self.poster.username, "status": status_param}
        )
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        if logged_in_as_poster:
            expected_posts = posts_mapping[status_param]
        else:
            expected_posts = posts_mapping[PostStatus.PUBLISHED]
            if status_param not in ("", PostStatus.PUBLISHED):
                expected_posts = []

        assert len(posts) == len(expected_posts)
        assert set(lpluck("uuid", posts)) == set(
            map(str, lpluck_attr("uuid", expected_posts))
        )
