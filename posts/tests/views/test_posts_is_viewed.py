import pytest
from funcy import lpluck
from rest_framework import status

from posts.choices import PostFeedType, PostStatus
from posts.tests.factories import PostFactory, PostViewFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestFilterByViewedStatus(PostsApiActionsMixin):
    def setup_method(self):
        self.user = UserPublicFactory()
        # Создаем посты, один из которых будет просмотрен пользователем
        self.viewed_post = PostFactory(status=PostStatus.PUBLISHED)
        self.unviewed_post = PostFactory(status=PostStatus.PUBLISHED)
        self.draft_post = PostFactory(status=PostStatus.DRAFT)
        self.deleted_post = PostFactory(status=PostStatus.DELETED)
        PostViewFactory(post=self.viewed_post, user=self.user)

    def test_returns_only_viewed_posts_for_authenticated_user(self, authed_api_client):
        client = authed_api_client(self.user)
        result = self._fetch_posts_feed(client, {"is_viewed": "true"})
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        assert len(posts) == 1
        assert posts[0]["slug"] == self.viewed_post.slug

    def test_returns_only_unviewed_posts_for_authenticated_user(self, authed_api_client):
        client = authed_api_client(self.user)
        result = self._fetch_posts_feed(client, {"is_viewed": "false"})
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        assert len(posts) == 1
        assert posts[0]["slug"] == self.unviewed_post.slug

    def test_returns_all_posts_for_authenticated_user_when_is_viewed_not_set(
        self, authed_api_client
    ):
        client = authed_api_client(self.user)
        result = self._fetch_posts_feed(client, {})
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        post_slugs = lpluck("slug", posts)
        assert self.viewed_post.slug in post_slugs
        assert self.unviewed_post.slug in post_slugs
        assert self.draft_post.slug not in post_slugs
        assert self.deleted_post.slug not in post_slugs

    def test_does_not_apply_viewed_filter_for_anonymous_user(self, anon_api_client):
        result = self._fetch_posts_feed(anon_api_client(), {"is_viewed": "true"})
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        assert {self.viewed_post.slug, self.unviewed_post.slug} == set(
            lpluck("slug", posts)
        )

    @pytest.mark.parametrize("feed_type", [PostFeedType.VIEWED])
    @pytest.mark.parametrize("is_viewed_value", ["true", "false", None])
    def test_returns_only_viewed_posts_for_viewed_regardless_independ_value(
        self, feed_type, is_viewed_value, authed_api_client
    ):
        client = authed_api_client(self.user)
        # Преобразуем значение feed_type в строковое представление
        # для использования в запросе
        feed_type_value = feed_type.value
        request_params = {"feed_type": feed_type_value}
        if is_viewed_value is not None:
            request_params["is_viewed"] = is_viewed_value

        result = self._fetch_posts_feed(client, request_params)
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        # Проверяем, что вне зависимости от значения is_viewed возвращаются только
        # просмотренные посты
        assert len(posts) == 1
        assert posts[0]["slug"] == self.viewed_post.slug
