import reversion
from django.contrib import admin
from django.db.models import JSONField
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html
from jsoneditor.forms import JSONEditor
from pydantic import TypeAdapter
from reversion.admin import VersionAdmin

from common.content.blocks.common import PostContentBlocks
from posts.choices import PostStatus
from posts.forms import PostSeriesMembershipForm
from posts.models import (
    Announcement,
    Post,
    PostAnonymMapping,
    PostSeries,
    PostSeriesMembership,
)


@admin.register(Post)
class PostAdmin(VersionAdmin):
    formfield_overrides = {
        JSONField: {
            "widget": JSONEditor(
                jsonschema=TypeAdapter(PostContentBlocks).json_schema(),
                attrs={"style": "min-height: 600px"},
            )
        },
    }
    list_display = (
        "title",
        "comments",
        "status",
        "published_at",
        "created_at",
        "views_count",
        "user",
        "series_name",
    )

    # list_select_related = ("user", "series")
    list_select_related = ("user",)

    raw_id_fields = (
        "user",
        "tags",
    )

    list_filter = (
        "status",
        "published_at",
        "created_at",
    )

    ordering = ("-id",)
    search_fields = (
        "title",
        "slug",
        "content",
        "user__username",
        "user__email",
        "tags__name",
    )

    readonly_fields = (
        "views_count",
        "comments_count",
        "votes_up_count",
        "votes_down_count",
        "rating",
        "series_name",
    )

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions.pop("delete_selected", None)
        return actions

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset.prefetch_related(
            "comments",
            "tags",
            # "series"
        )

    def comments(self, obj):
        return format_html(
            "<a href='{}'>комменты ({})<a>",
            reverse("admin:comments_comment_changelist") + f"?post_id={obj.pk}",
            obj.comments.count(),
        )

    def has_add_permission(self, request):
        return False

    def delete_model(self, request, obj: Post):
        with reversion.create_revision():
            obj.status = PostStatus.DELETED
            obj.save()
            reversion.set_user(request.user)
            reversion.set_comment("Пост был удален из админки")

    def delete_queryset(self, request, queryset):
        return None

    def series_name(self, obj):
        """Отображение информации о серии в списке постов."""
        series = obj.get_post_series()
        if series:
            return format_html(
                "<a href='{}'>{}</a>",
                reverse("admin:posts_postseries_change", args=[series["id"]]),
                series["name"],
            )
        return "Нет серии"

    series_name.short_description = "Серия"


@admin.register(PostAnonymMapping)
class PostAnonymMappingAdmin(VersionAdmin):
    list_display = (
        "post",
        "autor",
    )

    list_select_related = (
        "post",
        "autor",
    )

    raw_id_fields = ("autor",)

    list_filter = ("autor__username",)

    ordering = ("-id",)
    search_fields = (
        "post__title",
        "post__slug",
        "post__content",
        "autor__username",
        "autor__email",
        "tags__name",
    )

    readonly_fields = ("autor",)

    def has_add_permission(self, request):
        return False

    def delete_queryset(self, request, queryset):
        return None


@admin.register(Announcement)
class AnnouncementAdmin(admin.ModelAdmin):
    list_display = ("post", "priority", "is_currently_active", "start_date", "end_date")
    list_filter = ("is_active", "start_date", "end_date", "priority")
    search_fields = ("post__title",)
    raw_id_fields = ("post",)
    readonly_fields = ("is_currently_active",)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset.select_related("post").order_by("priority", "start_date")

    def has_delete_permission(self, request, obj=None):
        return True

    def save_model(self, request, obj, form, change):
        if not obj.is_active and not obj.end_date:
            obj.end_date = timezone.now()
        super().save_model(request, obj, form, change)


@admin.register(PostSeries)
class PostSeriesAdmin(admin.ModelAdmin):
    list_display = ("name", "user", "created_at", "avatar", "posts_count")
    list_filter = ("created_at",)
    search_fields = (
        "name",
        "user__username",
    )
    raw_id_fields = ("user",)
    readonly_fields = ("created_at", "posts_count")

    def posts_count(self, obj):
        """Возвращает количество постов в серии."""
        return obj.memberships.count()

    posts_count.short_description = "Количество постов"

    def get_queryset(self, request):
        """Добавляем аннотации для подсчёта количества постов."""
        queryset = super().get_queryset(request)
        return queryset.prefetch_related("memberships__post")

    def has_delete_permission(self, request, obj=None):
        """Разрешаем удаление серий."""
        return True


@admin.register(PostSeriesMembership)
class PostSeriesMembershipAdmin(VersionAdmin):
    form = PostSeriesMembershipForm

    list_display = (
        "series",
        "post",
        "added_at",
    )

    list_select_related = ("series",)
    raw_id_fields = (
        "series",
        "post",
    )
    list_filter = ("added_at",)
    ordering = ("-added_at",)
    search_fields = (
        "series__name",
        "series__user__username",
        "post__title",  # Исправляем поиск по постам
    )

    readonly_fields = ("added_at",)

    def get_queryset(self, request):
        """Оптимизация запросов."""
        queryset = super().get_queryset(request)
        return queryset.select_related("series", "series__user")
