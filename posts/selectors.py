from functools import wraps
from typing import Type

from django.conf import settings
from django.db.models import F, FloatField, Q, QuerySet, Value
from django.db.models.functions import Cast
from django.http import QueryDict
from django.utils import timezone
from django_filters.rest_framework import FilterSet
from funcy import count_by
from pydantic import ValidationError

from comments.models import Comment
from common.content.blocks.common import PostContentBlocks
from common.content.blocks.context import init_blocks_context
from common.content.blocks.image import BlockImage
from common.content.blocks.video import BlockVideo
from lists.selectors import (
    fetch_tag_ignores,
    fetch_tag_subscriptions,
    fetch_user_ignores,
    fetch_user_subscriptions,
)
from posts.choices import PostStatus, TimePeriod
from posts.helpers import time_period_to_timedelta
from posts.models import Post, PostVote
from posts.querysets import PostQuerySet
from tags.models import Tag
from users.models import UserPublic
from users.selectors import can_moderate_posts


def hide_posts_with_adult_tags(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        qs_is_none = kwargs.get("queryset")
        result = func(*args, **kwargs)
        viewer = kwargs.get(
            "viewer", args[0] if len(args) and isinstance(args[0], UserPublic) else None
        )
        if isinstance(qs_is_none, type(None)) and viewer is not None:
            result = apply_exclude_tags(result, viewer)

        return result

    return wrapper


def hide_viewed_posts_by_user_settings(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        viewer = kwargs.get(
            "viewer", args[0] if len(args) and isinstance(args[0], UserPublic) else None
        )
        if viewer is not None:
            result = apply_exclude_viewed_post_by_user_settings(result, viewer)
        return result

    return wrapper


def hide_posts_by_tags_by_user_settings(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        viewer = kwargs.get(
            "viewer", args[0] if len(args) and isinstance(args[0], UserPublic) else None
        )
        if viewer is not None:
            result = apply_exclude_post_by_tags_by_user_settings(result, viewer)
        return result

    return wrapper


def fetch_all_posts(viewer: UserPublic) -> PostQuerySet:
    return Post.objects.order_by("-created_at").with_ratings(viewer)


def fetch_posts(viewer: UserPublic, with_ratings: bool = True) -> PostQuerySet:
    """Fetch pots which were added recently."""
    result = (
        Post.published.select_related("user")
        .only(
            "uuid",
            "votes_up_count",
            "status",
            "is_anonym",
            "votes_down_count",
            "rating",
            "content",
            "views_count",
            "comments_count",
            "user__id",
            "user__is_active",
            "user__is_verified",
            "user__avatar",
            "user__username",
            "title",
            "slug",
            "published_at",
        )
        .order_by("-published_at")
    )
    if viewer.is_authenticated:
        result = apply_ignore_list(result, viewer)
    if with_ratings:
        result = result.with_ratings(viewer)
    result = apply_exclude_tags(result, viewer)
    return result


@hide_viewed_posts_by_user_settings
@hide_posts_by_tags_by_user_settings
@hide_posts_with_adult_tags
def fetch_new_posts(
    viewer: UserPublic,
    queryset: PostQuerySet = None,
    default_period: bool = False,
) -> QuerySet:
    """Fetch posts with a lot of comments."""
    if queryset is None:
        queryset = fetch_posts(viewer)
    if default_period:
        queryset = _filter_by_time_period(queryset, TimePeriod.MONTH)
    return queryset


def apply_ignore_list(queryset: PostQuerySet, viewer: UserPublic) -> PostQuerySet:
    ignored_users_pks = list(fetch_user_ignores(viewer).values_list("user_id", flat=True))
    ignored_tags_pks = list(fetch_tag_ignores(viewer).values_list("tag_id", flat=True))
    if ignored_users_pks:
        queryset = queryset.exclude(user_id__in=ignored_users_pks)
    if ignored_tags_pks:
        queryset = queryset.exclude(tags__id__in=ignored_tags_pks)
    return queryset


def get_user_restricted_tag_names(user: UserPublic = None) -> set[str]:
    restricted_tags = set(get_restricted_tag_names_for_unautorized_and_less_18())
    if not user or not user.is_authenticated:
        return restricted_tags
    if user.age < settings.MINIMAL_AGE_FOR_NSFW or not user.is_verified:
        return restricted_tags
    return set()


def apply_exclude_tags(queryset: PostQuerySet, viewer: UserPublic) -> QuerySet:
    restricted_tag_names = get_user_restricted_tag_names(viewer)
    restricted_tag_ids = set(
        Tag.objects.filter(name__in=restricted_tag_names).values_list("id", flat=True)
    )

    queryset = queryset.exclude(tags__id__in=restricted_tag_ids)

    return queryset


def apply_exclude_post_by_tags_by_user_settings(
    queryset: PostQuerySet, viewer: UserPublic
) -> QuerySet:
    if not viewer.is_anonymous and not viewer.show_nsfw:
        queryset = queryset.exclude(tags__name__in=["NSFW"])
    if not viewer.is_anonymous and not viewer.show_hardcore:
        queryset = queryset.exclude(tags__name__in=["Жесть"])
    if not viewer.is_anonymous and not viewer.show_politics:
        queryset = queryset.exclude(tags__name__in=["Политика"])
    return queryset


def apply_exclude_viewed_post_by_user_settings(
    queryset: PostQuerySet, viewer: UserPublic
) -> QuerySet:
    if not viewer.is_anonymous and not viewer.show_viewed:
        queryset = queryset.exclude(views__user=viewer).distinct()

    return queryset


def fetch_my_posts(user: UserPublic) -> QuerySet:
    """Fetch all user own posts, in all statuses."""
    return (
        Post.objects.prefetch_related("tags")
        .select_related("user")
        .filter(user_id=user.pk)
        .with_ratings(user)
        .order_by(F("published_at").desc(nulls_last=True), "-created_at")
    )


@hide_posts_with_adult_tags
@hide_posts_by_tags_by_user_settings
def fetch_user_posts(
    user: UserPublic, viewer: UserPublic, queryset: PostQuerySet = None
) -> QuerySet:
    """Fetch all posts from the user."""
    queryset = fetch_posts(viewer) if queryset is None else queryset
    return queryset.filter(user_id=user.pk)


@hide_viewed_posts_by_user_settings
@hide_posts_by_tags_by_user_settings
@hide_posts_with_adult_tags
def fetch_rating_posts(
    viewer: UserPublic,
    queryset: PostQuerySet = None,
    default_period: bool = False,
) -> QuerySet:
    """Fetch posts with the highest rating."""
    if queryset is None:
        queryset = fetch_posts(viewer)
    if default_period:
        queryset = _filter_by_time_period(queryset, TimePeriod.MONTH)
    return (
        queryset.annotate(
            _positive_ratio=Cast("votes_up_count", FloatField())
            / (F("votes_up_count") + F("votes_down_count"))
        ).filter(
            votes_up_count__gte=get_min_positive_votes_for_top_rating_feed(),
            _positive_ratio__gte=(
                get_min_positive_votes_percentage_for_top_rating_feed() / 100.0
            ),
        )
    ).order_by("-_positive_ratio")


@hide_viewed_posts_by_user_settings
@hide_posts_by_tags_by_user_settings
@hide_posts_with_adult_tags
def fetch_trends_posts(
    viewer: UserPublic,
    queryset: PostQuerySet = None,
    default_period: bool = False,
) -> QuerySet:
    """Fetch posts with the highest rating."""
    if queryset is None:
        queryset = fetch_posts(viewer)
    if default_period:
        queryset = _filter_by_time_period(queryset, TimePeriod.MONTH)
    return (
        queryset.annotate(
            _positive_ratio=Cast("votes_up_count", FloatField())
            / (F("votes_up_count") + F("votes_down_count"))
        ).filter(
            votes_up_count__gte=get_min_positive_votes_for_trends_rating_feed(),
            _positive_ratio__gte=(
                get_min_positive_votes_percentage_for_trends_rating_feed() / 100.0
            ),
        )
    ).order_by("-_positive_ratio")


@hide_viewed_posts_by_user_settings
@hide_posts_by_tags_by_user_settings
@hide_posts_with_adult_tags
def fetch_commented_posts(
    viewer: UserPublic,
    queryset: PostQuerySet = None,
    default_period: bool = False,
) -> QuerySet:
    """Fetch posts with a lot of comments."""
    if queryset is None:
        queryset = fetch_posts(viewer)
    if default_period:
        queryset = _filter_by_time_period(queryset, TimePeriod.WEEK)

    return queryset.order_by("-comments_count")


@hide_posts_with_adult_tags
@hide_posts_by_tags_by_user_settings
def fetch_viewed_posts(viewer: UserPublic, *args, **kwargs) -> QuerySet:
    if not viewer.is_authenticated:
        # Need to annotate even empty queryset with viewed_at for ordering to work
        return Post.objects.annotate(viewed_at=Value(timezone.now())).none()

    return (
        Post.published.with_ratings(viewer)
        .filter(views__user=viewer)
        # Need annotation there for sorting in the filter and then
        # to use this field in paginator
        .annotate(viewed_at=F("views__viewed_at"))
        .distinct()
    )


@hide_posts_with_adult_tags
@hide_posts_by_tags_by_user_settings
def fetch_saved_posts(viewer: UserPublic, *args, **kwargs) -> QuerySet:
    if not viewer.is_authenticated:
        # Need to annotate even empty queryset with viewed_at for ordering to work
        return Post.objects.none()

    return (
        Post.published.filter(saved__user=viewer)
        # Need annotation there for sorting in the filter and then
        # to use this field in paginator
        .annotate(saved_at=F("saved__saved_at")).distinct()
    )


@hide_posts_with_adult_tags
@hide_viewed_posts_by_user_settings
@hide_posts_by_tags_by_user_settings
def fetch_subscribed_posts(
    viewer: UserPublic,
    with_ratings: bool = True,
    with_comments_count: bool = True,
    *args,
    **kwargs,
) -> QuerySet:
    if not viewer.is_authenticated:
        return Post.objects.none()
    subscription_user_pks = list(
        fetch_user_subscriptions(viewer).values_list("user_id", flat=True)
    )
    subscription_tag_pks = list(
        fetch_tag_subscriptions(viewer).values_list("tag_id", flat=True)
    )

    return (
        fetch_posts(viewer, with_ratings=with_ratings)
        .filter(
            Q(user_id__in=subscription_user_pks) | Q(tags__id__in=subscription_tag_pks)
        )
        .distinct()
    )


@hide_posts_with_adult_tags
@hide_viewed_posts_by_user_settings
@hide_posts_by_tags_by_user_settings
def fetch_copyright_posts(
    viewer: UserPublic,
    *args,
    **kwargs,
) -> QuerySet:
    tags_copyright = list(Tag.objects.filter(name__in=("[Авторское]", "[Своё]")))
    return fetch_posts(viewer).filter(tags__in=tags_copyright, rating__gte=0).distinct()


def get_count_of_new_items_in_subscription_feed(user: UserPublic) -> int:
    try:
        subscriptions_feed_viewed_at = user.context.subscriptions_feed_viewed_at
    except AttributeError:
        subscriptions_feed_viewed_at = None

    result = fetch_subscribed_posts(user, with_ratings=False, with_comments_count=False)
    if subscriptions_feed_viewed_at:
        result = result.filter(published_at__gt=subscriptions_feed_viewed_at)

    return result.distinct().count()


def get_post_editable_window_minutes(user: UserPublic) -> int:
    """Return for how many minutes since publishing post can be edited."""
    return settings.POST_EDITABLE_WINDOW_MINUTES


def get_post_vote_value_for_author(author: UserPublic, post_vote: PostVote) -> float:
    """Get value of how much rating post author should get on a single post vote."""
    return post_vote.value * settings.POST_RATING_MULTIPLIER


def can_edit_post(user: UserPublic, post: Post) -> bool:
    # Check if user is moderator and then allow to edit any post.
    if can_moderate_posts(user):
        return True
    if user and user != post.user:
        return False
    if post.status != PostStatus.PUBLISHED or not post.published_at:
        return True
    now = timezone.now()
    return (now - post.published_at).total_seconds() <= get_post_editable_window_minutes(
        user
    ) * 60


def can_vote_for_post(user: UserPublic, post: Post) -> bool:
    # TODO: implement
    return True


def can_create_post(user: UserPublic) -> bool:
    # TODO: implement
    return True


def get_min_positive_votes_for_trends_rating_feed() -> int:
    return settings.POST_MIN_POSITIVE_VOTES_FOR_TRENDS_RATING_FEED


def get_min_positive_votes_percentage_for_trends_rating_feed() -> int:
    return settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TRENDS_RATING_FEED


def get_min_positive_votes_for_top_rating_feed() -> int:
    return settings.POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED


def get_restricted_tag_names_for_unautorized_and_less_18() -> list:
    return settings.RESTRICTED_TAG_NAMES_FOR_UNAUTORIZED_AND_LESS_18


def get_min_positive_votes_percentage_for_top_rating_feed() -> int:
    return settings.POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED


def get_tag_for_telegram() -> Tag:
    return Tag.objects.get(name="[телеграм]")


def get_tag_for_anonym() -> Tag:
    return Tag.objects.get(name="Анонимные посты")


def get_tag_for_answer() -> Tag:
    return Tag.objects.get(name="Ответ на пост")


def get_samples_for_list_cache(query_params: QueryDict) -> list:
    return [
        Post.published.all(),
        PostVote.objects.all(),
        Comment.objects.all(),
    ]


def get_extras_for_list_cache(  # noqa: CCR001
    user: UserPublic,
    query_params: QueryDict,
    filterset_class: Type[FilterSet] | None = None,
) -> str:
    extras = []

    if cursor := query_params.get("cursor"):
        extras.append(f"cursor:{cursor}")

    if filterset_class:
        for filter_name in filterset_class.get_filters():
            if (filter_value := query_params.get(filter_name)) is not None:
                extras.append(f"{filter_name}:{filter_value}")

    if user.is_authenticated:
        extras.append(f"u:{user.pk}")
        if PostVote.objects.filter(user=user).exists():
            extras.append("voted")
    else:
        extras.append("u:0")

    return "|".join(extras)


def get_post_images_count(post: Post) -> int:
    """Возвращает количество блоков с изображениями из контента поста."""
    try:
        with init_blocks_context({"sanitize": False}):
            content = PostContentBlocks(**post.content, should_validate=False)
    except ValidationError:
        return 0
    blocks_count = count_by(type, content.blocks)
    return blocks_count[BlockImage]


def get_post_videos_count(post: Post) -> int:
    """Возвращает количество блоков с видео из контента поста."""
    try:
        with init_blocks_context({"sanitize": False}):
            content = PostContentBlocks(**post.content, should_validate=False)
    except ValidationError:
        return 0
    blocks_count = count_by(type, content.blocks)
    return blocks_count[BlockVideo]


def _filter_by_time_period(queryset: PostQuerySet, time_period: TimePeriod | None = None):
    if time_period is None or time_period == TimePeriod.ALL:
        return queryset
    from_datetime = timezone.now() - time_period_to_timedelta(time_period)
    return queryset.filter(published_at__gte=from_datetime)
