import json
import logging
import re

from posts.models import Post
from posts.selectors import get_tag_for_anonym, get_tag_for_answer, get_tag_for_telegram
from tags.models import Tag

TELEGRAM_REGEX = re.compile(r"//([a-zA-Z0-9_-]+\.)?t(elegram)?.(me(/)?|dog/)")

logger = logging.getLogger(__name__)


class PostEvaluator:
    def __init__(self, post: Post):
        self.post = post

    def run(self) -> None:
        self.autoassign_tags()

    def autoassign_tags(self) -> None:
        self._process_telegram_tag()
        self._process_anonym_tag()
        self._process_answer_tag()

    def _process_telegram_tag(self) -> None:
        try:
            telegram_tag = get_tag_for_telegram()
        except Tag.DoesNotExist:
            logger.error(
                "Cannot autotag post %s with telegram links, since tag for "
                "telegram doesn't exist",
                self.post,
            )
            return

        if self._has_telegram_links():
            self.post.tags.add(telegram_tag)
        else:
            self.post.tags.remove(telegram_tag)

    def _process_anonym_tag(self) -> None:
        try:
            anonym_tag = get_tag_for_anonym()
        except Tag.DoesNotExist:
            logger.error(
                "Cannot autotag post %s with anonym sign, since tag for "
                "anonym doesn't exist",
                self.post,
            )
            return

        if self.post.is_anonym:
            self.post.tags.add(anonym_tag)
        else:
            self.post.tags.remove(anonym_tag)

    def _process_answer_tag(self) -> None:
        try:
            answer_tag = get_tag_for_answer()
        except Tag.DoesNotExist:
            logger.error(
                "Cannot autotag post %s with answer sign, since tag for "
                "answer doesn't exist",
                self.post,
            )
            return

        if self.post.is_answer:
            self.post.tags.add(answer_tag)

    def _has_telegram_links(self) -> re.Match[str] | None:
        # ? Replace with contextlib.suppress(AttributeError):
        try:
            flatten_content = " ".join(
                [
                    json.dumps(block.get("data", ""))
                    for block in self.post.content.get("blocks", [])
                ]
            )
            return TELEGRAM_REGEX.search(flatten_content)
        except AttributeError:
            pass
