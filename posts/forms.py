from django import forms

from posts.models import Post, PostSeriesMembership


class PostSeriesMembershipForm(forms.ModelForm):
    """Форма для фильтрации постов по автору серии."""

    class Meta:
        model = PostSeriesMembership
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Проверяем, есть ли уже выбранная серия
        series = self.instance.series if self.instance and self.instance.pk else None

        if series:
            # Ограничиваем выбор постов автором серии
            self.fields["post"].queryset = Post.objects.filter(user=series.user)
        else:
            # Если серия не выбрана, запрещаем выбор постов
            self.fields["post"].queryset = Post.objects.none()
