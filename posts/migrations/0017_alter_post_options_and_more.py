# Generated by Django 4.2.6 on 2023-10-17 01:59

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("posts", "0016_remove_post_image_remove_post_video"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="post",
            options={"verbose_name": "Пост", "verbose_name_plural": "Посты"},
        ),
        migrations.AddIndex(
            model_name="post",
            index=models.Index(
                fields=["user", "status"], name="posts_post_user_id_8bb7a9_idx"
            ),
        ),
    ]
