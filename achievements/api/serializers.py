from rest_framework import serializers

from achievements.models import Achievement, UserAchievement


class AchievementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Achievement
        fields = (
            "id",
            "name",
            "short_description",
            "long_description",
            "prefix",
            "image_url",
            "type",
        )


class UserAchievementSerializer(serializers.ModelSerializer):
    achievement = AchievementSerializer()

    class Meta:
        model = UserAchievement
        fields = ("id", "achievement", "received_at", "is_rejected", "is_selected_prefix")
