from rest_access_policy import AccessPolicy, Statement

from users.selectors import can_moderate_posts


class AchievementAccessPolicy(AccessPolicy):
    statements = [
        Statement(
            action=["list", "retrieve", "achievements_by_user", "<safe_methods>"],
            principal="*",
            effect="allow",
        ),
        Statement(
            action=[
                "select_prefix",
                "reject",
                "check_achievements",
                "available_achievements",
            ],
            principal="authenticated",
            effect="allow",
            condition=["user_must_be_author"],
        ),
    ]

    def user_must_be_author(self, request, view, action) -> bool:
        user = request.user
        if can_moderate_posts(user):
            return True
        entity = view.get_object()
        return entity.user == user
