from django.contrib import admin

from achievements.models import Achievement, UserAchievement


@admin.register(Achievement)
class AchievementAdmin(admin.ModelAdmin):
    list_display = ("name", "type_achieve", "prefix", "image_url")
    list_filter = ("type_achieve",)
    search_fields = ("name", "short_description")
    ordering = ("name",)


@admin.register(UserAchievement)
class UserAchievementAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "achievement",
        "received_at",
        "is_rejected",
        "is_selected_prefix",
    )
    list_filter = ("is_rejected", "is_selected_prefix", "received_at")
    search_fields = ("user__username", "achievement__name")
    raw_id_fields = ("user", "achievement")
    ordering = ("-received_at",)
