# Generated by Django 5.0 on 2025-01-04 23:37

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="Achievement",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "name",
                    models.CharField(max_length=100, verbose_name="Название достижения"),
                ),
                (
                    "short_description",
                    models.CharField(max_length=255, verbose_name="Краткое описание"),
                ),
                (
                    "long_description",
                    models.TextField(blank=True, verbose_name="Длинное описание"),
                ),
                (
                    "prefix",
                    models.CharField(
                        blank=True, max_length=50, null=True, verbose_name="Префикс"
                    ),
                ),
                (
                    "image_url",
                    models.URLField(blank=True, default="", verbose_name="Изображение"),
                ),
                (
                    "type_achieve",
                    models.CharField(
                        choices=[
                            ("progress", "Прогресс"),
                            ("regular", "Регулярная"),
                            ("epic", "Эпическая"),
                            ("individual", "Индивидуальная"),
                            ("other", "Другая"),
                        ],
                        default="other",
                        max_length=20,
                        verbose_name="Тип достижения",
                    ),
                ),
                ("comment", models.TextField(blank=True, verbose_name="Комментарий")),
                (
                    "condition",
                    models.JSONField(
                        blank=True,
                        help_text='JSON-объект с условиями (например, {"comments": 10})',
                        null=True,
                        verbose_name="Условия достижения",
                    ),
                ),
            ],
            options={
                "verbose_name": "Достижение",
                "verbose_name_plural": "Достижения",
            },
        ),
        migrations.CreateModel(
            name="UserAchievement",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "received_at",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="Дата получения"
                    ),
                ),
                (
                    "is_rejected",
                    models.BooleanField(default=False, verbose_name="Отклонено"),
                ),
                (
                    "is_selected_prefix",
                    models.BooleanField(default=False, verbose_name="Выбранный префикс"),
                ),
                (
                    "achievement",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="user_achievements",
                        to="achievements.achievement",
                        verbose_name="Достижение",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="achievements",
                        to=settings.AUTH_USER_MODEL,
                        verbose_name="Пользователь",
                    ),
                ),
            ],
            options={
                "verbose_name": "Достижение пользователя",
                "verbose_name_plural": "Достижения пользователя",
                "unique_together": {("user", "achievement")},
            },
        ),
        migrations.CreateModel(
            name="UserAchievementProgress",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "progress",
                    models.JSONField(
                        blank=True,
                        help_text='JSON-объект с прогрессом (например, {"comments": 7})',
                        null=True,
                        verbose_name="Прогресс",
                    ),
                ),
                (
                    "is_completed",
                    models.BooleanField(default=False, verbose_name="Выполнено"),
                ),
                (
                    "achievement",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="progress",
                        to="achievements.achievement",
                        verbose_name="Достижение",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="achievement_progress",
                        to=settings.AUTH_USER_MODEL,
                        verbose_name="Пользователь",
                    ),
                ),
            ],
            options={
                "verbose_name": "Прогресс достижения пользователя",
                "verbose_name_plural": "Прогресс достижений пользователей",
                "unique_together": {("user", "achievement")},
            },
        ),
    ]
