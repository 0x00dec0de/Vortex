from django.db import models

from users.models import UserPublic


class AchievementType(models.TextChoices):
    PROGRESS = "progress", "Прогресс"
    REGULAR = "regular", "Регулярная"
    EPIC = "epic", "Эпическая"
    INDIVIDUAL = "individual", "Индивидуальная"
    OTHER = "other", "Другая"


class Achievement(models.Model):
    """Модель для описания достижений."""

    name = models.CharField(max_length=100, verbose_name="Название достижения")
    short_description = models.CharField(max_length=255, verbose_name="Краткое описание")
    long_description = models.TextField(blank=True, verbose_name="Длинное описание")
    prefix = models.CharField(
        max_length=50, blank=True, null=True, verbose_name="Префикс"
    )
    image_url = models.URLField(blank=True, default="", verbose_name="Изображение")
    type_achieve = models.CharField(
        max_length=20,
        choices=AchievementType.choices,
        default=AchievementType.OTHER,
        verbose_name="Тип достижения",
    )
    comment = models.TextField(blank=True, verbose_name="Комментарий")
    condition = models.JSONField(
        blank=True,
        null=True,
        verbose_name="Условия достижения",
        help_text='JSON-объект с условиями (например, {"comments": 10})',
    )

    class Meta:
        verbose_name = "Достижение"
        verbose_name_plural = "Достижения"

    def __str__(self):
        return self.name


class UserAchievement(models.Model):
    """Модель для отслеживания достижений пользователя."""

    user = models.ForeignKey(
        UserPublic,
        on_delete=models.CASCADE,
        related_name="achievements",
        verbose_name="Пользователь",
    )
    achievement = models.ForeignKey(
        Achievement,
        on_delete=models.CASCADE,
        related_name="user_achievements",
        verbose_name="Достижение",
    )
    received_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата получения")
    is_rejected = models.BooleanField(default=False, verbose_name="Отклонено")
    is_selected_prefix = models.BooleanField(
        default=False, verbose_name="Выбранный префикс"
    )

    class Meta:
        verbose_name = "Достижение пользователя"
        verbose_name_plural = "Достижения пользователя"
        unique_together = ("user", "achievement")

    def __str__(self):
        return f"{self.user.username} — {self.achievement.name}"


class UserAchievementProgress(models.Model):
    """Модель для отслеживания прогресса пользователя по достижениям."""

    user = models.ForeignKey(
        UserPublic,
        on_delete=models.CASCADE,
        related_name="achievement_progress",
        verbose_name="Пользователь",
    )
    achievement = models.ForeignKey(
        Achievement,
        on_delete=models.CASCADE,
        related_name="progress",
        verbose_name="Достижение",
    )
    progress = models.JSONField(
        blank=True,
        null=True,
        verbose_name="Прогресс",
        help_text='JSON-объект с прогрессом (например, {"comments": 7})',
    )
    is_completed = models.BooleanField(default=False, verbose_name="Выполнено")

    class Meta:
        verbose_name = "Прогресс достижения пользователя"
        verbose_name_plural = "Прогресс достижений пользователей"
        unique_together = ("user", "achievement")

    def __str__(self):
        return f"{self.user.username} — {self.achievement.name} (Прогресс)"
