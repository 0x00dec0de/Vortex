import sys
from io import BytesIO

from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile
from PIL import Image, ImageOps
from PIL.Image import Resampling

from uploads.exceptions import FileTooBigException
from uploads.helpers import is_apng


def resize_image_from_inmemory_file(
    image: InMemoryUploadedFile, to_size: tuple[int, int]
) -> InMemoryUploadedFile:
    # Don't resize gif and apng, just keep it as is
    if image.content_type == "image/gif" or (
        image.content_type == "image/png" and is_apng(image.read())
    ):
        if image.size > settings.IMAGE_MAX_GIF_APNG_SIZE_BYTES:
            raise FileTooBigException(
                f"Слишком большой файл, разрешено загружать файлы не "
                f"более {settings.IMAGE_MAX_GIF_APNG_SIZE_BYTES / 1024 / 1024} Мб"
            )
        image.seek(0)
        return image
    image.seek(0)
    img = Image.open(image)
    # This is to have image orientation as in EXIF
    ImageOps.exif_transpose(img, in_place=True)
    img.thumbnail(size=to_size, resample=Resampling.LANCZOS)
    img_io = BytesIO()
    img.save(img_io, format="webp")

    return InMemoryUploadedFile(
        img_io,
        "image",
        f"{image.name}.webp",
        "image/webp",
        sys.getsizeof(img_io),
        image.charset,
    )
