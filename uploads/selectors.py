from uploads.choices import ImageType
from uploads.constants import MAX_IMG_SIZE_FOR_AVATAR, MAX_IMG_SIZE_FOR_POST


def get_image_size(image_type: ImageType = ImageType.POST) -> tuple[int, int]:
    size_mapping = {
        ImageType.POST: MAX_IMG_SIZE_FOR_POST,
        ImageType.AVATAR: MAX_IMG_SIZE_FOR_AVATAR,
        ImageType.SERIES: MAX_IMG_SIZE_FOR_AVATAR,
    }
    return size_mapping.get(image_type, MAX_IMG_SIZE_FOR_POST)
