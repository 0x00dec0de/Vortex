from rest_framework import serializers

from uploads.models import Image
from uploads.selectors import get_image_size
from uploads.services import resize_image_from_inmemory_file


class ImageCreateSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    image = serializers.ImageField()

    class Meta:
        model = Image
        fields = (
            "user",
            "image",
            "image_type",
        )

    def validate(self, attrs):
        attrs["image"] = resize_image_from_inmemory_file(
            image=attrs["image"],
            to_size=get_image_size(attrs.get("image_type")),
        )
        return attrs


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = (
            "uuid",
            "image",
            "width",
            "height",
        )
