from rest_framework.routers import SimpleRouter

from uploads.api.v1 import views

app_name = "uploads"
router = SimpleRouter()

router.register("", views.UploadImageView, basename="uploads")

urlpatterns = router.urls
