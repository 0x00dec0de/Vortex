from rest_framework import mixins, parsers, status, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from uploads.api.serializers import ImageCreateSerializer, ImageSerializer
from uploads.models import Image


class UploadImageView(viewsets.GenericViewSet, mixins.CreateModelMixin):
    parser_classes = (
        parsers.MultiPartParser,
        parsers.FormParser,
    )
    queryset = Image.objects.all()
    serializer_class = ImageCreateSerializer
    permission_classes = [IsAuthenticated]
    throttle_scope = "upload-image"

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        return Response(
            ImageSerializer(serializer.instance).data,
            status=status.HTTP_201_CREATED,
            headers=headers,
        )
