from django.db import models


class ImageType(models.TextChoices):
    POST = "post"
    AVATAR = "avatar"
    SERIES = "series"
