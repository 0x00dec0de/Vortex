from django.contrib import admin

from uploads.models import Image


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    raw_id_fields = ("user",)
    list_display = (
        "image",
        "width",
        "height",
        "image_type",
        "created_at",
        "user",
    )

    list_filter = ("image_type",)
    search_fields = ("user__username",)
