from rest_framework.exceptions import ValidationError


class FileTooBigException(ValidationError):
    """Исключение при загрузке слишком большого файла."""
