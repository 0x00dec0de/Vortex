# Generated by Django 4.2.6 on 2023-10-17 09:51

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models

import common.deconstructibles


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("uploads", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="image",
            name="user",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="images",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AlterField(
            model_name="image",
            name="image",
            field=models.ImageField(
                height_field="height",
                upload_to=common.deconstructibles.upload_to("%Y/%m/%d"),
                width_field="width",
            ),
        ),
    ]
