import uuid

from django.db import models

from common.deconstructibles import upload_to
from common.models import Timestamped
from uploads.choices import ImageType


class Image(Timestamped):
    user = models.ForeignKey(
        "users.UserPublic",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="images",
    )
    uuid = models.UUIDField(
        default=uuid.uuid4, unique=True, db_index=True, editable=False
    )
    image = models.ImageField(
        upload_to=upload_to("%Y/%m/%d"), width_field="width", height_field="height"
    )
    image_type = models.CharField(
        choices=ImageType.choices, max_length=6, default=ImageType.POST
    )
    width = models.PositiveSmallIntegerField(default=0)
    height = models.PositiveSmallIntegerField(default=0)

    class Meta:
        verbose_name = "Картинка"
        verbose_name_plural = "Картинки"
