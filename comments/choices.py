from django.db import models


class CommentVoteChoice(models.IntegerChoices):
    UPVOTE = 1
    DOWNVOTE = -1


class CommentStatus(models.TextChoices):
    PUBLISHED = "published", "Опубликован"
    DELETED = "deleted", "Удален"


class MyReplies(models.TextChoices):
    OLD = "1", "Все ответы"
    ALL = "all", "Все ответы"
    COMMENT_REPLIES = "comment-replies", "Ответы на комментарии"
    POST_REPLIES = "post-replies", "Комментарии к постам"
