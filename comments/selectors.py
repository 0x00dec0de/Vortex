import contextlib
from typing import Type

from django.conf import settings
from django.db.models import Q, QuerySet
from django.http import QueryDict
from django.utils import timezone
from django_filters.rest_framework import FilterSet
from mptt.querysets import TreeQuerySet

from comments.models import Comment, CommentVote
from posts.choices import PostStatus
from users.models import UserPublic
from users.selectors import can_moderate_comments  # , is_user_autobanned


def get_all_comments() -> TreeQuerySet[Comment]:
    """Просто возвращает все комментарии без каких либо аннотаций и сортировок."""
    return Comment.objects.all()


def get_all_comments_with_ratings(
    user: UserPublic, with_voted: bool = True
) -> TreeQuerySet[Comment]:
    """Возвращает комментарии, аннотированные рейтингом и флагом голосования для юзера."""
    return (
        Comment.objects.all()
        .with_ratings(user, with_voted)
        .select_related("user", "parent", "post")
        .order_by("created_at")
    )


def get_user_comments(user: UserPublic, with_voted: bool = True) -> TreeQuerySet[Comment]:
    """Возвращает все комментарии авторизованного юзера."""
    if not user.is_authenticated:
        return Comment.objects.none()
    return Comment.objects.filter(user=user).with_ratings(user, with_voted)


def get_replies_for_user_comments_and_posts(user: UserPublic) -> QuerySet[Comment]:
    """Возвращает ответы на комментарии и посты пользователя."""
    all_replies = get_replies_for_user_posts(user) | get_replies_for_user_comments(user)
    return all_replies.distinct().order_by("-created_at")


def get_replies_for_user_comments(user: UserPublic) -> QuerySet[Comment]:
    """Возвращает ответы на комментарии пользователя."""
    return (
        get_all_comments_with_ratings(user)
        .filter(post__status=PostStatus.PUBLISHED, parent__user=user)
        .exclude(user=user)
        .distinct()
        .order_by("-created_at")
    )


def get_replies_for_user_posts(user: UserPublic) -> QuerySet[Comment]:
    """Возвращает ответы на посты пользователя."""
    return (
        get_all_comments_with_ratings(user)
        .filter(post__status=PostStatus.PUBLISHED, post__user=user, parent__isnull=True)
        .exclude(user=user)
        .distinct()
        .order_by("-created_at")
    )


def get_user_default_comments_level(user: UserPublic) -> int:
    """Return deep comment  level."""
    # if user.is_authenticated and user.deep_tree_comment:
    #     return UserPublic.deep_tree_comment
    # return settings.COMMENTS_TREE_DEFAULT_LEVEL
    return 100


def get_comment_editable_window_minutes(user: UserPublic) -> int:
    """Return for how many minutes since posting comment can be edited."""
    return settings.COMMENTS_EDITABLE_WINDOW_MINUTES


def get_comment_vote_value_for_author(
    author: UserPublic, comment_vote: CommentVote
) -> float:
    """Get value of how much rating comment author should get on a single comment vote."""
    return comment_vote.value * settings.COMMENT_RATING_MULTIPLIER


def can_edit_comment(user: UserPublic, comment: Comment) -> bool:
    if can_moderate_comments(user):
        return True
    if user and user != comment.user:
        return False
    # if user.is_banned:
    #     return False
    now = timezone.now()
    comment_age_seconds = (now - comment.created_at).total_seconds()
    votes_exists = comment.votes_up_count or comment.votes_down_count
    posted_in_editable_window = (
        comment_age_seconds <= get_comment_editable_window_minutes(user) * 60
    )
    return not votes_exists and posted_in_editable_window


def can_hard_delete_comment(user: UserPublic, comment: Comment) -> bool:
    """Возвращает флаг, если комментарий может быть полностью удален из БД."""
    if not comment.is_leaf_node():
        return False
    if comment.votes.exists():
        return False
    if can_moderate_comments(user):
        return True
    # if user.is_banned or is_user_autobanned(user):
    #     return False
    return True


def get_samples_for_list_cache(query_params: QueryDict) -> list:
    post_slug = query_params.get("post")
    samples = [UserPublic.objects.all()]
    if post_slug:
        samples.extend(
            [
                Comment.objects.filter(post__slug=post_slug),
                CommentVote.objects.filter(comment__post__slug=post_slug),
            ]
        )
    return samples


def get_extras_for_list_cache(  # noqa: CCR001
    user: UserPublic,
    query_params: QueryDict,
    filterset_class: Type[FilterSet] | None = None,
) -> str:
    extras = [
        "v2",
        "o:" + query_params.get("ordering", ""),
    ]

    post_slug = query_params.get("post")

    if filterset_class:
        for filter_name in filterset_class.get_filters():
            if (filter_value := query_params.get(filter_name)) is not None:
                extras.append(f"{filter_name}:{filter_value}")

    if user.is_authenticated:
        extras.append(f"u:{user.pk}")
        if "replies" in query_params:
            with contextlib.suppress(AttributeError):
                extras.append(f"replies_viewed_at:{user.context.replies_feed_viewed_at}")
        if CommentVote.objects.filter(comment__post__slug=post_slug, user=user).exists():
            extras.append("voted")
    else:
        extras.append("u:0")

    return "|".join(extras)


def get_count_of_new_items_in_replies_feed(user: UserPublic) -> int:
    try:
        replies_feed_viewed_at = user.context.replies_feed_viewed_at
    except AttributeError:
        replies_feed_viewed_at = None

    result = (
        get_all_comments()
        .filter(post__status=PostStatus.PUBLISHED)
        .filter(Q(parent__user=user) | (Q(post__user=user) & Q(parent__isnull=True)))
        .exclude(user=user)
    )
    if replies_feed_viewed_at:
        result = result.filter(created_at__gt=replies_feed_viewed_at)

    return result.distinct().count()
