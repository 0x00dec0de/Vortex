import uuid
from functools import cached_property

import reversion
from django.conf import settings
from django.db import models
from django.db.models import (
    Case,
    Count,
    Exists,
    F,
    OuterRef,
    Q,
    UniqueConstraint,
    Value,
    When,
)
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from mptt.querysets import TreeQuerySet

from comments.choices import CommentStatus, CommentVoteChoice
from common.models import Timestamped
from users.models import UserPublic


def with_ratings(instance: TreeQuerySet, viewer: UserPublic, with_voted=True):
    queryset = instance.annotate(
        _votes_up_count=Count("votes", Q(votes__value=CommentVoteChoice.UPVOTE)),
        _votes_down_count=Count("votes", Q(votes__value=CommentVoteChoice.DOWNVOTE)),
        _rating=(F("_votes_up_count") - F("_votes_down_count")),
    )
    if not all([with_voted, viewer, viewer.is_authenticated]):
        return queryset

    queryset = queryset.annotate(
        _voted=Case(
            When(
                Exists(
                    CommentVote.objects.filter(
                        comment=OuterRef("pk"),
                        user=viewer,
                        value=CommentVoteChoice.UPVOTE,
                    )
                ),
                then=Value(CommentVoteChoice.UPVOTE),
            ),
            When(
                Exists(
                    CommentVote.objects.filter(
                        comment=OuterRef("pk"),
                        user=viewer,
                        value=CommentVoteChoice.DOWNVOTE,
                    )
                ),
                then=Value(CommentVoteChoice.DOWNVOTE),
            ),
        )
    )
    return queryset


# 🚨uh-oh! monkey patching 🐒. Приходится извращаться, потому как mptt не позволяет
# оверрайдить TreeQuerySet
TreeQuerySet.with_ratings = with_ratings


@reversion.register(ignore_duplicates=True, exclude=("updated_at",))
class Comment(MPTTModel, Timestamped):
    """Model to store user comment on posts."""

    uuid = models.UUIDField(
        default=uuid.uuid4, unique=True, db_index=True, editable=False
    )
    parent = TreeForeignKey(
        "self", on_delete=models.CASCADE, null=True, blank=True, related_name="children"
    )
    user = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="comments"
    )
    post = models.ForeignKey(
        "posts.Post", on_delete=models.CASCADE, related_name="comments"
    )
    content = models.JSONField(default=dict)
    votes_up_count = models.IntegerField(default=0)
    votes_down_count = models.IntegerField(default=0)
    rating = models.IntegerField(default=0)
    status = models.CharField(
        max_length=9, choices=CommentStatus.choices, default=CommentStatus.PUBLISHED
    )

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"

    def get_absolute_url(self):
        return (
            f"{settings.FRONTEND_BASE_DOMAIN}/post/{self.post.slug}?comment={self.uuid}"
        )

    def __str__(self):
        return f"<{self.pk}: {self.user.username} -> {self.post.title}>"

    @cached_property
    def parent_uuid(self):
        try:
            return self.parent.uuid
        except AttributeError:
            return None


class CommentVote(Timestamped):
    """Model to store comment votes."""

    user = models.ForeignKey(
        "users.UserPublic", related_name="comment_votes", on_delete=models.CASCADE
    )
    comment = models.ForeignKey(
        "comments.Comment", related_name="votes", on_delete=models.CASCADE
    )
    value = models.SmallIntegerField(choices=CommentVoteChoice.choices)

    class Meta:
        constraints = [
            UniqueConstraint(fields=("user", "comment"), name="user_comment_vote"),
        ]
