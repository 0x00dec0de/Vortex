from django_filters import rest_framework as filters

from comments.choices import MyReplies
from comments.models import Comment
from comments.selectors import (
    get_replies_for_user_comments,
    get_replies_for_user_comments_and_posts,
    get_replies_for_user_posts,
    get_user_comments,
)
from posts.models import Post


class CommentFilter(filters.FilterSet):
    post = filters.CharFilter(method="filter_by_post_slug")
    parent = filters.UUIDFilter(method="filter_by_parent")
    replies = filters.ChoiceFilter(
        method="filter_replies_only", choices=MyReplies.choices
    )
    my = filters.BooleanFilter(method="filter_my_comments_only")

    def __init__(self, data=None, *args, **kwargs):
        method = getattr(kwargs.get("request"), "method", None)
        if method in ("PATCH", "DELETE", "POST"):
            super().__init__(data, *args, **kwargs)
            return
        if data is not None:
            data = data.copy()
            if not data.get("post"):
                # Just dummy value in order to return guaranteed empty queryset in case
                # post GET parameter not set
                data["post"] = -1
        super().__init__(data, *args, **kwargs)

    def filter_by_post_slug(self, queryset, name, value):
        """Find post by provided uuid and then filter by post id."""
        try:
            post = Post.objects.get(slug=value)
        except Post.DoesNotExist:
            return queryset.none()
        return queryset.filter(post_id=post.pk)

    def filter_by_parent(self, queryset, name, value):
        try:
            parent = Comment.objects.get(uuid=value)
        except Comment.DoesNotExist:
            return queryset.none()

        return parent.get_children().with_ratings(self.request.user)

    def filter_replies_only(self, queryset, name, value):
        user = self.request.user
        selectors = {
            MyReplies.OLD: get_replies_for_user_comments_and_posts,
            MyReplies.ALL: get_replies_for_user_comments_and_posts,
            MyReplies.POST_REPLIES: get_replies_for_user_posts,
            MyReplies.COMMENT_REPLIES: get_replies_for_user_comments,
        }
        if value and user.is_authenticated:
            selector = selectors[value]
            return selector(user)
        return queryset.none()

    def filter_my_comments_only(self, queryset, name, value):
        user = self.request.user
        if value and user.is_authenticated:
            return get_user_comments(user)
        return queryset.none()


class CommentFilterBackend(filters.DjangoFilterBackend):
    def get_ordering(self, request, queryset, view):
        if view.is_replies():
            return "-created_at"
        ordering_mapping = {
            "rating": "_rating",
            "-rating": "-_rating",
            "created_at": "created_at",
            "-created_at": "-created_at",
        }
        return ordering_mapping.get(request.query_params.get("ordering")) or "-created_at"
