import logging

from django.db import IntegrityError, transaction
from django.utils import timezone

from comments.choices import CommentVoteChoice
from comments.exceptions import CommentVoteException
from comments.models import Comment, CommentVote
from posts.models import Post
from users.models import UserContext, UserPublic
from users.services import update_user_context_field_with_data

logger = logging.getLogger(__name__)


@transaction.atomic
def update_post_comments_count(post_id: int):
    """Update comments count fot the post comment posted on."""
    post = Post.objects.select_for_update().get(pk=post_id)
    post.comments_count = post.comments.count()
    post.save(update_fields=["comments_count"])


@transaction.atomic
def update_author_comments_count(author_user_id: int):
    author = UserPublic.objects.select_for_update().get(pk=author_user_id)
    author.comments_count = author.comments.count()
    author.save(update_fields=["comments_count"])


@transaction.atomic
def record_vote_for_comment(comment: Comment, actor: UserPublic, vote: CommentVoteChoice):
    """Record vote for a comment and trigger updates of comment and comment's author."""

    if comment.user_id == actor.id:
        raise CommentVoteException("Голосовать за свой комментарий невозможно.")

    comment_vote = CommentVote.objects.filter(comment=comment, user=actor).first()
    if comment_vote:
        # Здесь если при наличии голоса, пользователь проголосовал еще раз,
        # отменяем рейтинг и удаляем CommentVote
        comment_vote.delete()
        return

    try:
        CommentVote.objects.create(comment=comment, user=actor, value=vote)
    except IntegrityError:
        # Если не дает создать такой голос, значит он уже в базе и будет посчитан,
        # скорее всего это rage-clicks и можно проигнорировать
        logger.info(
            "User %s trying to vote with same value %s for comment %s",
            actor,
            vote,
            comment,
        )


def update_notify_feed_viewed_at(user: UserPublic):
    """Обновляем время, когда пользователь просмотрел ленту уведомлений."""
    update_user_context_field_with_data(
        user, UserContext.notify_viewed_at, timezone.now()
    )


def update_replies_feed_viewed_at(user: UserPublic):
    """Обновляем время, когда пользователь просмотрел ленту ответов."""
    update_user_context_field_with_data(
        user, UserContext.replies_feed_viewed_at, timezone.now()
    )


def update_notifications_viewed_at(user: UserPublic):
    """Обновляем время, когда пользователь просмотрел ленту ответов."""
    update_user_context_field_with_data(
        user, UserContext.notify_viewed_at, timezone.now()
    )


def update_subscriptions_feed_viewed_at(user: UserPublic):
    """Обновляем время, когда пользователь просмотрел ленту подписок."""
    update_user_context_field_with_data(
        user, UserContext.subscriptions_feed_viewed_at, timezone.now()
    )
