from datetime import timedelta

import pytest
from django.utils import timezone
from freezegun import freeze_time
from funcy import first, lpluck, pluck, pluck_attr, where
from rest_framework import status

from comments.choices import MyReplies
from comments.tests.factories import CommentFactory
from comments.tests.mixins import CommentApiActionsMixin
from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestRepliesFeed(CommentApiActionsMixin):
    def setup_method(self):
        self.me = UserPublicFactory()
        self.my_post = PostFactory(user=self.me, status=PostStatus.PUBLISHED)

    def test_can_get_replies_marked_as_new(self, authed_api_client):
        viewed_comments = CommentFactory.create_batch(size=2, post=self.my_post)
        self._get_comments(authed_api_client(self.me), replies=MyReplies.ALL)
        with freeze_time(timezone.now() + timedelta(days=1)):
            new_comments = CommentFactory.create_batch(size=2, post=self.my_post)
            result = self._get_comments(authed_api_client(self.me), replies=MyReplies.ALL)

        assert result.status_code == status.HTTP_200_OK, result.content.decode()

        # Комментарии должны быть отсортированы от новых к старым
        all_comments_sorted = sorted(
            viewed_comments + new_comments, key=lambda c: c.created_at, reverse=True
        )
        new_comments_sorted = sorted(
            new_comments, key=lambda c: c.created_at, reverse=True
        )
        viewed_comments_sorted = sorted(
            viewed_comments, key=lambda c: c.created_at, reverse=True
        )
        response = result.json()
        comments = response["results"]

        assert lpluck("uuid", comments) == list(
            map(str, pluck_attr("uuid", all_comments_sorted))
        )
        assert lpluck("uuid", where(comments, new=True)) == list(
            map(str, pluck_attr("uuid", new_comments_sorted))
        )
        assert lpluck("uuid", where(comments, new=False)) == list(
            map(str, pluck_attr("uuid", viewed_comments_sorted))
        )

    @pytest.mark.parametrize(
        "replies_type",
        (
            MyReplies.ALL,
            MyReplies.POST_REPLIES,
            MyReplies.COMMENT_REPLIES,
        ),
    )
    def test_can_get_replies_to_my_comments_and_posts(
        self, replies_type, authed_api_client, anon_api_client
    ):
        post_1, post_2 = PostFactory.create_batch(size=2, status=PostStatus.PUBLISHED)
        my_comment_1 = CommentFactory(post=post_1, user=self.me)
        my_comment_2 = CommentFactory(post=post_2, user=self.me)

        # Ответ юзера на свой пост, не надо выводить
        CommentFactory(post=self.my_post, user=self.me)
        # Чей-то ответ на пост юзера, нужно вывести
        others_comment_to_my_post = CommentFactory(post=self.my_post)
        # Чей-то ответ на чей-то коммент к посту юзера, не выводим
        CommentFactory(post=self.my_post, parent=others_comment_to_my_post)
        # Ответ юзера на свой же коммент, не надо выводить
        my_reply_to_my_comment = CommentFactory(
            parent=my_comment_1, post=post_1, user=self.me
        )
        # Чьи-то ответы на коммент юзера, должны вывести
        reply_to_my_reply_to_my_comment = CommentFactory(
            parent=my_reply_to_my_comment, post=post_1
        )
        someones_replies_to_my_comment = CommentFactory.create_batch(
            size=2, parent=my_comment_1, post=post_1
        )
        someones_reply_to_my_comment_2 = CommentFactory(parent=my_comment_2, post=post_2)

        # Чей-то ответ на чей-то коммент, не должно быть в ответе
        CommentFactory(parent=first(someones_replies_to_my_comment), post=post_1)

        replies_mapping = {
            MyReplies.ALL: {
                "comments": [
                    *someones_replies_to_my_comment,
                    reply_to_my_reply_to_my_comment,
                    someones_reply_to_my_comment_2,
                    others_comment_to_my_post,
                ],
                "post_slugs": {post_1.slug, post_2.slug, self.my_post.slug},
            },
            MyReplies.POST_REPLIES: {
                "comments": [others_comment_to_my_post],
                "post_slugs": {self.my_post.slug},
            },
            MyReplies.COMMENT_REPLIES: {
                "comments": [
                    *someones_replies_to_my_comment,
                    reply_to_my_reply_to_my_comment,
                    someones_reply_to_my_comment_2,
                ],
                "post_slugs": {post_1.slug, post_2.slug},
            },
        }
        expected_comments = replies_mapping[replies_type]["comments"]
        expected_post_slugs = replies_mapping[replies_type]["post_slugs"]

        now = timezone.now()
        with freeze_time(now):
            result = self._get_comments(authed_api_client(self.me), replies=replies_type)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        response = result.json()
        comments = response["results"]
        assert set(pluck("uuid", comments)) == set(
            map(str, pluck_attr("uuid", expected_comments))
        )
        assert set(pluck("slug", pluck("post", comments))) == expected_post_slugs
        self.me.refresh_from_db()
        assert self.me.context.replies_feed_viewed_at == now

        result = self._get_comments(anon_api_client(), replies=replies_type)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        response = result.json()
        comments = response["results"]
        assert len(comments) == 0, "Ответ должен быть пустой для анонима"
