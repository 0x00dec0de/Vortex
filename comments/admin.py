import reversion
from django.contrib import admin
from django.db.models import JSONField
from django.urls import reverse
from django.utils.html import format_html
from jsoneditor.forms import JSONEditor
from mptt.admin import MPTTModelAdmin
from pydantic import TypeAdapter
from reversion.admin import VersionAdmin

from comments.choices import CommentStatus
from comments.models import Comment
from common.content.blocks.common import CommentContentBlocks


@admin.register(Comment)
class CommentAdmin(MPTTModelAdmin, VersionAdmin):
    formfield_overrides = {
        JSONField: {
            "widget": JSONEditor(
                jsonschema=TypeAdapter(CommentContentBlocks).json_schema(),
                attrs={"style": "min-height: 600px"},
            )
        },
    }
    mptt_indent_field = "short_content"
    list_display_links = ("short_content",)
    list_display = (
        "short_content",
        "username",
        "post_title",
        "status",
        "created_at",
    )
    list_filter = ("user",)
    fields = (
        "post",
        "parent",
        "user",
        "content",
        "votes_up_count",
        "votes_down_count",
        "rating",
        "created_at",
    )
    raw_id_fields = (
        "post",
        "user",
        "parent",
    )
    readonly_fields = (
        "votes_up_count",
        "votes_down_count",
        "rating",
        "created_at",
    )
    list_select_related = (
        "user",
        "post",
        "parent",
    )
    search_fields = (
        "user__username",
        "user__email",
        "post__title",
        "post__slug",
        "post__tags__name",
        "content",
    )
    ordering = ("-created_at",)

    def short_content(self, obj: Comment) -> str:
        """Get short version of published content."""
        try:
            return obj.content["blocks"][0]["data"]["text"][:100]
        except (KeyError, IndexError):
            return "n/a"

    def username(self, obj: Comment) -> str:
        """Get author's username."""
        return obj.user.username

    def post_title(self, obj: Comment) -> str:
        """Get title of the post."""
        return format_html(
            "<a href='{}'>{}</a>",
            reverse("admin:posts_post_change", args=(obj.post.pk,)),
            obj.post.title,
        )

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions.pop("delete_selected", None)
        return actions

    def has_add_permission(self, request):
        return False

    def delete_model(self, request, obj: Comment):
        with reversion.create_revision():
            obj.status = CommentStatus.DELETED
            obj.save()
            reversion.set_user(request.user)
            reversion.set_comment("Комментарий был удален из админки")

    def delete_queryset(self, request, queryset):
        return None
