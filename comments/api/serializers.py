import uuid

import pydantic_core
from django.conf import settings
from drf_spectacular.utils import OpenApiExample, extend_schema_serializer
from rest_framework import serializers

from comments.choices import CommentVoteChoice
from comments.models import Comment, CommentVote
from comments.selectors import can_edit_comment
from common.content.blocks.common import CommentContentBlocks
from common.errors import convert_errors
from posts.api.serializers import PostTitleSerializer
from posts.models import Post
from users.api.serializers import UserPublicMinimalSerializer


class CommentSerializer(serializers.ModelSerializer):
    """Serializer to represent comment in flat list."""

    author = UserPublicMinimalSerializer(source="user")
    parent_uuid = serializers.UUIDField(source="parent.uuid", allow_null=True)
    can_edit = serializers.SerializerMethodField()
    votes_up_count = serializers.IntegerField(source="_votes_up_count")
    votes_down_count = serializers.IntegerField(source="_votes_down_count")
    rating = serializers.FloatField(source="_rating")
    voted = serializers.ChoiceField(
        choices=CommentVoteChoice.choices, allow_null=True, source="_voted"
    )

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "parent_uuid",
            "level",
            "is_leaf_node",
            "can_edit",
            "author",
            "content",
            "rating",
            "votes_up_count",
            "votes_down_count",
            "voted",
            "created_at",
        )
        read_only_fields = fields

    def get_can_edit(self, obj: Comment):
        request = self.context.get("request")
        user = request and request.user
        return can_edit_comment(user, obj)


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            name="Постинг ответа на коммент",
            value={
                "post": "post-slug",
                "parent_uuid": uuid.uuid4(),
                "content": {
                    "blocks": [
                        {"type": "paragraph", "data": {"text": "Комментарий"}},
                        {
                            "type": "image",
                            "data": {
                                "file": {"url": "https://path.to/image.jpg"},
                                "caption": "Подпись к фото",
                            },
                        },
                    ],
                },
            },
            request_only=True,
        ),
        OpenApiExample(
            name="Постинг коммента к посту",
            value={
                "post": "post-slug",
                "content": {
                    "blocks": [
                        {"type": "paragraph", "data": {"text": "Комментарий"}},
                        {
                            "type": "image",
                            "data": {
                                "file": {"url": "https://path.to/image.jpg"},
                                "caption": "Подпись к фото",
                            },
                        },
                    ],
                },
            },
            request_only=True,
        ),
    ]
)
class CommentCreateSerializer(serializers.ModelSerializer):
    """Serializer to create comment."""

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    author = UserPublicMinimalSerializer(source="user", read_only=True)
    parent = serializers.SlugRelatedField(
        slug_field="uuid",
        queryset=Comment.objects.only("pk"),
        required=False,
        write_only=True,
    )
    parent_uuid = serializers.UUIDField(required=False)
    post = serializers.SlugRelatedField(
        slug_field="slug", queryset=Post.objects.only("pk")
    )
    can_edit = serializers.BooleanField(read_only=True, default=True)
    votes_up_count = serializers.IntegerField(read_only=True, default=0)
    votes_down_count = serializers.IntegerField(read_only=True, default=0)
    rating = serializers.FloatField(read_only=True, default=0)
    voted = serializers.ChoiceField(
        choices=CommentVoteChoice.choices, allow_null=True, read_only=True, default=None
    )

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "user",
            "author",
            "post",
            "parent",
            "parent_uuid",
            "content",
            "can_edit",
            "level",
            "is_leaf_node",
            "votes_up_count",
            "votes_down_count",
            "voted",
            "created_at",
            "rating",
        )

        read_only_fields = (
            "uuid",
            "author",
            "can_edit",
            "level",
            "is_leaf_node",
            "votes_up_count",
            "votes_down_count",
            "voted",
            "created_at",
            "rating",
        )

    def validate(self, attrs):
        """Validate comment data, make sure parent comment from the same post."""
        if parent := attrs.get("parent_uuid"):
            post = attrs["post"]
            if parent.post_id != post.pk:
                raise serializers.ValidationError(
                    "Нельзя отвечать на комментарий из другого поста!"
                )
        return super().validate(attrs)

    def validate_content(self, value):
        if not isinstance(value, dict):
            raise serializers.ValidationError("Неверный формат данных")
        try:
            value = CommentContentBlocks(**value).model_dump(
                by_alias=True, exclude_none=True
            )
        except pydantic_core.ValidationError as e:
            errors = convert_errors(e, include_input=False)
            raise serializers.ValidationError(errors[0]["msg"])
        return value

    def validate_parent_uuid(self, value):
        try:
            return Comment.objects.get(uuid=value)
        except Comment.DoesNotExist:
            return None

    def create(self, validated_data):
        data = validated_data.copy()
        if "parent_uuid" in data:
            data["parent"] = data.pop("parent_uuid", None)
        return super().create(data)


class CommentUpdateSerializer(serializers.ModelSerializer):
    author = UserPublicMinimalSerializer(source="user", read_only=True)

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "author",
            "content",
        )

        read_only_fields = (
            "uuid",
            "author",
        )


class CommentVoteCreateSerializer(serializers.ModelSerializer):
    """Serializer validating data submitted to cast a vote on a comment."""

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = CommentVote
        fields = (
            "user",
            "value",
        )


class CommentRatingOnlySerializer(serializers.ModelSerializer):
    """Serializer to return only rating related data."""

    votes_up_count = serializers.IntegerField(source="_votes_up_count")
    votes_down_count = serializers.IntegerField(source="_votes_down_count")
    rating = serializers.FloatField(source="_rating")
    voted = serializers.ChoiceField(
        choices=CommentVoteChoice.choices, allow_null=True, source="_voted"
    )

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "votes_up_count",
            "votes_down_count",
            "rating",
            "voted",
        )


class CommentRepliesSerializer(CommentSerializer):
    post = PostTitleSerializer()
    parent_uuid = serializers.UUIDField(source="parent.uuid", allow_null=True)
    author = UserPublicMinimalSerializer(source="user")
    votes_up_count = serializers.IntegerField(source="_votes_up_count")
    votes_down_count = serializers.IntegerField(source="_votes_down_count")
    rating = serializers.FloatField(source="_rating")
    new = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "parent_uuid",
            "is_leaf_node",
            "post",
            "author",
            "content",
            "rating",
            "votes_up_count",
            "votes_down_count",
            "voted",
            "new",
            "created_at",
        )

    def get_new(self, obj: Comment):
        if replies_feed_viewed_at := self.context.get("replies_feed_viewed_at"):
            return obj.created_at > replies_feed_viewed_at
        return False


class CommentTop3Serializer(CommentRepliesSerializer):
    absolute_url = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "parent_uuid",
            "is_leaf_node",
            "post",
            "author",
            "content",
            "rating",
            "votes_up_count",
            "votes_down_count",
            "voted",
            "new",
            "created_at",
            "absolute_url",
        )

    def get_absolute_url(self, obj: Comment):
        return f"{settings.FRONTEND_BASE_DOMAIN}/post/{obj.post.slug}?comment={obj.uuid}"


class UserOwnCommentSerializer(CommentSerializer):
    post = PostTitleSerializer()
    parent_uuid = serializers.UUIDField(source="parent.uuid", allow_null=True)
    author = UserPublicMinimalSerializer(source="user")
    votes_up_count = serializers.IntegerField(source="_votes_up_count")
    votes_down_count = serializers.IntegerField(source="_votes_down_count")
    rating = serializers.FloatField(source="_rating")

    class Meta:
        model = Comment
        fields = (
            "uuid",
            "parent_uuid",
            "is_leaf_node",
            "post",
            "author",
            "content",
            "rating",
            "votes_up_count",
            "votes_down_count",
            "created_at",
        )
