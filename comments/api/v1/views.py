import contextlib
from datetime import timedelta

import reversion
from django.db.models import Count, F, Q
from django.http import QueryDict
from django.utils import timezone
from drf_spectacular.utils import extend_schema
from rest_access_policy import AccessViewSetMixin
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from comments.api.pagination import KapibaraCommentsCursorPagination
from comments.api.policies import CommentAccessPolicy
from comments.api.serializers import (
    CommentCreateSerializer,
    CommentRatingOnlySerializer,
    CommentRepliesSerializer,
    CommentSerializer,
    CommentTop3Serializer,
    CommentUpdateSerializer,
    CommentVoteCreateSerializer,
    UserOwnCommentSerializer,
)
from comments.api.throttling import CommentThrottle
from comments.choices import CommentVoteChoice
from comments.exceptions import CommentEditException
from comments.filters import CommentFilter, CommentFilterBackend
from comments.selectors import (
    get_all_comments,
    get_all_comments_with_ratings,
    get_extras_for_list_cache,
    get_samples_for_list_cache,
    get_user_comments,
)
from comments.services import (
    record_vote_for_comment,
    update_author_comments_count,
    update_post_comments_count,
    update_replies_feed_viewed_at,
)
from common.api.helpers import get_formatted_error_response
from common.api.mixins import CachedListViewMixin
from common.api.parameters import PARENT_COMMENT_UUID, POST_SLUG
from posts.models import Post
from posts.selectors import get_user_restricted_tag_names
from posts.services import parse_comment_content_make_mentions
from users.models import UserPublic
from users.selectors import can_add_comment, can_vote, is_user_autobanned


class CommentViewSet(
    CachedListViewMixin,
    AccessViewSetMixin,
    ModelViewSet,
):
    serializer_class = CommentSerializer
    access_policy = CommentAccessPolicy
    lookup_field = "uuid"
    http_method_names = ["post", "get", "patch", "delete"]
    filterset_class = CommentFilter
    throttle_scope = "comments"
    throttle_classes = [CommentThrottle]

    filter_backends = [CommentFilterBackend, OrderingFilter]

    @property
    def pagination_class(self):
        if self.is_replies() or self._is_my_comments():
            return KapibaraCommentsCursorPagination
        return None

    def is_replies(self):
        return self.request.query_params.get("replies") is not None

    def _is_my_comments(self):
        return self.request.query_params.get("my") is not None

    def get_queryset(self):  # noqa: CFQ004
        user = self.request.user
        if self.action in ("update", "partial_update", "destroy"):
            return get_all_comments()
        if self._is_my_comments() and user.is_authenticated:
            return get_user_comments(user)

        return get_all_comments_with_ratings(self.request.user)

    def get_object(self):
        if self.action == "retrieve":
            queryset = self.get_queryset()
        else:
            queryset = self.filter_queryset(self.get_queryset())

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def get_serializer_class(self):  # noqa: CFQ004
        if self.action in ("create",):
            return CommentCreateSerializer
        if self.action in ("get_top_3", "get_random_positive"):
            return CommentTop3Serializer
        if self.action in (
            "update",
            "partial_update",
        ):
            return CommentUpdateSerializer
        if self.is_replies():
            return CommentRepliesSerializer
        if self._is_my_comments():
            return UserOwnCommentSerializer

        return super().get_serializer_class()

    def get_serializer_context(self):
        user: UserPublic = self.request.user
        context = super().get_serializer_context()
        with contextlib.suppress(AttributeError):
            context["replies_feed_viewed_at"] = user.context.replies_feed_viewed_at
        return context

    @extend_schema(parameters=[POST_SLUG, PARENT_COMMENT_UUID])
    def list(self, request, *args, **kwargs):
        user = request.user
        post_slug = request.query_params.get("post")
        post = Post.objects.filter(slug=post_slug).first()
        if post:
            tags = list(post.tags.values_list("name", flat=True))
            is_restricted = [
                tag for tag in tags if tag in get_user_restricted_tag_names(user)
            ]
            if is_restricted:
                return Response(data=[])
        result = super().list(request, *args, **kwargs)
        if self.is_replies() and user.is_authenticated:
            update_replies_feed_viewed_at(user)
        return result

    @action(methods=["GET"], detail=False, url_path="top-3")
    def get_top_3(self, request, *args, **kwargs):
        """
        Retrieves the top 3 comments from the last 24 hours based on
        upvotes, net votes (upvotes minus downvotes), and creation date.
        """
        # Получаем текущее время и вычитаем сутки
        current_time = timezone.now()
        time_threshold = current_time - timedelta(days=1)

        # Получаем топ-3 комментариев с максимальным рейтингом
        # за последние сутки, исключая посты с запрещёнными тегами
        top_comments = (
            self.get_queryset()
            .filter(created_at__gte=time_threshold)
            .annotate(
                _votes_up_count=Count("votes", Q(votes__value=CommentVoteChoice.UPVOTE)),
                _votes_down_count=Count(
                    "votes", Q(votes__value=CommentVoteChoice.DOWNVOTE)
                ),
                _rating=F("_votes_up_count") - F("_votes_down_count"),
            )
            .exclude(post__tags__name__in=get_user_restricted_tag_names(request.user))
            .order_by("-_rating")[:3]
        )
        serializer = self.get_serializer(top_comments, many=True)
        return Response(serializer.data)

    @action(methods=["GET"], detail=False, url_path="random-positive")
    def get_random_positive(self, request, *args, **kwargs):
        """
        Retrieves random positive comments from the last 24 hours based on
        upvotes, net votes (upvotes minus downvotes), and creation date.
        """
        # Получаем текущее время и вычитаем сутки
        current_time = timezone.now()
        time_threshold = current_time - timedelta(days=1)

        # Получаем случайные положительные комментарии за последние сутки
        random_positive_comments = (
            self.get_queryset()
            .filter(created_at__gte=time_threshold)
            .annotate(
                _votes_up_count=Count("votes", Q(votes__value=CommentVoteChoice.UPVOTE)),
                _votes_down_count=Count(
                    "votes", Q(votes__value=CommentVoteChoice.DOWNVOTE)
                ),
                _rating=F("_votes_up_count") - F("_votes_down_count"),
            )
            .exclude(post__tags__name__in=get_user_restricted_tag_names(request.user))
            .filter(_rating__gte=0)  # Оставляем только положительные комментарии
            .order_by("?")[:3]  # Сортируем случайным образом
        )

        serializer = self.get_serializer(random_positive_comments, many=True)
        return Response(serializer.data)

    def samples_for_list_cache(self, query_params: QueryDict) -> list:
        return get_samples_for_list_cache(query_params)

    def extras_for_list_cache(
        self, user: UserPublic, query_params: QueryDict
    ) -> str | None:
        return get_extras_for_list_cache(
            user, query_params, filterset_class=self.filterset_class
        )

    def create(self, request, *args, **kwargs):
        user = request.user
        if is_user_autobanned(user):
            code = "rating_too_low"
            message = "Вы не можете публиковать комментарии из-за низкого рейтинга"
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )
        # if user.is_banned:
        #     code = "rating_too_low"
        #     message = "Вы не можете публиковать комментарии потому что вы заблокированы"
        #     return Response(
        #         get_formatted_error_response(
        #             code=code,
        #             message=message,
        #         ),
        #         status=status.HTTP_429_TOO_MANY_REQUESTS,
        #     )
        if not can_add_comment(user):
            if user.is_verified:
                code = "limit_reached"
                message = "Вы достигли лимита комментариев в сутки"
            else:
                code = "limit_reached_confirm_telegram"
                message = (
                    "Вы достигли лимита комментариев в сутки для неверифицированного "
                    "пользователя. Подтвердите свой Телеграм для снятия всех лимитов."
                )
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )
        response = super().create(request, *args, **kwargs)
        parse_comment_content_make_mentions(
            response.data["uuid"], response.data["content"]
        )
        return response

    def perform_create(self, serializer):
        with reversion.create_revision():
            super().perform_create(serializer)
            reversion.set_user(self.request.user)
            reversion.set_comment("Комментарий добавлен")
        update_author_comments_count(serializer.instance.user_id)
        update_post_comments_count(serializer.instance.post_id)

    def perform_update(self, serializer):
        instance = serializer.instance
        if instance.votes.exists():
            raise CommentEditException(
                "Нельзя редактировать, т.к. за этот комментарий уже проголосовали"
            )
        with reversion.create_revision():
            serializer.save()
            reversion.set_user(self.request.user)
            reversion.set_comment("Комментарий отредактирован")

    @action(
        methods=["POST"],
        detail=True,
        serializer_class=CommentVoteCreateSerializer,
    )
    def vote(self, request, *args, **kwargs):
        """Record vote for the comment."""
        user = request.user
        if is_user_autobanned(user):
            code = "rating_too_low"
            message = "Вы не можете голосовать за комментарии из-за низкого рейтинга"
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )
        # if user.is_banned:
        #     code = "rating_too_low"
        #     message = (
        #         "Вы не можете голосовать за комментарии потому что вас заблокировали"
        #     )
        #     return Response(
        #         get_formatted_error_response(
        #             code=code,
        #             message=message,
        #         ),
        #         status=status.HTTP_429_TOO_MANY_REQUESTS,
        #     )
        if not can_vote(user):
            if user.is_verified:
                code = "limit_reached"
                message = "Вы достигли лимита голосов за комментарии в сутки"
            else:
                code = "limit_reached_confirm_telegram"
                message = (
                    "Вы достигли лимита голосов за комментарии в сутки для "
                    "неверифицированного пользователя. Подтвердите свой Телеграм для "
                    "снятия всех лимитов."
                )
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )
        comment = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        record_vote_for_comment(comment, request.user, serializer.data["value"])
        # Получаем из базы, используя заданный queryset для того, чтобы все аннотации
        # прикрепились к объекту. comment.refresh_from_db() не работает, т.к. нужные
        # аннотации не сохраняются
        comment = self.get_object()
        return Response(
            CommentRatingOnlySerializer(comment).data, status=status.HTTP_201_CREATED
        )
