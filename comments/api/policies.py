from rest_access_policy import AccessPolicy, Statement

from comments.selectors import can_edit_comment, can_hard_delete_comment
from users.selectors import can_moderate_comments


class CommentAccessPolicy(AccessPolicy):
    statements = [
        Statement(
            action=["list", "retrieve", "<safe_methods>"],
            principal="*",
            effect="allow",
        ),
        Statement(
            action=["create"],
            principal="authenticated",
            effect="allow",
        ),
        Statement(
            action=["update", "partial_update"],
            principal="authenticated",
            effect="allow",
            condition=[
                "user_must_be_author",
                "should_be_allowed_to_edit",
            ],
        ),
        Statement(
            action=["destroy"],
            principal="authenticated",
            effect="allow",
            condition=[
                "user_must_be_author",
                "should_be_allowed_to_edit",
                "should_be_allowed_to_delete",
            ],
        ),
        Statement(
            action=["vote"],
            principal="authenticated",
            effect="allow",
        ),
    ]

    def user_must_be_author(self, request, view, action):
        user = request.user
        comment = view.get_object()
        if can_moderate_comments(user):
            return True
        return comment.user == user

    def should_be_allowed_to_edit(self, request, view, action):
        return can_edit_comment(request.user, view.get_object())

    def should_be_allowed_to_delete(self, request, view, action):
        return can_hard_delete_comment(request.user, view.get_object())
