from drf_spectacular.utils import extend_schema
from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from common.api import parameters

# from common.api.helpers import get_formatted_error_response
from users.api.permissions import Authenticator, LoadtestWorker, OwnUser
from users.api.serializers import (
    OwnUserSerializer,
    UserContextSerializer,
    UserPublicCreateSerializer,
    UserPublicFullSerializer,
    UserPublicMinimalSerializer,
)
from users.api.v1.filters import UserSearchFilter
from users.models import UserPublic
from users.selectors import fetch_users  # , is_user_autobanned
from users.services import mark_user_account_unverified, mark_user_account_verified


class UserViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    GenericViewSet,
):
    """API to manage user profile."""

    queryset = fetch_users()
    serializer_class = UserPublicFullSerializer
    lookup_field = "username__iexact"
    lookup_url_kwarg = "username"
    lookup_value_regex = r"[\w.\-]+"
    http_method_names = ["post", "get", "patch"]
    permission_classes = [Authenticator | LoadtestWorker]

    def get_permissions(self):
        """Return proper permissions based on action user performing."""
        if self.action in ("create", "verify", "unverify", "destroy", "delete"):
            permission = Authenticator | LoadtestWorker
            return [permission()]
        elif self.action in ("update", "partial_update"):
            permission = Authenticator | LoadtestWorker | OwnUser
            return [permission()]
        return [AllowAny()]

    def get_serializer_class(self):
        """Return serializer class based on action."""
        if self.action == "create":
            return UserPublicCreateSerializer
        if self.action in ("update", "partial_update") or (
            self.action == "retrieve" and self.request.user == self.get_object()
        ):
            return OwnUserSerializer
        return self.serializer_class

    @extend_schema(parameters=[parameters.INTERNAL_TOKEN])
    def create(self, request, *args, **kwargs):
        """Создает профиль юзера, принимает только запросы из других сервисов."""
        return super().create(request, *args, **kwargs)

    @extend_schema(parameters=[parameters.INTERNAL_TOKEN])
    @action(methods=["POST"], detail=True, serializer_class=UserPublicCreateSerializer)
    def verify(self, request, *args, **kwargs):
        """Отмечает пользователя как is_verified, только запросы из других сервисов."""
        user = self.get_object()
        user = mark_user_account_verified(user)
        return Response(self.get_serializer(user).data, status=status.HTTP_200_OK)

    @extend_schema(parameters=[parameters.INTERNAL_TOKEN])
    @action(methods=["POST"], detail=True, serializer_class=UserPublicCreateSerializer)
    def unverify(self, request, *args, **kwargs):
        """Отменяет верификацию пользователя, только запросы из других сервисов."""
        user = self.get_object()
        user = mark_user_account_unverified(user)
        return Response(self.get_serializer(user).data, status=status.HTTP_200_OK)

    # def partial_update(self, request, *args, **kwargs):
    #     """Обновление профиля пользователя."""
    #     user = self.get_object()
    #
    #     # Добавляем проверку на блокировку
    #     if user.is_banned or is_user_autobanned(user):
    #         code = "rating_too_low"
    #         message = "Вы не можете редактировать профиль потому что вы заблокированы."
    #         return Response(
    #             get_formatted_error_response(
    #                 code=code,
    #                 message=message,
    #             ),
    #             status=status.HTTP_429_TOO_MANY_REQUESTS,
    #         )
    #     # Если пользователь не заблокирован, выполняем стандартное обновление
    #     return super().partial_update(request, *args, **kwargs)


class UserContextView(APIView):
    permission_classes = [IsAuthenticated]

    @extend_schema(responses={status.HTTP_200_OK: UserContextSerializer})
    def get(self, request, *args, **kwargs):
        serializer = UserContextSerializer(request.user)
        return Response(serializer.data)


class UserSearchViewSet(GenericViewSet, mixins.ListModelMixin):
    """API для поиска пользователей для призыва в комментах/постах."""

    permission_classes = [IsAuthenticated]
    serializer_class = UserPublicMinimalSerializer
    filterset_class = UserSearchFilter
    search_fields = ("username",)
    pagination_class = None

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = UserPublic.objects.all()
        username = self.request.query_params.get("s")
        # What is the purpose of this QB query - docstring is not clear enough
        if username:
            queryset = queryset.filter(username__istartswith=username)
        return queryset
