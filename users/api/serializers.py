from typing import Optional

from django.conf import settings
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from comments.selectors import (
    get_comment_editable_window_minutes,
    get_count_of_new_items_in_replies_feed,
    get_user_default_comments_level,
)
from common.helpers import is_url_from_media_storage
from notification.selectors import get_count_of_new_notifications
from posts.selectors import (
    get_count_of_new_items_in_subscription_feed,
    get_post_editable_window_minutes,
)
from users.models import UserPublic
from users.selectors import (
    can_add_comment,
    can_publish_post,
    can_vote,
    get_comments_limit,
    get_draft_posts_limit,
    get_posts_limit,
    get_user_comments_count,
    get_user_posts_count,
    get_user_posts_trends_count,
    get_user_rating,
    get_user_subscribers_count,
    get_user_subscription_uuid,
    get_user_subscriptions_count,
    get_user_votes_down_count,
    get_user_votes_up_count,
    get_votes_limit,
    is_user_ignored_by_user,
    is_user_subscribed_to_user,
)


class UserPublicCreateSerializer(serializers.ModelSerializer):
    """Serializer to create user account via auth service."""

    username = serializers.CharField(
        validators=[
            UniqueValidator(queryset=UserPublic.objects.only("pk"), lookup="iexact")
        ]
    )
    email = serializers.EmailField(
        validators=[
            UniqueValidator(queryset=UserPublic.objects.only("pk"), lookup="iexact")
        ]
    )

    class Meta:
        model = UserPublic
        fields = (
            "external_user_uid",
            "username",
            "email",
            "is_verified",
        )


class UserPublicFullSerializer(serializers.ModelSerializer):
    """Serializer to represent whole user info."""

    rating = serializers.SerializerMethodField()
    votes_up_count = serializers.SerializerMethodField()
    votes_down_count = serializers.SerializerMethodField()
    comments_count = serializers.SerializerMethodField()
    posts_count = serializers.SerializerMethodField()
    posts_trends_count = serializers.SerializerMethodField()
    subscribers_count = serializers.SerializerMethodField()
    subscriptions_count = serializers.SerializerMethodField()
    is_ignored = serializers.SerializerMethodField()
    is_subscribed = serializers.SerializerMethodField()
    subscribe_id = serializers.SerializerMethodField()
    registered_at = serializers.DateTimeField(source="created_at", read_only=True)
    show_nsfw = serializers.BooleanField()
    show_hardcore = serializers.BooleanField()
    show_politics = serializers.BooleanField()
    show_viewed = serializers.BooleanField()
    vote_left_side = serializers.BooleanField()
    tags_up_side = serializers.BooleanField()
    expand_posts = serializers.BooleanField()
    is_official = serializers.BooleanField()
    is_from_pikabu = serializers.BooleanField()
    # deep_tree_comment = serializers.IntegerField()

    class Meta:
        model = UserPublic
        fields = (
            "external_user_uid",
            "username",
            "avatar",
            "bio",
            "gender",
            "rating",
            "posts_count",
            "posts_trends_count",
            "comments_count",
            "votes_up_count",
            "votes_down_count",
            "subscribers_count",
            "subscriptions_count",
            "is_ignored",
            "is_subscribed",
            "subscribe_id",
            "is_verified",
            "show_nsfw",
            "show_hardcore",
            "show_politics",
            "show_viewed",
            "vote_left_side",
            "tags_up_side",
            "expand_posts",
            # "deep_tree_comment",
            "registered_at",
            "is_official",
            "is_from_pikabu",
        )
        read_only_fields = (
            "external_user_uid",
            "username",
            "rating",
            "posts_count",
            "posts_trends_count",
            "comments_count",
            "votes_up_count",
            "votes_down_count",
            "is_verified",
            "created_at",
        )

    def validate_avatar(self, value):
        extensions = ["jpg", "png", "gif", "jpeg", "webp"]
        if value and not is_url_from_media_storage(value, extensions):
            raise serializers.ValidationError(
                f"Нельзя загружать такие файлы! "
                f"Допустимые типы файлов - {', '.join(extensions)}"
            )
        return value

    def get_rating(self, obj: UserPublic) -> float:
        return get_user_rating(obj)

    def get_votes_up_count(self, obj: UserPublic) -> int:
        return get_user_votes_up_count(obj)

    def get_votes_down_count(self, obj: UserPublic) -> int:
        return get_user_votes_down_count(obj)

    def get_comments_count(self, obj: UserPublic) -> int:
        return get_user_comments_count(obj)

    def get_posts_count(self, obj: UserPublic) -> int:
        return get_user_posts_count(obj)

    def get_posts_trends_count(self, obj: UserPublic) -> int:
        return get_user_posts_trends_count(obj)

    def get_subscribers_count(self, obj: UserPublic) -> int:
        return get_user_subscribers_count(obj)

    def get_subscriptions_count(self, obj: UserPublic) -> int:
        return get_user_subscriptions_count(obj)

    def get_is_ignored(self, obj: UserPublic) -> bool:
        return is_user_ignored_by_user(self.context["request"].user, obj)

    def get_is_subscribed(self, obj: UserPublic) -> bool:
        return is_user_subscribed_to_user(self.context["request"].user, obj)

    def get_subscribe_id(self, obj: UserPublic) -> Optional[str]:
        return get_user_subscription_uuid(self.context["request"].user, obj)


class OwnUserSerializer(UserPublicFullSerializer):
    class Meta(UserPublicFullSerializer.Meta):
        fields = UserPublicFullSerializer.Meta.fields + ("date_of_birth",)


class UserPublicMinimalSerializer(serializers.ModelSerializer):
    """Serializer to represent minimum info about user in posts."""

    subscribe_id = serializers.SerializerMethodField()

    def get_subscribe_id(self, obj: UserPublic) -> Optional[str]:
        return (
            get_user_subscription_uuid(self.context["request"].user, obj)
            if self.context
            else None
        )

    class Meta:
        model = UserPublic
        fields = ("username", "avatar", "is_verified", "subscribe_id")


class UserContextDetailsSerializer(serializers.Serializer):
    external_user_uid = serializers.CharField()
    username = serializers.CharField()
    email = serializers.EmailField()
    avatar = serializers.URLField()


class UserContextSerializer(serializers.Serializer):
    user = UserContextDetailsSerializer(source="*")
    # Count of unread replies to comments and posts
    unread_replies = serializers.SerializerMethodField()
    # Count of unread notify
    unread_notify = serializers.SerializerMethodField()
    # Count of unread posts in subscription feed
    unread_subscriptions = serializers.SerializerMethodField()
    min_tags_in_post = serializers.SerializerMethodField()
    max_tags_in_post = serializers.SerializerMethodField()

    # Edit restrictions
    comment_editable_window_minutes = serializers.SerializerMethodField()
    user_default_comments_level = serializers.SerializerMethodField()
    post_editable_window_minutes = serializers.SerializerMethodField()

    # General content limits
    content_html_block_max_length = serializers.SerializerMethodField()
    content_caption_max_length = serializers.SerializerMethodField()
    content_max_list_items = serializers.SerializerMethodField()
    content_list_item_max_length = serializers.SerializerMethodField()
    # Content limits for posts
    post_min_content_blocks = serializers.SerializerMethodField()
    post_max_total_blocks = serializers.SerializerMethodField()
    post_max_html_blocks = serializers.SerializerMethodField()
    post_max_img_blocks = serializers.SerializerMethodField()
    post_max_video_blocks = serializers.SerializerMethodField()

    # Content limits for comments
    comment_min_content_blocks = serializers.SerializerMethodField()
    comment_max_total_blocks = serializers.SerializerMethodField()
    comment_max_html_blocks = serializers.SerializerMethodField()
    comment_max_img_blocks = serializers.SerializerMethodField()
    comment_max_video_blocks = serializers.SerializerMethodField()

    # Dynamic user limits
    limits_timeframe_hours = serializers.SerializerMethodField()
    posts_published_limit = serializers.SerializerMethodField()
    posts_drafts_limit = serializers.SerializerMethodField()
    comments_limit = serializers.SerializerMethodField()
    votes_limit = serializers.SerializerMethodField()
    can_publish_post = serializers.SerializerMethodField()
    can_comment = serializers.SerializerMethodField()
    can_vote = serializers.SerializerMethodField()

    def get_unread_replies(self, obj: UserPublic) -> int:
        return get_count_of_new_items_in_replies_feed(obj)

    def get_unread_notify(self, obj: UserPublic) -> int:
        return get_count_of_new_notifications(obj)

    def get_unread_subscriptions(self, obj: UserPublic) -> int:
        return get_count_of_new_items_in_subscription_feed(obj)

    def get_comment_editable_window_minutes(self, obj: UserPublic) -> int:
        return get_comment_editable_window_minutes(obj)

    def get_user_default_comments_level(self, obj: UserPublic) -> int:
        return get_user_default_comments_level(obj)

    def get_post_editable_window_minutes(self, obj: UserPublic) -> int:
        return get_post_editable_window_minutes(obj)

    def get_posts_published_limit(self, obj: UserPublic) -> int:
        return get_posts_limit(obj)

    def get_posts_drafts_limit(self, obj: UserPublic) -> int:
        return get_draft_posts_limit(obj)

    def get_limits_timeframe_hours(self, obj: UserPublic) -> int:
        return settings.LIMIT_TIMEFRAME_HOURS

    def get_comments_limit(self, obj: UserPublic) -> int:
        return get_comments_limit(obj)

    def get_votes_limit(self, obj: UserPublic) -> int:
        return get_votes_limit(obj)

    def get_min_tags_in_post(self, obj: UserPublic) -> int:
        return settings.MIN_TAGS_IN_POST

    def get_max_tags_in_post(self, obj: UserPublic) -> int:
        return settings.MAX_TAGS_IN_POST

    def get_can_publish_post(self, obj: UserPublic) -> bool:
        return can_publish_post(obj)

    def get_can_comment(self, obj: UserPublic) -> bool:
        return can_add_comment(obj)

    def get_can_vote(self, obj: UserPublic) -> bool:
        return can_vote(obj)

    def get_content_html_block_max_length(self, obj: UserPublic) -> int:
        return settings.CONTENT_BLOCK_MAX_LENGTH_BYTES

    def get_content_caption_max_length(self, obj: UserPublic) -> int:
        return settings.CONTENT_CAPTION_MAX_LENGTH_BYTES

    def get_content_list_item_max_length(self, obj: UserPublic) -> int:
        return settings.CONTENT_LIST_ITEM_MAX_LENGTH_BYTES

    def get_content_max_list_items(self, obj: UserPublic) -> int:
        return settings.CONTENT_MAX_LIST_ITEMS

    def get_post_min_content_blocks(self, obj: UserPublic) -> int:
        return 1

    def get_post_max_total_blocks(self, obj: UserPublic) -> int:
        return settings.CONTENT_POST_BLOCK_MAX_TOTAL_ITEMS

    def get_post_max_html_blocks(self, obj: UserPublic) -> int:
        return settings.CONTENT_POST_BLOCK_MAX_HTML_ITEMS

    def get_post_max_img_blocks(self, obj: UserPublic) -> int:
        return settings.CONTENT_POST_BLOCK_IMG_MAX_ITEMS

    def get_post_max_video_blocks(self, obj: UserPublic) -> int:
        return settings.CONTENT_POST_BLOCK_VIDEO_MAX_ITEMS

    def get_comment_min_content_blocks(self, obj: UserPublic) -> int:
        return 1

    def get_comment_max_total_blocks(self, obj: UserPublic) -> int:
        return settings.CONTENT_COMMENT_BLOCK_MAX_TOTAL_ITEMS

    def get_comment_max_html_blocks(self, obj: UserPublic) -> int:
        return settings.CONTENT_COMMENT_BLOCK_MAX_HTML_ITEMS

    def get_comment_max_img_blocks(self, obj: UserPublic) -> int:
        return settings.CONTENT_COMMENT_BLOCK_IMG_MAX_ITEMS

    def get_comment_max_video_blocks(self, obj: UserPublic) -> int:
        return settings.CONTENT_COMMENT_BLOCK_VIDEO_MAX_ITEMS
