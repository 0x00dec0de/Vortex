import pytest
from django.utils import timezone
from rest_framework import status

from comments.tests.factories import CommentFactory
from lists.tests.factories import TagSubscriptionFactory, UserSubscriptionFactory
from posts.choices import PostFeedType, PostStatus
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserContextFactory, UserPublicFactory
from users.tests.mixins import UserApiActionsMixin


@pytest.mark.django_db
class TestUserContextView(UserApiActionsMixin, PostsApiActionsMixin):
    def setup_method(self):
        self.context = UserContextFactory()
        self.me = UserPublicFactory(context=self.context)

    @pytest.mark.parametrize(
        "post_status,new_comments_count,expected_comments_count",
        (
            (PostStatus.DRAFT, 1, 0),
            (PostStatus.DELETED, 1, 0),
            (PostStatus.PUBLISHED, 2, 2),
        ),
    )
    def test_unread_replies_count(
        self, post_status, new_comments_count, expected_comments_count, authed_api_client
    ):
        result = self._get_user_context(authed_api_client(self.me))
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.json()
        assert data["unread_replies"] == 0, "initial new comments count is wrong"

        post = PostFactory(status=post_status)
        my_comment = CommentFactory(user=self.me, post=post)
        CommentFactory.create_batch(size=new_comments_count, post=post, parent=my_comment)

        result = self._get_user_context(authed_api_client(self.me))
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.json()
        assert (
            data["unread_replies"] == expected_comments_count
        ), "new comments count is wrong"

        self.context.replies_feed_viewed_at = timezone.now()
        self.context.save()

        result = self._get_user_context(authed_api_client(self.me))
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.json()
        assert data["unread_replies"] == 0, "once read, new comments count is wrong"

    @pytest.mark.parametrize(
        "post_status,new_user_post_count,new_tag_post_count,expected_unread_subs_count",
        (
            (PostStatus.DRAFT, 1, 1, 0),
            (PostStatus.DELETED, 1, 1, 0),
            (PostStatus.PUBLISHED, 1, 1, 2),
        ),
    )
    def test_unread_subscriptions_count(
        self,
        post_status,
        new_user_post_count,
        new_tag_post_count,
        expected_unread_subs_count,
        authed_api_client,
    ):
        # Создаем подписку на юзера
        user_subscription = UserSubscriptionFactory(subscriber=self.me)
        tag_subscription = TagSubscriptionFactory(subscriber=self.me)

        # Проверяем, что счетчик непрочитанных постов - 0
        unread_subs = self._get_unread_subscriptions_count(authed_api_client(self.me))
        assert unread_subs == 0, "initial new subs count is wrong"

        # Создаем пост от подписанного юзера в различных статусах, и проверяем счетчик
        PostFactory(
            user=user_subscription.user, status=post_status, tags=[tag_subscription.tag]
        )
        PostFactory(status=post_status, tags=[tag_subscription.tag])

        unread_subs = self._get_unread_subscriptions_count(authed_api_client(self.me))
        assert unread_subs == expected_unread_subs_count, "new subs count is wrong"

        # Открываем ленту подписок, смотрим, что счетчик обнулился
        self._fetch_posts_feed(
            authed_api_client(self.me), {"feed_type": PostFeedType.SUBSCRIPTIONS}
        )
        unread_subs = self._get_unread_subscriptions_count(authed_api_client(self.me))
        assert unread_subs == 0, "once read new subs count is wrong"

    def test_user_context_require_auth(self, anon_api_client):
        result = self._get_user_context(anon_api_client())
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def _get_unread_subscriptions_count(self, client):
        result = self._get_user_context(client)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.json()
        return data["unread_subscriptions"]
