from enum import Enum

import pytest
from rest_framework import status

from users.tests.factories import UserPublicFactory
from users.tests.mixins import UserApiActionsMixin


class TestUsernames(Enum):
    REQUESTER = "backbrief"
    USER_1 = "SuperVestal"
    USER_2 = "19reCAPfoNd1"
    USER_3 = "1987654321"


@pytest.mark.django_db
class TestUserSearchViewSet(UserApiActionsMixin):
    def setup_method(self):
        self.users = {
            TestUsernames.REQUESTER: UserPublicFactory(
                username=TestUsernames.REQUESTER.value
            ),
            TestUsernames.USER_1: UserPublicFactory(username=TestUsernames.USER_1.value),
            TestUsernames.USER_2: UserPublicFactory(username=TestUsernames.USER_2.value),
            TestUsernames.USER_3: UserPublicFactory(username=TestUsernames.USER_3.value),
        }

    def test_search__without_param_s(self, authed_api_client, django_assert_num_queries):
        requester = self.users[TestUsernames.REQUESTER]
        # 1 for all users and 1 for specific by uid
        with django_assert_num_queries(6) as _:
            result = self._search_for_user(authed_api_client(requester))

        response_data = result.json()
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert len(response_data) == 4
        assert response_data[0]["username"] == requester.username
        assert response_data[0]["avatar"] == requester.avatar
        assert response_data[0]["is_verified"] is requester.is_verified

    # TODO: fix it
    # @pytest.mark.parametrize(
    #     "search_value,expected_username",
    #     (
    #         (TestUsernames.REQUESTER.value, TestUsernames.REQUESTER.value),
    #         (TestUsernames.USER_1.value.lower(), TestUsernames.USER_1.value),
    #         (TestUsernames.USER_2.value.upper(), TestUsernames.USER_2.value),
    #     ),
    #     ids=("exact case", "lowercase", "uppercase"),
    # )
    # def test_search__exact(
    #     self,
    #     search_value,
    #     expected_username,
    #     authed_api_client,
    #     django_assert_num_queries,
    # ):
    #     requester = self.users[TestUsernames.REQUESTER]
    #     # ? Should user see itself in results
    #     # 1 for all users and 1 for specific by uid and 1 for search in filters.py
    #     with django_assert_num_queries(4) as _:
    #         result = self._search_for_user(
    #             authed_api_client(requester), username=search_value
    #         )
    #
    #     assert result.status_code == status.HTTP_200_OK, result.content.decode()
    #     response_data = result.json()
    #     assert len(response_data) == 1
    #     assert response_data[0]["username"] == expected_username

    @pytest.mark.parametrize(
        "search_value,expected_results_count",
        (
            (TestUsernames.USER_1.value[:2], 1),
            ("abc", 0),
            (TestUsernames.USER_2.value[:2], 2),
            (TestUsernames.USER_2.value.upper()[:8], 1),
        ),
        ids=("first 2 letters", "no results", "digits, 2 results", "uppercase"),
    )
    def test_search__startswith(
        self, search_value, expected_results_count, authed_api_client
    ):
        requester = self.users[TestUsernames.REQUESTER]

        result = self._search_for_user(
            authed_api_client(requester), username=search_value
        )

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert len(result.json()) == expected_results_count

    @pytest.mark.parametrize(
        "search_value,expected_results_count",
        (
            (TestUsernames.REQUESTER.value[3:6], 1),
            ("qwerty", 0),
            ("1", 2),
            (TestUsernames.USER_1.value.lower()[5:], 1),
        ),
        ids=(
            "middle of username",
            "no results",
            "end of username, 2 results",
            "end of username, lowercase",
        ),
    )
    def test_search__contains(
        self, search_value, expected_results_count, authed_api_client
    ):
        requester = self.users[TestUsernames.REQUESTER]

        result = self._search_for_user(
            authed_api_client(requester), username=search_value
        )

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert len(result.json()) == expected_results_count
