import datetime
import uuid

import faker
import pytest
from django.conf import settings
from django.db.models import Sum
from rest_framework import status

from comments.choices import CommentVoteChoice
from comments.models import Comment
from comments.tests.factories import CommentFactory, CommentVoteFactory
from lists.tests.factories import UserIgnoredFactory, UserSubscriptionFactory
from posts.choices import PostStatus, PostVoteChoice
from posts.models import Post
from posts.tests.factories import PostFactory, PostVoteFactory
from users.choices import UserGender
from users.models import UserPublic
from users.tests.factories import UserPublicFactory
from users.tests.mixins import UserApiActionsMixin


@pytest.mark.django_db
class TestUsersViewSet(UserApiActionsMixin):
    def setup_method(self):
        self.internal_token_header = "X-Test-Header"
        self.internal_token = "test-token"
        self.username = "any-username"
        self.email = "test@kapi.bar"
        self.external_user_uid = str(uuid.uuid4())
        self.data = {
            "external_user_uid": self.external_user_uid,
            "username": self.username,
            "email": self.email,
        }
        settings.INTERNAL_TOKEN_HEADER = self.internal_token_header
        settings.INTERNAL_TOKENS = [self.internal_token]

    def test_can_create_non_verified_user_using_service(self, anon_api_client):
        result = self._create_user(
            anon_api_client(),
            self.data,
            headers={self.internal_token_header: self.internal_token},
        )
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        data = result.data
        assert data["external_user_uid"] == self.external_user_uid
        assert data["username"] == self.username
        assert data["email"] == self.email
        assert data["is_verified"] is False
        user = UserPublic.objects.get(external_user_uid=self.external_user_uid)
        assert user.is_active is True
        assert user.is_verified is False

    @pytest.mark.parametrize(
        "existing_username,new_username",
        (("zhopa", "zhopa"), ("Zho.Pa", "zho.pa"), ("zho.pa", "ZHO.PA")),
    )
    def test_cant_create_same_username(
        self, existing_username, new_username, anon_api_client
    ):
        UserPublicFactory(username=existing_username)
        result = self._create_user(
            anon_api_client(),
            {**self.data, "username": new_username},
            headers={self.internal_token_header: self.internal_token},
        )
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        data = result.json()
        assert data["username"][0] == "Значения поля должны быть уникальны."

    @pytest.mark.parametrize(
        "existing_email,new_email",
        (
            ("zhopa@zadnitsa.com", "zhopa@zadnitsa.com"),
            ("zhopa@zadnitsa.COM", "zhopa@zadnitsa.com"),
        ),
    )
    def test_cant_create_same_email(self, existing_email, new_email, anon_api_client):
        UserPublicFactory(email=existing_email)
        result = self._create_user(
            anon_api_client(),
            {**self.data, "email": new_email},
            headers={self.internal_token_header: self.internal_token},
        )
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        data = result.json()
        assert data["email"][0] == "Значения поля должны быть уникальны."

    @pytest.mark.parametrize(
        "action,initial_is_verified,post_action_is_verified",
        (
            ("verify", False, True),
            ("unverify", True, False),
        ),
    )
    def test_can_toggle_user_is_verified_using_service(
        self,
        action,
        initial_is_verified,
        post_action_is_verified,
        anon_api_client,
        settings,
    ):
        settings.INTERNAL_TOKEN_HEADER = self.internal_token_header
        settings.INTERNAL_TOKENS = [self.internal_token]

        user = UserPublicFactory(**self.data, is_verified=initial_is_verified)

        result = self._verify_unverify_user(
            action,
            anon_api_client(),
            user,
            headers={self.internal_token_header: self.internal_token},
        )
        user.refresh_from_db()
        assert result.status_code == status.HTTP_200_OK
        data = result.data
        assert data["external_user_uid"] == self.external_user_uid
        assert data["username"] == self.username
        assert data["email"] == self.email
        assert data["is_verified"] is post_action_is_verified
        assert user.is_verified is post_action_is_verified

    @pytest.mark.parametrize(
        "action,initial_is_verified", (("verify", False), ("unverify", True))
    )
    def test_cant_toggle_user_is_verified_with_wrong_header_name(
        self, action, initial_is_verified, anon_api_client, settings
    ):
        settings.INTERNAL_TOKENS = [self.internal_token]
        user = UserPublicFactory(**self.data, is_verified=initial_is_verified)
        result = self._verify_unverify_user(
            action,
            anon_api_client(),
            user,
            headers={"X-Wrong-Header": self.internal_token},
        )
        user.refresh_from_db()
        assert result.status_code == status.HTTP_401_UNAUTHORIZED
        assert user.is_verified is initial_is_verified

    @pytest.mark.parametrize(
        "action,initial_is_verified", (("verify", False), ("unverify", True))
    )
    def test_cant_toggle_is_verified_with_wrong_token(
        self, action, initial_is_verified, anon_api_client, settings
    ):
        settings.INTERNAL_TOKEN_HEADER = self.internal_token_header
        user = UserPublicFactory(**self.data, is_verified=initial_is_verified)
        result = self._verify_unverify_user(
            action,
            anon_api_client(),
            user,
            headers={self.internal_token_header: "wrong_value"},
        )
        user.refresh_from_db()
        assert result.status_code == status.HTTP_401_UNAUTHORIZED
        assert user.is_verified is initial_is_verified

    @pytest.mark.parametrize(
        "action,initial_is_verified", (("verify", False), ("unverify", True))
    )
    def test_cant_toggle_is_verified_without_headers(
        self, action, initial_is_verified, anon_api_client
    ):
        user = UserPublicFactory(**self.data, is_verified=initial_is_verified)
        result = self._verify_unverify_user(action, anon_api_client(), user)
        user.refresh_from_db()
        assert result.status_code == status.HTTP_401_UNAUTHORIZED
        assert user.is_verified is initial_is_verified

    @pytest.mark.parametrize("is_superuser", (True, False))
    @pytest.mark.parametrize("is_staff", (True, False))
    @pytest.mark.parametrize(
        "action,initial_is_verified", (("verify", False), ("unverify", True))
    )
    def test_cant_verify_as_logged_in_user(
        self, action, initial_is_verified, is_staff, is_superuser, authed_api_client
    ):
        user = UserPublicFactory(
            is_staff=is_staff,
            is_superuser=is_superuser,
            is_verified=initial_is_verified,
        )
        result = self._verify_unverify_user(action, authed_api_client(user), user)
        user.refresh_from_db()
        assert result.status_code == status.HTTP_403_FORBIDDEN
        assert user.is_verified is initial_is_verified

    def test_cant_create_with_wrong_header_name(self, anon_api_client, settings):
        settings.INTERNAL_TOKENS = [self.internal_token]
        result = self._create_user(
            anon_api_client(),
            self.data,
            headers={"X-Wrong-Header": self.internal_token},
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_cant_create_with_wrong_token(self, anon_api_client, settings):
        settings.INTERNAL_TOKEN_HEADER = self.internal_token_header
        result = self._create_user(
            anon_api_client(),
            self.data,
            headers={self.internal_token_header: "wrong_value"},
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_cant_create_without_headers(self, anon_api_client):
        result = self._create_user(anon_api_client(), self.data)
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    @pytest.mark.parametrize("is_superuser", (True, False))
    @pytest.mark.parametrize("is_staff", (True, False))
    def test_cant_create_as_logged_in_user(
        self, is_staff, is_superuser, authed_api_client
    ):
        user = UserPublicFactory(
            is_staff=is_staff,
            is_superuser=is_superuser,
        )
        result = self._create_user(authed_api_client(user), self.data)
        assert result.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize("is_verified", (True, False))
    @pytest.mark.parametrize("field_name", ("external_user_uid", "username", "email"))
    def test_cant_create_user_with_same_fields(
        self, field_name, is_verified, anon_api_client, settings
    ):
        settings.INTERNAL_TOKEN_HEADER = self.internal_token_header
        settings.INTERNAL_TOKENS = [self.internal_token]

        # Existing user
        UserPublicFactory(**self.data, is_verified=is_verified)

        # Creating fake data for a new user
        fake = faker.Faker()
        data = {
            "external_user_uid": str(uuid.uuid4()),
            "username": fake.user_name(),
            "email": fake.email(),
        }

        result = self._create_user(
            anon_api_client(),
            {
                **data,
                # replace field with data present on existing user
                field_name: self.data[field_name],
            },
            headers={self.internal_token_header: self.internal_token},
        )
        assert result.status_code == status.HTTP_400_BAD_REQUEST

    def test_can_update_own_account(self, authed_api_client):
        new_dob = "2020-01-01"
        new_bio = "new kapibio"
        gender = UserGender.MALE
        user = UserPublicFactory(gender=UserGender.NOT_SPECIFIED)

        result = self._update_user(
            authed_api_client(user),
            user,
            data={
                "date_of_birth": new_dob,
                "bio": new_bio,
                "gender": gender,
            },
        )
        user.refresh_from_db()

        data = result.data
        assert result.status_code == status.HTTP_200_OK
        assert data["date_of_birth"] == new_dob
        assert data["bio"] == new_bio
        assert data["gender"] == gender
        assert f"{user.date_of_birth:%Y-%m-%d}" == new_dob
        assert user.bio == new_bio
        assert user.gender == gender

    @pytest.mark.parametrize(
        "avatar_url,expected_status_code",
        (
            ("https://random.url", status.HTTP_400_BAD_REQUEST),
            ("", status.HTTP_200_OK),
            (
                f"{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/",
                status.HTTP_400_BAD_REQUEST,
            ),
            (
                f"{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/"
                f"img.jpg",
                status.HTTP_200_OK,
            ),
            (
                f"{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/"
                f"img.JPG",
                status.HTTP_200_OK,
            ),
            (
                f"{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/"
                f"img.jpeg",
                status.HTTP_200_OK,
            ),
            (
                f"{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/"
                f"img.JPEG",
                status.HTTP_200_OK,
            ),
            (
                f"{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/"
                f"img.png",
                status.HTTP_200_OK,
            ),
            (
                f"{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/"
                f"img.PNG",
                status.HTTP_200_OK,
            ),
            (
                f"{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/"
                f"img.gif",
                status.HTTP_200_OK,
            ),
            (
                f"{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/"
                f"img.GIF",
                status.HTTP_200_OK,
            ),
        ),
    )
    def test_updating_avatar(self, avatar_url, expected_status_code, authed_api_client):
        original_url = "https://kapi.bar/img.jpg"

        user = UserPublicFactory(avatar=original_url)
        result = self._update_user(
            authed_api_client(user), user, data={"avatar": avatar_url}
        )
        assert result.status_code == expected_status_code
        user.refresh_from_db()
        if expected_status_code == status.HTTP_200_OK:
            data = result.data
            assert user.avatar == avatar_url
            assert data["avatar"] == avatar_url
        else:
            assert user.avatar == original_url

    def test_cant_update_readonly_fields(self, authed_api_client):
        user = UserPublicFactory()

        result = self._update_user(
            authed_api_client(user),
            user,
            data={
                "rating": 1000,
                "comments_count": 100,
                "votes_up_count": 200,
                "votes_down_count": 300,
            },
        )
        user.refresh_from_db()

        data = result.data
        assert result.status_code == status.HTTP_200_OK
        assert data["rating"] == 0
        assert data["comments_count"] == 0
        assert data["votes_up_count"] == 0
        assert data["votes_down_count"] == 0

    @pytest.mark.parametrize(
        "is_authenticated,expected_status_code",
        ((True, status.HTTP_403_FORBIDDEN), (False, status.HTTP_401_UNAUTHORIZED)),
    )
    def test_cant_update_other_account(
        self, is_authenticated, expected_status_code, authed_api_client, anon_api_client
    ):
        initial_date_of_birth = "2000-01-01"
        initial_bio = "viva la kapibara"
        other_user = UserPublicFactory(
            date_of_birth=initial_date_of_birth,
            bio=initial_bio,
        )
        if is_authenticated:
            client = authed_api_client(UserPublicFactory())
        else:
            client = anon_api_client()
        result = self._update_user(
            client,
            other_user,
            data={
                "date_of_birth": "2020-01-01",
                "bio": "new kapibio",
            },
        )
        other_user.refresh_from_db()

        assert result.status_code == expected_status_code
        assert f"{other_user.date_of_birth:%Y-%m-%d}" == initial_date_of_birth
        assert other_user.bio == initial_bio

    def test_get_proper_data_in_public_profile(self, anon_api_client):
        bio = "This is my bio"
        dob = datetime.date(2020, 1, 22)
        gender = UserGender.FEMALE

        user = UserPublicFactory(
            bio=bio,
            date_of_birth=dob,
            gender=gender,
            is_verified=True,
        )
        PostFactory(user=user, status=PostStatus.DRAFT)
        PostFactory(user=user, status=PostStatus.DELETED)
        post = PostFactory(user=user, status=PostStatus.PUBLISHED)
        other_user_comment = CommentFactory(post=post)
        own_comment = CommentFactory(post=post, user=user)
        CommentVoteFactory(
            user=user, comment=other_user_comment, value=CommentVoteChoice.DOWNVOTE
        )

        CommentVoteFactory(user=user, comment=own_comment, value=CommentVoteChoice.UPVOTE)
        CommentVoteFactory.create_batch(
            size=2, comment=own_comment, value=CommentVoteChoice.DOWNVOTE
        )

        PostVoteFactory(user=user, post=post, value=PostVoteChoice.UPVOTE)
        PostVoteFactory.create_batch(size=2, post=post, value=PostVoteChoice.UPVOTE)
        PostVoteFactory(post=post, value=PostVoteChoice.DOWNVOTE)

        subscribers = UserSubscriptionFactory.create_batch(size=2, user=user)
        subscriptions = UserSubscriptionFactory.create_batch(size=3, subscriber=user)

        rating_posts = (
            Post.published.filter(user=user).annotate(r=Sum("votes__value")).first().r
        ) * settings.POST_RATING_MULTIPLIER
        rating_comments = (
            Comment.objects.filter(user_id=user).annotate(r=Sum("votes__value")).first().r
        ) * settings.COMMENT_RATING_MULTIPLIER

        result = self._get_user(anon_api_client(), user)
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        assert response["is_verified"] is True
        # Убедимся, что мы никому не показываем пользовательский день рождения
        assert "date_of_birth" not in response
        assert response["external_user_uid"] == user.external_user_uid
        assert response["posts_count"] == 1
        assert response["comments_count"] == 1
        assert response["votes_up_count"] == 2
        assert response["votes_down_count"] == 1
        assert response["rating"] == (rating_posts + rating_comments)
        assert response["gender"] == gender
        assert response["subscribers_count"] == len(subscribers)
        assert response["subscriptions_count"] == len(subscriptions)

    def test_own_user_can_only_see_fields_not_visible_to_others(self, authed_api_client):
        dob = datetime.date(2020, 1, 22)
        user = UserPublicFactory(date_of_birth=dob)
        result = self._get_user(authed_api_client(user), user)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        response = result.json()
        assert response["date_of_birth"] == dob.strftime("%Y-%m-%d")

    @pytest.mark.parametrize("username", ("normal", "With.Dot", "With_Undescrore"))
    def test_fetch_user_details_for_different_usernames_case_insensitive(
        self, username, anon_api_client
    ):
        user = UserPublicFactory(username=username)
        user.username = username.lower()
        result = self._get_user(anon_api_client(), user)
        assert result.status_code == status.HTTP_200_OK
        data = result.json()
        assert data["username"] == username

    def test_is_ignored_flag(self, authed_api_client, anon_api_client):
        me = UserPublicFactory()
        ignored_user = UserPublicFactory()
        UserIgnoredFactory(ignorer=me, user=ignored_user)

        # Проверяем от имени пользователя, кто игнорирует
        response = self._get_user(authed_api_client(me), ignored_user)
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        assert response.json()["is_ignored"] is True

        # Пользователь не игнорирует просматриваемого юзера
        response = self._get_user(authed_api_client(ignored_user), me)
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        assert response.json()["is_ignored"] is False

        # Анонимный просмотр
        response = self._get_user(anon_api_client(), me)
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        assert response.json()["is_ignored"] is False

    def test_is_subscribed_flat(self, authed_api_client, anon_api_client):
        me = UserPublicFactory()
        subscribed_user = UserPublicFactory()
        UserSubscriptionFactory(subscriber=me, user=subscribed_user)

        # Проверяем от имени пользователя, кто подписан
        response = self._get_user(authed_api_client(me), subscribed_user)
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        assert response.json()["is_subscribed"] is True
        assert response.json()["subscribe_id"] is not None

        # Пользователь не игнорирует просматриваемого юзера
        response = self._get_user(authed_api_client(subscribed_user), me)
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        assert response.json()["is_subscribed"] is False

        # Анонимный просмотр
        response = self._get_user(anon_api_client(), me)
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        assert response.json()["is_subscribed"] is False
