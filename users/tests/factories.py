import uuid

import factory
from factory.django import DjangoModelFactory


class UserPublicFactory(DjangoModelFactory):
    external_user_uid = factory.Faker("uuid4")
    username = factory.Faker("uuid4")
    email = factory.LazyAttribute(lambda _: f"{uuid.uuid4()}@kapi.bar")
    avatar = factory.LazyAttribute(lambda _: f"{uuid.uuid4()}.webp")
    is_active = True
    is_verified = True

    class Meta:
        model = "users.UserPublic"


class UserContextFactory(DjangoModelFactory):
    class Meta:
        model = "users.UserContext"
