import pytest

from comments.choices import CommentVoteChoice
from comments.tests.factories import CommentFactory, CommentVoteFactory
from posts.choices import PostStatus, PostVoteChoice
from posts.tests.factories import PostFactory, PostVoteFactory
from users.selectors import (
    get_user_rating,
    get_user_rating_for_comments,
    get_user_rating_for_posts,
)
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestUserRatingSelectors:
    def setup_method(self):
        self.user = UserPublicFactory()

    @pytest.mark.parametrize(
        "post_status,votes_up,votes_down,expected_rating",
        (
            (PostStatus.PUBLISHED, 2, 0, 2),
            (PostStatus.PUBLISHED, 1, 2, -1),
            (PostStatus.PUBLISHED, 0, 0, 0),
            (PostStatus.DELETED, 2, 0, 2),
            (PostStatus.DELETED, 1, 2, -1),
            (PostStatus.DELETED, 0, 0, 0),
            (PostStatus.DRAFT, 2, 0, 0),
            (PostStatus.DRAFT, 1, 2, 0),
            (PostStatus.DRAFT, 0, 0, 0),
        ),
    )
    def test_get_user_rating_for_posts(
        self, post_status, votes_up, votes_down, expected_rating
    ):
        assert (
            get_user_rating_for_posts(self.user) == 0
        ), "Wrong rating for posts when no posts"
        assert get_user_rating(self.user) == 0, "Wrong total rating when no posts"

        post = PostFactory(user=self.user, status=post_status)
        PostVoteFactory.create_batch(
            size=votes_up, post=post, value=PostVoteChoice.UPVOTE
        )
        PostVoteFactory.create_batch(
            size=votes_down, post=post, value=PostVoteChoice.DOWNVOTE
        )

        assert (
            get_user_rating_for_posts(self.user) == expected_rating
        ), "Wrong rating for post"
        assert get_user_rating(self.user) == expected_rating, "Wrong total rating"

    @pytest.mark.parametrize(
        "votes_up,votes_down,expected_rating",
        (
            (2, 0, 1),
            (1, 2, -0.5),
            (1, 1, 0),
            (0, 0, 0),
        ),
    )
    def test_get_user_rating_for_comments(self, votes_up, votes_down, expected_rating):
        assert (
            get_user_rating_for_comments(self.user) == 0
        ), "Wrong rating for posts when no comments"
        assert get_user_rating(self.user) == 0, "Wrong total rating when no comments"

        comment = CommentFactory(user=self.user)
        CommentVoteFactory.create_batch(
            size=votes_up, comment=comment, value=CommentVoteChoice.UPVOTE
        )
        CommentVoteFactory.create_batch(
            size=votes_down, comment=comment, value=CommentVoteChoice.DOWNVOTE
        )

        assert (
            get_user_rating_for_comments(self.user) == expected_rating
        ), "Wrong rating for comments"
        assert get_user_rating(self.user) == expected_rating, "Wrong total rating"
