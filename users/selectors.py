import datetime
from typing import Optional

from django.conf import settings
from django.db.models import Sum
from django.utils import timezone

from comments.choices import CommentVoteChoice
from posts.choices import PostStatus, PostVoteChoice
from posts.models import PostBayanVote
from users.models import UserPublic


def get_user_rating(user: UserPublic) -> float:
    return get_user_rating_for_posts(user) + get_user_rating_for_comments(user)


def get_user_rating_for_posts(user: UserPublic) -> float:
    if user.is_authenticated:
        rating = (
            user.posts.filter(status__in=(PostStatus.PUBLISHED, PostStatus.DELETED))
            .exclude(
                tags__name__in=settings.NORATING_TAG_NAMES
                # Исключаем посты с тегами из списка
            )
            .aggregate(total_rating=Sum("votes__value"))["total_rating"]
        )
        return (rating or 0) * settings.POST_RATING_MULTIPLIER
    return 0


def get_user_rating_for_comments(user: UserPublic) -> float:
    rating = user.comments.aggregate(total_rating=Sum("votes__value"))["total_rating"]
    # Если у юзера нет комментариев, тогда rating будет None, поэтому приводим его к нулю
    return (rating or 0) * settings.COMMENT_RATING_MULTIPLIER


def get_user_votes_up_count(user: UserPublic) -> int:
    return (
        user.post_votes.filter(value=PostVoteChoice.UPVOTE).count()
        + user.comment_votes.filter(value=CommentVoteChoice.UPVOTE).count()
    )


def get_user_votes_down_count(user: UserPublic) -> int:
    return (
        user.post_votes.filter(value=PostVoteChoice.DOWNVOTE).count()
        + user.comment_votes.filter(value=CommentVoteChoice.DOWNVOTE).count()
    )


def get_user_comments_count(user: UserPublic) -> int:
    return user.comments.count()


def get_user_posts_count(user: UserPublic) -> int:
    return user.posts.filter(status=PostStatus.PUBLISHED).count()


def get_user_posts_trends_count(user: UserPublic) -> int:
    return user.posts.filter(
        status=PostStatus.PUBLISHED, trending_date__isnull=False
    ).count()


def get_user_subscribers_count(user: UserPublic) -> int:
    if user.is_authenticated:
        return user.subscribers.count()
    return 0


def get_user_subscriptions_count(user: UserPublic) -> int:
    if user.is_authenticated:
        return user.user_subscriptions.count()
    return 0


def is_user_ignored_by_user(user: UserPublic, user_to_check: UserPublic) -> bool:
    if user.is_authenticated:
        return user.ignored_users.filter(user=user_to_check).exists()
    return False


def is_user_subscribed_to_user(user: UserPublic, user_to_check: UserPublic) -> bool:
    if user.is_authenticated:
        return user.user_subscriptions.filter(user=user_to_check).exists()
    return False


def get_user_subscription_uuid(
    user: UserPublic, user_to_check: UserPublic
) -> Optional[str]:
    if user.is_authenticated:
        return (
            user.user_subscriptions.filter(user=user_to_check)
            .values_list("uuid", flat=True)
            .first()
        )
    return None


def fetch_users():
    result = UserPublic.objects.filter(is_active=True)

    return result.cache()


def get_limit_timeframe_datestamp() -> datetime.datetime:
    return timezone.now() - datetime.timedelta(hours=settings.LIMIT_TIMEFRAME_HOURS)


def get_draft_posts_limit(user: UserPublic) -> int:
    return settings.LIMIT_DRAFT_POSTS


def can_create_draft_post(user: UserPublic) -> bool:
    return user.posts.filter(status=PostStatus.DRAFT).count() < get_draft_posts_limit(
        user
    )


def get_posts_limit(user: UserPublic) -> int:
    if not user.is_authenticated:
        return 0
    if user.is_banned:
        return 0
    if user.is_verified:
        return settings.LIMIT_POSTS_DEFAULT
    return settings.LIMIT_POSTS_FOR_UNVERIFIED


def can_publish_post(user: UserPublic) -> bool:
    time_since = get_limit_timeframe_datestamp()
    return user.posts.filter(
        status__in=(PostStatus.PUBLISHED, PostStatus.DELETED),
        created_at__gte=time_since,
    ).count() < get_posts_limit(user)


def get_comments_limit(user: UserPublic) -> int:
    if not user.is_authenticated:
        return 0
    if user.is_verified:
        return settings.LIMIT_COMMENTS_DEFAULT
    if user.is_banned:
        return 0
    return settings.LIMIT_COMMENTS_FOR_UNVERIFIED


def can_add_comment(user: UserPublic) -> bool:
    time_since = get_limit_timeframe_datestamp()
    return user.comments.filter(created_at__gte=time_since).count() < get_comments_limit(
        user
    )


def get_votes_limit(user: UserPublic) -> int:
    if not user.is_authenticated:
        return 0
    if user.is_banned:
        return 0
    if user.is_verified:
        return settings.LIMIT_VOTES_DEFAULT
    return settings.LIMIT_VOTES_FOR_UNVERIFIED


def get_votes_bayan_limit(user: UserPublic) -> int:
    if not user.is_authenticated:
        return 0
    if user.is_banned:
        return 0
    if user.is_verified:
        return settings.MAXIMUM_COUNT_VOTES_FOR_BAYAN_FOR_USER
    return 0


def can_vote(user: UserPublic) -> bool:
    time_since = get_limit_timeframe_datestamp()

    return (
        user.post_votes.filter(created_at__gte=time_since).count()
        + user.comment_votes.filter(created_at__gte=time_since).count()
    ) < get_votes_limit(user)


def can_vote_bayan(user: UserPublic) -> bool:
    time_since = get_limit_timeframe_datestamp()

    return (
        PostBayanVote.objects.filter(user=user, created_at__gte=time_since).count()
    ) < get_votes_bayan_limit(user)


def can_moderate_posts(user: UserPublic) -> bool:
    """Indicates whether user can perform posts moderation."""
    # TODO: add more granular permissions
    return user.is_authenticated and (user.is_staff or user.is_superuser)


def can_moderate_comments(user: UserPublic) -> bool:
    """Indicates whether user can perform comments moderation."""
    # TODO: add more granular permissions
    return user.is_authenticated and (user.is_staff or user.is_superuser)


def is_user_autobanned(user: UserPublic) -> bool:
    rating_for_autoban = settings.RATING_FOR_AUTOBAN
    if rating_for_autoban > 0:
        rating_for_autoban = rating_for_autoban * -1
    return get_user_rating(user) <= rating_for_autoban
