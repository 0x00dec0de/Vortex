# Generated by Django 5.0 on a later date

from django.db import migrations


def set_default_values(apps, schema_editor):
    UserPublic = apps.get_model("users", "UserPublic")
    UserPublic.objects.filter(show_hardcore__isnull=True).update(show_hardcore=False)
    UserPublic.objects.filter(show_nsfw__isnull=True).update(show_nsfw=False)
    UserPublic.objects.filter(show_politics__isnull=True).update(show_politics=False)
    UserPublic.objects.filter(show_viewed__isnull=True).update(show_viewed=True)


class Migration(migrations.Migration):
    dependencies = [
        ("users", "0016_userpublic_show_hardcore_userpublic_show_nsfw_and_more"),
    ]

    operations = [
        migrations.RunPython(set_default_values),
    ]
