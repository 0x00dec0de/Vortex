# Generated by Django 4.2.7 on 2023-12-04 13:09

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("users", "0013_alter_usernote_updated_at_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="userpublic",
            name="gender",
            field=models.CharField(
                choices=[("f", "Женский"), ("m", "Мужской"), ("n", "Не указан")],
                default="n",
                max_length=1,
            ),
        ),
    ]
