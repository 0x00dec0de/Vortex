# Generated by Django 5.0 on 2024-03-05 22:02

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("users", "0015_usercontext_subscriptions_feed_viewed_at"),
    ]

    operations = [
        migrations.AddField(
            model_name="userpublic",
            name="show_hardcore",
            field=models.BooleanField(null=True),
        ),
        migrations.AddField(
            model_name="userpublic",
            name="show_nsfw",
            field=models.BooleanField(null=True),
        ),
        migrations.AddField(
            model_name="userpublic",
            name="show_politics",
            field=models.BooleanField(null=True),
        ),
        migrations.AddField(
            model_name="userpublic",
            name="show_viewed",
            field=models.BooleanField(null=True),
        ),
    ]
