from django.contrib import admin

from users.models import UserPublic


@admin.register(UserPublic)
class UserPublicAdmin(admin.ModelAdmin):
    ordering = ("external_user_uid",)
    search_fields = (
        "username",
        "external_user_uid",
        "email",
    )
    list_display = (
        "__str__",
        "is_staff",
        "is_superuser",
        "is_active",
        "is_verified",
        "is_banned",
        "last_login",
    )

    list_filter = (
        "is_staff",
        "is_superuser",
        "is_active",
        "is_verified",
        "is_banned",
    )
    raw_id_fields = ("context",)
    readonly_fields = (
        "last_login",
        "external_user_uid",
        "email",
        "username",
    )
    exclude = (
        "rating",
        "comments_count",
        "votes_up_count",
        "votes_down_count",
    )
    filter_horizontal = (
        "groups",
        "user_permissions",
    )
