from django.db.models import QuerySet

from users.models import UserPublic


def fetch_user_subscriptions(user: UserPublic) -> QuerySet:
    return user.user_subscriptions.all()


def fetch_user_ignores(user: UserPublic) -> QuerySet:
    return user.ignored_users.all()


def fetch_tag_subscriptions(user: UserPublic) -> QuerySet:
    return user.tag_subscriptions.all()


def fetch_tag_ignores(user: UserPublic) -> QuerySet:
    return user.ignored_tags.all()
