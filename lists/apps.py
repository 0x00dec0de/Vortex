from django.apps import AppConfig


class ListsConfig(AppConfig):
    name = "lists"
    verbose_name = "📜 Подписки / Игноры"
