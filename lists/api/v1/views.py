from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_access_policy import AccessViewSetMixin
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from lists.api.policies import SubscriptionAccessPolicy
from lists.api.serializers import (
    CreateTagIgnoreSerializer,
    CreateTagSubscriptionSerializer,
    CreateUserIgnoreSerializer,
    CreateUserSubscriptionSerializer,
    TagIgnoreSerializer,
    TagSubscriptionSerializer,
    UserIgnoreSerializer,
    UserSubscriptionSerializer,
)
from lists.selectors import (
    fetch_tag_ignores,
    fetch_tag_subscriptions,
    fetch_user_ignores,
    fetch_user_subscriptions,
)


@extend_schema(tags=["Подписки"])
@extend_schema_view(
    list=extend_schema(summary="Вывод всех пользователей на которых подписан юзер."),
    create=extend_schema(summary="Создание подписки на пользователя."),
    destroy=extend_schema(summary="Удаление подписки на пользователя."),
)
class UserSubscriptionViewSet(
    AccessViewSetMixin,
    GenericViewSet,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
):
    pagination_class = None
    lookup_field = "uuid"
    access_policy = SubscriptionAccessPolicy

    def get_queryset(self):
        return fetch_user_subscriptions(self.request.user)

    def get_serializer_class(self):
        if self.action == "create":
            return CreateUserSubscriptionSerializer
        return UserSubscriptionSerializer


@extend_schema(tags=["Подписки"])
@extend_schema_view(
    list=extend_schema(summary="Вывод всех тегов на которых подписан юзер."),
    create=extend_schema(summary="Создание подписки на тег."),
    destroy=extend_schema(summary="Удаление подписки на тег."),
)
class TagSubscriptionViewSet(
    AccessViewSetMixin,
    GenericViewSet,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
):
    pagination_class = None
    lookup_field = "uuid"
    access_policy = SubscriptionAccessPolicy

    def get_queryset(self):
        return fetch_tag_subscriptions(self.request.user)

    def get_serializer_class(self):
        if self.action == "create":
            return CreateTagSubscriptionSerializer
        return TagSubscriptionSerializer


@extend_schema(tags=["Игнор-лист"])
@extend_schema_view(
    list=extend_schema(summary="Вывод всех игнорируемых юзеров."),
    create=extend_schema(summary="Добавление юзера в игнор-лист."),
    destroy=extend_schema(summary="Удаление юзера из игнор-листа."),
)
class UserIgnoreViewSet(
    AccessViewSetMixin,
    GenericViewSet,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
):
    pagination_class = None
    lookup_field = "uuid"
    access_policy = SubscriptionAccessPolicy

    def get_queryset(self):
        return fetch_user_ignores(self.request.user)

    def get_serializer_class(self):
        if self.action == "create":
            return CreateUserIgnoreSerializer
        return UserIgnoreSerializer


@extend_schema(tags=["Игнор-лист"])
@extend_schema_view(
    list=extend_schema(summary="Вывод всех игнорируемых тегов."),
    create=extend_schema(summary="Добавление тега в игнор-лист."),
    destroy=extend_schema(summary="Удаление тега из игнор-листа."),
)
class TagIgnoreViewSet(
    AccessViewSetMixin,
    GenericViewSet,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
):
    pagination_class = None
    lookup_field = "uuid"
    access_policy = SubscriptionAccessPolicy

    def get_queryset(self):
        return fetch_tag_ignores(self.request.user)

    def get_serializer_class(self):
        if self.action == "create":
            return CreateTagIgnoreSerializer
        return TagIgnoreSerializer
