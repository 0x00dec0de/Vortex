from rest_framework.routers import SimpleRouter

from lists.api.v1 import views

app_name = "lists"
router = SimpleRouter()

router.register(
    "user-subscriptions", views.UserSubscriptionViewSet, basename="user-subscriptions"
)
router.register("user-ignores", views.UserIgnoreViewSet, basename="user-ignores")
router.register(
    "tag-subscriptions", views.TagSubscriptionViewSet, basename="tag-subscriptions"
)
router.register("tag-ignores", views.TagIgnoreViewSet, basename="tag-ignores")

urlpatterns = router.urls
