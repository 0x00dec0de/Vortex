from rest_access_policy import AccessPolicy, Statement


class SubscriptionAccessPolicy(AccessPolicy):
    statements = [
        Statement(
            action=["*"],
            principal=["anonymous"],
            effect="deny",
        ),
        Statement(
            action=["*"],
            principal=["authenticated"],
            effect="allow",
        ),
    ]
