from django.contrib import admin

from lists.models import TagIgnored, TagSubscription, UserIgnored, UserSubscription


# Register your models here.
@admin.register(UserSubscription)
class UserSubscriptionAdmin(admin.ModelAdmin):
    list_display = (
        "subscriber",
        "user",
        "created_at",
    )
    search_fields = (
        "subscriber__email",
        "subscriber__username",
        "user__email",
        "user__username",
    )
    list_select_related = (
        "subscriber",
        "user",
    )
    raw_id_fields = (
        "subscriber",
        "user",
    )
    ordering = ("-created_at",)


@admin.register(TagSubscription)
class TagSubscriptionAdmin(admin.ModelAdmin):
    list_display = (
        "subscriber",
        "tag",
        "created_at",
    )
    search_fields = (
        "subscriber__email",
        "subscriber__username",
        "tag__name",
    )
    list_select_related = (
        "subscriber",
        "tag",
    )
    raw_id_fields = (
        "subscriber",
        "tag",
    )
    ordering = ("-created_at",)


@admin.register(UserIgnored)
class UserIgnoredAdmin(admin.ModelAdmin):
    list_display = (
        "ignorer",
        "user",
        "ignored_until",
        "created_at",
    )
    search_fields = (
        "ignorer__email",
        "ignorer__username",
        "user__email",
        "user__username",
    )
    list_select_related = (
        "ignorer",
        "user",
    )
    raw_id_fields = (
        "ignorer",
        "user",
    )
    ordering = ("-created_at",)


@admin.register(TagIgnored)
class TagIgnoredAdmin(admin.ModelAdmin):
    list_display = (
        "ignorer",
        "tag",
        "ignored_until",
        "created_at",
    )
    search_fields = (
        "ignorer__email",
        "ignorer__username",
        "tag__name",
    )
    list_select_related = (
        "ignorer",
        "tag",
    )
    raw_id_fields = (
        "ignorer",
        "tag",
    )
    ordering = ("-created_at",)
