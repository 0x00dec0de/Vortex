from rest_framework.reverse import reverse

from lists.models import TagIgnored, UserIgnored, UserSubscription
from tags.models import Tag
from users.models import UserPublic


class ListsApiActionsMixin:
    def _create_user_subscription(self, client, user: UserPublic):
        return client.post(
            reverse("v1:lists:user-subscriptions-list"),
            data={"username": user.username.upper()},
        )

    def _create_user_ignore(self, client, user: UserPublic):
        return client.post(
            reverse("v1:lists:user-ignores-list"),
            data={"username": user.username.upper()},
        )

    def _get_user_subscriptions(self, client):
        return client.get(reverse("v1:lists:user-subscriptions-list"))

    def _get_user_ignores(self, client):
        return client.get(reverse("v1:lists:user-ignores-list"))

    def _delete_user_subscription(self, client, user_subscription: UserSubscription):
        return client.delete(
            reverse(
                "v1:lists:user-subscriptions-detail",
                kwargs={"uuid": user_subscription.uuid},
            )
        )

    def _delete_user_ignore(self, client, user_ignored: UserIgnored):
        return client.delete(
            reverse(
                "v1:lists:user-ignores-detail",
                kwargs={"uuid": user_ignored.uuid},
            )
        )

    def _create_tag_subscription(self, client, tag: Tag):
        return client.post(
            reverse("v1:lists:tag-subscriptions-list"), data={"tag": tag.name}
        )

    def _create_tag_ignore(self, client, tag: Tag):
        return client.post(reverse("v1:lists:tag-ignores-list"), data={"tag": tag.name})

    def _get_tag_subscriptions(self, client):
        return client.get(reverse("v1:lists:tag-subscriptions-list"))

    def _get_tag_ignoress(self, client):
        return client.get(reverse("v1:lists:tag-ignores-list"))

    def _delete_tag_subscription(self, client, user_subscription: UserSubscription):
        return client.delete(
            reverse(
                "v1:lists:tag-subscriptions-detail",
                kwargs={"uuid": user_subscription.uuid},
            )
        )

    def _delete_tag_ignore(self, client, tag_ignored: TagIgnored):
        return client.delete(
            reverse(
                "v1:lists:tag-ignores-detail",
                kwargs={"uuid": tag_ignored.uuid},
            )
        )
