import pytest
from funcy import lpluck, lpluck_attr
from rest_framework import status

from lists.models import UserIgnored
from lists.tests.factories import UserIgnoredFactory
from lists.tests.mixins import ListsApiActionsMixin
from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestUserIgnores(ListsApiActionsMixin, PostsApiActionsMixin):
    def setup_method(self):
        self.ignorer, self.user_to_ignore = UserPublicFactory.create_batch(size=2)

    def test_user_ignore_another_user(self, authed_api_client):
        response = self._create_user_ignore(
            authed_api_client(self.ignorer), user=self.user_to_ignore
        )
        assert response.status_code == status.HTTP_201_CREATED, response.content.decode()
        result = response.json()

        db_user_ignores = UserIgnored.objects.filter(
            ignorer=self.ignorer, user=self.user_to_ignore
        )
        db_user_ignore = db_user_ignores.first()
        assert db_user_ignores.count() == 1

        assert result["uuid"] == str(db_user_ignore.uuid)
        assert result["user"]["username"] == self.user_to_ignore.username
        assert result["user"]["avatar"] == self.user_to_ignore.avatar

    def test_user_ignore_the_same_user(self, authed_api_client):
        db_ignore = UserIgnoredFactory(ignorer=self.ignorer, user=self.user_to_ignore)
        response = self._create_user_ignore(
            authed_api_client(self.ignorer), user=self.user_to_ignore
        )
        assert response.status_code == status.HTTP_201_CREATED, response.content.decode()
        result = response.json()

        assert result["uuid"] == str(db_ignore.uuid)
        assert result["user"]["username"] == self.user_to_ignore.username
        assert result["user"]["avatar"] == self.user_to_ignore.avatar

    def test_user_cannot_ignore_themselves(self, authed_api_client):
        response = self._create_user_ignore(
            authed_api_client(self.ignorer), user=self.ignorer
        )
        assert (
            response.status_code == status.HTTP_400_BAD_REQUEST
        ), response.content.decode()
        result = response.json()
        assert result["detail"][0]["msg"] == "Нельзя добавить в игнор свой аккаунт"
        assert result["detail"][0]["type"] == "cannot_ignore_themselves"

    def test_user_cannot_ignore_non_existing_user(self, authed_api_client):
        response = self._create_user_ignore(
            authed_api_client(self.ignorer), user=UserPublicFactory.build()
        )
        assert (
            response.status_code == status.HTTP_400_BAD_REQUEST
        ), response.content.decode()

    def test_anon_user_cannot_ignore(self, anon_api_client):
        response = self._create_user_ignore(anon_api_client(), user=self.user_to_ignore)
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
        ), response.content.decode()

    def test_user_can_list_own_user_ignores(self, authed_api_client):
        ignores = UserIgnoredFactory.create_batch(size=3, ignorer=self.ignorer)
        response = self._get_user_ignores(authed_api_client(self.ignorer))
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        result = response.json()

        sorted_ignores = sorted(ignores, key=lambda s: s.user.username)
        sorted_ignores_from_api = sorted(result, key=lambda s: s["user"]["username"])

        assert list(map(str, lpluck_attr("uuid", sorted_ignores))) == lpluck(
            "uuid", sorted_ignores_from_api
        )

    def test_anon_user_cannot_list_user_ignores(self, anon_api_client):
        response = self._get_user_ignores(anon_api_client())
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
        ), response.content.decode()

    def test_user_can_delete_user_ignore(self, authed_api_client):
        user_ignore = UserIgnoredFactory(ignorer=self.ignorer, user=self.user_to_ignore)
        response = self._delete_user_ignore(authed_api_client(self.ignorer), user_ignore)
        assert (
            response.status_code == status.HTTP_204_NO_CONTENT
        ), response.content.decode()
        assert not UserIgnored.objects.filter(
            ignorer=self.ignorer, user=self.user_to_ignore
        ).exists()

    def test_user_cant_delete_other_user_ignore(self, authed_api_client):
        user_ignore = UserIgnoredFactory(user=self.user_to_ignore)
        response = self._delete_user_ignore(authed_api_client(self.ignorer), user_ignore)
        assert (
            response.status_code == status.HTTP_404_NOT_FOUND
        ), response.content.decode()

    def test_user_cannot_see_posts_from_ignored_users_only(self, authed_api_client):
        user_ignore = UserIgnoredFactory(ignorer=self.ignorer, user=self.user_to_ignore)
        PostFactory(user=user_ignore.user, status=PostStatus.PUBLISHED)
        posts_from_ignored_user = PostFactory.create_batch(
            size=2, user=self.user_to_ignore, status=PostStatus.PUBLISHED
        )

        ignored_posts_uuid = set(map(str, lpluck_attr("uuid", posts_from_ignored_user)))

        response = self._fetch_posts_feed(authed_api_client(self.ignorer))
        result = response.json()
        assert response.status_code == status.HTTP_200_OK, response.content.decode()

        posts_uuid_from_api = set(lpluck("uuid", result["results"]))

        # Проверка, что в ленте постов нет постов от игнорируемого юзера
        assert ignored_posts_uuid - posts_uuid_from_api == ignored_posts_uuid
