import pytest
from funcy import lpluck, lpluck_attr
from rest_framework import status

from lists.models import TagIgnored
from lists.tests.factories import TagIgnoredFactory
from lists.tests.mixins import ListsApiActionsMixin
from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin
from tags.tests.factories import TagFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestTagIgnores(ListsApiActionsMixin, PostsApiActionsMixin):
    def setup_method(self):
        self.ignorer = UserPublicFactory()
        self.tag_to_ignore = TagFactory()

    def test_user_ignore_tag(self, authed_api_client):
        response = self._create_tag_ignore(
            authed_api_client(self.ignorer), tag=self.tag_to_ignore
        )
        assert response.status_code == status.HTTP_201_CREATED, response.content.decode()
        result = response.json()

        db_ignores = TagIgnored.objects.filter(
            ignorer=self.ignorer, tag=self.tag_to_ignore
        )
        db_ignore = db_ignores.first()
        assert db_ignores.count() == 1

        assert result["uuid"] == str(db_ignore.uuid)
        assert result["tag"] == self.tag_to_ignore.name

    def test_user_ignore_the_same_tag(self, authed_api_client):
        db_ignore = TagIgnoredFactory(ignorer=self.ignorer, tag=self.tag_to_ignore)
        response = self._create_tag_ignore(
            authed_api_client(self.ignorer), tag=self.tag_to_ignore
        )
        assert response.status_code == status.HTTP_201_CREATED, response.content.decode()
        result = response.json()

        assert result["uuid"] == str(db_ignore.uuid)
        assert result["tag"] == self.tag_to_ignore.name

    def test_user_cannot_ignore_non_existing_tag(self, authed_api_client):
        response = self._create_tag_ignore(
            authed_api_client(self.ignorer),
            tag=TagFactory.build(name="non-existing-tag"),
        )
        assert (
            response.status_code == status.HTTP_400_BAD_REQUEST
        ), response.content.decode()

    def test_anon_user_cannot_ignore_tag(self, anon_api_client):
        response = self._create_tag_ignore(anon_api_client(), tag=self.tag_to_ignore)
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
        ), response.content.decode()

    def test_user_can_list_own_tag_ignores(self, authed_api_client):
        ignores = TagIgnoredFactory.create_batch(size=3, ignorer=self.ignorer)
        response = self._get_tag_ignoress(authed_api_client(self.ignorer))
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        result = response.json()

        sorted_ignores = sorted(ignores, key=lambda s: s.tag.name)
        sorted_ignores_from_api = sorted(result, key=lambda s: s["tag"])

        assert list(map(str, lpluck_attr("uuid", sorted_ignores))) == lpluck(
            "uuid", sorted_ignores_from_api
        )

    def test_anon_user_cannot_list_tag_ignores(self, anon_api_client):
        response = self._get_tag_ignoress(anon_api_client())
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
        ), response.content.decode()

    def test_user_can_delete_ignored_tag(self, authed_api_client):
        tag_ignore = TagIgnoredFactory(ignorer=self.ignorer, tag=self.tag_to_ignore)
        response = self._delete_tag_ignore(authed_api_client(self.ignorer), tag_ignore)
        assert (
            response.status_code == status.HTTP_204_NO_CONTENT
        ), response.content.decode()
        assert not TagIgnored.objects.filter(
            ignorer=self.ignorer, tag=self.tag_to_ignore
        ).exists()

    def test_user_cant_delete_other_tag_ignore(self, authed_api_client):
        subscription = TagIgnoredFactory(tag=self.tag_to_ignore)
        response = self._delete_tag_ignore(authed_api_client(self.ignorer), subscription)
        assert (
            response.status_code == status.HTTP_404_NOT_FOUND
        ), response.content.decode()

    def test_user_can_see_posts_from_subscribed_tags_only(self, authed_api_client):
        subscription = TagIgnoredFactory(ignorer=self.ignorer, tag=self.tag_to_ignore)
        posts_with_ignored_tags = PostFactory.create_batch(
            size=2, tags=[subscription.tag], status=PostStatus.PUBLISHED
        )
        PostFactory.create_batch(size=2, status=PostStatus.PUBLISHED)

        ignored_posts_uuid = set(map(str, lpluck_attr("uuid", posts_with_ignored_tags)))

        response = self._fetch_posts_feed(authed_api_client(self.ignorer))
        result = response.json()
        assert response.status_code == status.HTTP_200_OK, response.content.decode()

        posts_uuid_from_api = set(lpluck("uuid", result["results"]))
        # Проверка, что в ленте постов нет постов с игнорируемыми тегами
        assert ignored_posts_uuid - posts_uuid_from_api == ignored_posts_uuid
