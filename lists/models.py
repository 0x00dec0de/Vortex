import uuid

from django.db import models
from django.db.models import UniqueConstraint

from common.models import Timestamped


class UserSubscription(Timestamped):
    uuid = models.UUIDField(
        default=uuid.uuid4, unique=True, db_index=True, editable=False
    )
    subscriber = models.ForeignKey(
        "users.UserPublic",
        verbose_name="Подписчик",
        on_delete=models.CASCADE,
        related_name="user_subscriptions",
    )
    user = models.ForeignKey(
        "users.UserPublic",
        verbose_name="Подписан на",
        on_delete=models.CASCADE,
        related_name="subscribers",
    )

    class Meta:
        verbose_name = "Подписка на пользователя"
        verbose_name_plural = "Подписки на пользователей"
        constraints = [
            UniqueConstraint(fields=("subscriber", "user"), name="subscriber_user"),
        ]

    def __str__(self):
        return f"{self.subscriber} ❤️ {self.user}"


class TagSubscription(Timestamped):
    uuid = models.UUIDField(
        default=uuid.uuid4, unique=True, db_index=True, editable=False
    )
    subscriber = models.ForeignKey(
        "users.UserPublic",
        verbose_name="Подписчик",
        on_delete=models.CASCADE,
        related_name="tag_subscriptions",
    )
    tag = models.ForeignKey(
        "tags.Tag",
        verbose_name="Подписан на",
        on_delete=models.CASCADE,
        related_name="subscribers",
    )

    class Meta:
        verbose_name = "Подписка на тег"
        verbose_name_plural = "Подписки на теги"
        constraints = [
            UniqueConstraint(fields=("subscriber", "tag"), name="subscriber_tag"),
        ]

    def __str__(self):
        return f"{self.subscriber} ❤️ {self.tag}"


class UserIgnored(Timestamped):
    uuid = models.UUIDField(
        default=uuid.uuid4, unique=True, db_index=True, editable=False
    )
    ignorer = models.ForeignKey(
        "users.UserPublic",
        verbose_name="Игнорирует",
        on_delete=models.CASCADE,
        related_name="ignored_users",
    )
    user = models.ForeignKey(
        "users.UserPublic",
        verbose_name="Игнорируемый",
        on_delete=models.CASCADE,
        related_name="ignorers",
    )
    ignored_until = models.DateField(
        verbose_name="Игнорирует до", blank=True, null=True, default=None
    )

    class Meta:
        verbose_name = "Игнор юзера"
        verbose_name_plural = "Игноры юзеров"
        constraints = [
            UniqueConstraint(fields=("ignorer", "user"), name="ignorer_user"),
        ]

    def __str__(self):
        return f"{self.ignorer} ❌️{self.user}"


class TagIgnored(Timestamped):
    uuid = models.UUIDField(
        default=uuid.uuid4, unique=True, db_index=True, editable=False
    )
    ignorer = models.ForeignKey(
        "users.UserPublic",
        verbose_name="Игнорирует",
        on_delete=models.CASCADE,
        related_name="ignored_tags",
    )
    tag = models.ForeignKey(
        "tags.Tag",
        verbose_name="Игнорируемый",
        on_delete=models.CASCADE,
        related_name="ignorers",
    )
    ignored_until = models.DateField(
        verbose_name="Игнорирует до", blank=True, null=True, default=None
    )

    class Meta:
        verbose_name = "Игнор тега"
        verbose_name_plural = "Игноры тегов"
        constraints = [
            UniqueConstraint(fields=("ignorer", "tag"), name="ignorer_tag"),
        ]

    def __str__(self):
        return f"{self.ignorer} ❌️{self.tag}"
