from rest_framework.routers import SimpleRouter

from tags.api.v1 import views

app_name = "tags"
router = SimpleRouter()

router.register("", views.TagsViewSet, basename="tags")

urlpatterns = router.urls
