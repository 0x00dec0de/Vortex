from rest_framework import mixins
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import GenericViewSet

from tags.api.filters import TagsFilter
from tags.api.serializers import TagSerializer
from tags.models import Tag


class TagsViewSet(GenericViewSet, mixins.ListModelMixin):
    """API для вывода тегов."""

    serializer_class = TagSerializer
    queryset = Tag.objects.none()
    pagination_class = None
    filterset_class = TagsFilter
    permission_classes = [AllowAny]
    throttle_scope = "tag-search"
