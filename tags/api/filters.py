from django_filters import rest_framework as filters

from tags.selectors import get_popular_used_tags, get_tags_for_autosuggestion


class TagsFilter(filters.FilterSet):
    s = filters.CharFilter(method="filter_by_name")  # noqa: VNE001
    popular = filters.BooleanFilter(method="filter_by_popular")

    def filter_by_name(self, queryset, name, value):
        return get_tags_for_autosuggestion(self.request.user, value)

    def filter_by_popular(self, queryset, name, value):
        return get_popular_used_tags(self.request.user)
