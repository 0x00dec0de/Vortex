import pytest
from funcy import lpluck
from rest_framework import status
from rest_framework.reverse import reverse

from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from tags.tests.factories import TagFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestPopularTags:
    def test_popular_tags(self, authed_api_client):
        tag_first = TagFactory(name="bbb")
        tag_second = TagFactory(name="ccc")
        tag_third = TagFactory(name="aaa")
        tag_fourth = TagFactory(name="eee")
        _, published_post_2 = PostFactory.create_batch(
            size=2, status=PostStatus.PUBLISHED, tags=[tag_second, tag_first]
        )
        published_post_2.tags.add(tag_third)

        # Теги из черновиков и удаленных постов не должны показываться
        PostFactory(status=PostStatus.DRAFT, tags=[tag_second, tag_third, tag_fourth])
        PostFactory(status=PostStatus.DELETED, tags=[tag_second, tag_third, tag_fourth])

        response = self._get_popular_tags(authed_api_client(UserPublicFactory()))

        assert response.status_code == status.HTTP_200_OK

        assert list(lpluck("name", response.json())) == [
            tag_first.name,
            tag_second.name,
            tag_third.name,
        ]

    def _get_popular_tags(self, client):
        return client.get(reverse("v1:tags:tags-list") + "?popular=1")
