import os

from celery import Celery

# from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core_app.settings")

app = Celery("kapibara")

app.config_from_object("django.conf:settings", namespace="CELERY")

app.autodiscover_tasks()

# app.conf.beat_schedule = {
#     "update_sitemap-every-day": {
#         "task": "common.tasks.update_sitemap",
#         # 'schedule': crontab(hour='0', minute='0'),  # Каждый день в полночь
#         "schedule": crontab(minute="*/5"),  # DEBUG Каждые 5 минут
#     },
# }
