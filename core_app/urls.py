from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from two_factor.urls import urlpatterns as tf_urls

from common.api.views import GetPreviewPageView
from users.api.v1.views import UserContextView, UserSearchViewSet

v1_urls = [
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="v1:schema"),
        name="swagger-ui",
    ),
    path(
        "schema/redoc/",
        SpectacularRedocView.as_view(url_name="v1:schema"),
        name="redoc",
    ),
    path("comments/", include("comments.api.v1.urls", namespace="comments")),
    path("posts/", include("posts.api.v1.urls", namespace="posts")),
    path("users/", include("users.api.v1.urls", namespace="users")),
    path("notifications/", include("notification.api.v1.urls", namespace="notification")),
    path("user-context/", UserContextView.as_view(), name="user-context"),
    path("user-search/", UserSearchViewSet.as_view({"get": "list"}), name="user-search"),
    path("tags/", include("tags.api.v1.urls", namespace="tags")),
    path("uploads/", include("uploads.api.v1.urls", namespace="uploads")),
    path("lists/", include("lists.api.v1.urls", namespace="lists")),
    path("sitemap/", include("common.api.urls", namespace="common")),
    path("preview/", GetPreviewPageView.as_view(), name="preview"),
    path("achievements/", include("achievements.api.v1.urls")),
]

urlpatterns = [
    path("", include(tf_urls)),
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
    path(f"{settings.DJANGO_ADMIN_PATH}/", admin.site.urls),
    path("v1/", include((v1_urls, "v1"), namespace="v1")),
]
