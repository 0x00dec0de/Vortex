from django.contrib.admin import AdminSite
from two_factor.admin import AdminSiteOTPRequired


class KapibaraOTPAdmin(AdminSiteOTPRequired):
    site_header = "КапиБар"


class KapibaraAdmin(AdminSite):
    site_header = "КапиБар"
