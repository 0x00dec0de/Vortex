from django.urls import include, path
from rest_framework.routers import SimpleRouter

from notification.api.v1.views import NotificationViewSet

from .views import UnsubscribeView

app_name = "users"

router = SimpleRouter()
router.register("", NotificationViewSet, basename="notify")

urlpatterns = [
    path("", include(router.urls)),  # Включает все API URL из маршрутизатора
    # path('unsubscribe/', unsubscribe, name='unsubscribe')  # Добавляет URL для отписки
    path("unsubscribe/", UnsubscribeView.as_view(), name="unsubscribe"),
]
