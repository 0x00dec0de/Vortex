from django.db.models import Q
from django.utils import timezone
from drf_spectacular.utils import (
    OpenApiParameter,
    OpenApiTypes,
    extend_schema,
    extend_schema_view,
)
from rest_framework import mixins, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from comments.services import update_notifications_viewed_at
from common.api.parameters import EMAIL
from notification.api.serializers import UserNotificationSerializer
from notification.api.v1.filters import UserNotificationFilter
from notification.forms import UnsubscribeForm
from notification.models import Unsubscribe, UserNotification
from users.models import UserPublic

from .serializers import (  # Импортируйте или создайте соответствующий сериализатор
    SimpleMessageSerializer,
)


class NotifyPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 100


@extend_schema(
    tags=["Уведомления"],
    parameters=[
        OpenApiParameter(
            name="page_size",
            description="Количество записей на странице",
            required=False,
            type=OpenApiTypes.INT,
            location=OpenApiParameter.QUERY,
        ),
        OpenApiParameter(
            name="page",
            description="Номер страницы",
            required=False,
            type=OpenApiTypes.INT,
            location=OpenApiParameter.QUERY,
        ),
    ],
    responses={status.HTTP_200_OK: UserNotificationSerializer},
)
@extend_schema_view(
    list=extend_schema(summary="Вывод всех уведомления для юзера."),
)
class NotificationViewSet(GenericViewSet, mixins.ListModelMixin):
    """API для поиска пользователей для призыва в комментах/постах."""

    permission_classes = [IsAuthenticated]
    serializer_class = UserNotificationSerializer
    filterset_class = UserNotificationFilter
    search_fields = ("username",)
    pagination_class = NotifyPagination

    def get_permissions(self):
        """
        Получить список разрешений, которые будет применять этот view.
        """
        if self.action == "list" and not self.request.user.is_authenticated:
            return [AllowAny()]
        return [permission() for permission in self.permission_classes]

    def get_queryset(self):
        """
        Возвращает уведомления для авторизованного пользователя или
        уведомления из определенных категорий без авторизации.
        """
        queryset = UserNotification.objects.all().order_by("-created_at")

        if self.request.user.is_authenticated:
            base_filter = Q(user=self.request.user) | Q(all_users=True)

            if self.request.user.is_staff or self.request.user.is_superuser:
                base_filter |= Q(for_moderators=True)

            queryset = queryset.filter(base_filter)
            update_notifications_viewed_at(self.request.user)
        else:
            allowed_categories = ["invite", "update", "events"]
            queryset = queryset.filter(
                Q(category__in=allowed_categories) & Q(all_users=True)
            )

        return queryset


class UnsubscribeView(APIView):
    @extend_schema(
        tags=["Отписка"],
        parameters=[EMAIL],
        responses={status.HTTP_200_OK: SimpleMessageSerializer},
        description="API для обработки отписки пользователем от рассылок по email",
        summary="Отписка от рассылок по email",
        methods=["POST"],
    )
    def post(self, request, *args, **kwargs):
        form = UnsubscribeForm(request.query_params)
        if form.is_valid():
            email = form.cleaned_data["email"]
            user = UserPublic.objects.filter(email=email).first()
            if not user:
                return Response(
                    {"message": "Пользователя с таким email не существует"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            Unsubscribe.objects.create(user=user, unsubscribed_at=timezone.now())
            return Response(
                {"message": "Unsubscribed successfully"}, status=status.HTTP_200_OK
            )
        else:
            return Response(form.errors, status=status.HTTP_400_BAD_REQUEST)


# def unsubscribe(request):
#     if request.method == 'POST':
#         form = UnsubscribeForm(request.POST)
#         if form.is_valid():
#             email = form.cleaned_data['email']
#             user = UserPublic.objects.get(email=email)
#             try:
#                 Unsubscribe.objects.create(user=user, unsubscribed_at=timezone.now())
#                 return redirect('unsubscribe_success')
#             except user.DoesNotExist:
#                 form.add_error('email', 'No user found with this email address')
#     else:
#         form = UnsubscribeForm()
#
#     return render(request, 'unsubscribe.html', {'form': form})
