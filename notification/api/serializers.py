from rest_framework import serializers

from notification.models import UserNotification


class UserNotificationSerializer(serializers.ModelSerializer):
    user_username = serializers.CharField(source="user.username", read_only=True)
    user_external_uid = serializers.CharField(
        source="user.external_user_uid", read_only=True
    )
    post_slug = serializers.SlugRelatedField(
        source="post", slug_field="slug", read_only=True
    )
    post_title = serializers.CharField(source="post.title", read_only=True)
    comment_uuid = serializers.SlugRelatedField(
        source="comment", slug_field="uuid", read_only=True
    )

    class Meta:
        model = UserNotification
        fields = (
            "id",
            "user_username",
            "user_external_uid",
            "message",
            "category",
            "created_at",
            "viewed_at",
            "post_slug",
            "post_title",
            "comment_uuid",
        )
        read_only_fields = (
            "id",
            "user_username",
            "user_external_uid",
            "message",
            "category",
            "created_at",
            "post_slug",
            "post_title",
            "comment_uuid",
        )
