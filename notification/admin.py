from django.conf import settings
from django.contrib import admin, messages

from notification.models import Unsubscribe, UserNotification

from .choices import EmailSendingStatus
from .forms import EmailForm
from .models import EmailSendingRecord, EmailTemplate
from .utils import send_email_sync


@admin.register(UserNotification)
class UserNotificationAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "message",
        "notification_type",
        "all_users",
        "for_moderators",
        "category",
        "created_at",
        "viewed_at",
        # "post",
        # "comment",
    )
    list_select_related = (
        "user",
        # "post",
        # "comment",
    )
    ordering = ("created_at",)
    search_fields = (
        "user__username",
        "post__title",
        "post__slug",
        "comment__uuid",
    )
    list_filter = (
        "category",
        "all_users",
        "for_moderators",
    )
    # autocomplete_fields = ["user"]
    raw_id_fields = ["user", "post", "comment"]
    list_per_page = 50  # Устанавливаем пагинацию для уменьшения нагрузки


class EmailAdmin(admin.ModelAdmin):
    list_display = ["name_sending", "template", "date_sent", "status", "error_message"]
    list_filter = ["name_sending", "template", "status", "date_sent"]
    search_fields = ["name_sending"]
    form = EmailForm
    autocomplete_fields = ["recipients"]
    actions = ["send_emails"]

    def send_emails(self, request, queryset):
        batch_size = 15
        sent_count = 0

        for email_record in queryset[:batch_size]:
            sent_status = self.send_single_pack_email(email_record)
            if sent_status:
                sent_count += 1

        self.message_user(
            request, f"Successfully sent {sent_count} emails.", messages.SUCCESS
        )

    @staticmethod
    def send_single_pack_email(email_record):
        template = email_record.template
        subject = template.subject
        message_html = template.message_html

        try:
            emails_failed = send_email_sync(
                sender=settings.DEFAULT_FROM_EMAIL,
                user_to=[user for user in email_record.recipients.all()],
                subject=subject,
                message="This is a plain text version of the message",
                html_message=message_html,
            )
            if not emails_failed:
                email_record.status = EmailSendingStatus.DELIVERED
            else:
                email_record.status = EmailSendingStatus.PARTIAL_DELIVERED
                email_record.error_message = f"Not sent: {', '.join(emails_failed)}"

        except Exception as e:
            email_record.status = EmailSendingStatus.FAILED
            email_record.error_message = (
                str(e)
                if not email_record.error_message
                else email_record.error_message + "; " + str(e)
            )

        email_record.save()
        return email_record.status == EmailSendingStatus.DELIVERED


class UnsubscribeAdmin(admin.ModelAdmin):
    list_display = [
        "user",
        "unsubscribed_at",
    ]
    list_filter = [
        "user",
        "unsubscribed_at",
    ]
    search_fields = [
        "user",
        "unsubscribed_at",
    ]


admin.site.register(EmailTemplate)
admin.site.register(EmailSendingRecord, EmailAdmin)
admin.site.register(Unsubscribe, UnsubscribeAdmin)
