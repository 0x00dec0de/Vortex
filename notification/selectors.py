from notification.models import UserNotification
from users.models import UserPublic


def get_all_notify(user: UserPublic) -> UserNotification:
    """Просто возвращает все нотификации пользователя."""
    return UserNotification.objects.filter(user=user).all()


def get_count_of_new_notifications(user: UserPublic) -> int:
    try:
        notify_list_viewed_at = user.context.notify_viewed_at
    except AttributeError:
        notify_list_viewed_at = None

    count = 0
    if notify_list_viewed_at:
        count = (
            UserNotification.objects.filter(
                user=user, created_at__gt=notify_list_viewed_at
            )
            .distinct()
            .count()
        )

    return count
