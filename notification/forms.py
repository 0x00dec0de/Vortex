from django import forms
from django.forms import TextInput

from .models import EmailSendingRecord


class EmailForm(forms.ModelForm):
    class Meta:
        model = EmailSendingRecord
        fields = ["name_sending", "template", "recipients", "cc", "bcc"]
        widgets = {
            "name_sending": TextInput(attrs={"size": "40"}),
            "cc": TextInput(attrs={"size": "40"}),
            "bcc": TextInput(attrs={"size": "40"}),
        }


class UnsubscribeForm(forms.Form):
    email = forms.EmailField(label="Enter your email to unsubscribe")
