from django.db import models
from django.db.models import CheckConstraint, Q

from notification.choices import EmailSendingStatus
from notification.constants import TYPE_CHOICES, NotificationCategories
from users.models import UserPublic


class UserNotification(models.Model):
    user = models.ForeignKey(
        "users.UserPublic",
        on_delete=models.CASCADE,
        related_name="recipient",
        null=True,
        blank=True,
    )
    all_users = models.BooleanField(default=False)
    for_moderators = models.BooleanField(default=False)
    message = models.TextField()
    notification_type = models.CharField(max_length=10, choices=TYPE_CHOICES)
    category = models.CharField(max_length=20, choices=NotificationCategories.choices)
    created_at = models.DateTimeField(auto_now_add=True)
    viewed_at = models.DateTimeField(null=True, blank=True)
    post = models.ForeignKey(
        "posts.Post",
        on_delete=models.CASCADE,
        related_name="affected_post",
        null=True,
        blank=True,
    )
    comment = models.ForeignKey(
        "comments.Comment",
        on_delete=models.CASCADE,
        related_name="affected_comment",
        null=True,
        blank=True,
    )

    class Meta:
        constraints = [
            CheckConstraint(
                check=Q(
                    ~Q(category__in=["mentions", "user_events"]) | Q(user__isnull=False)
                ),
                name="user_required_for_mentions_and_user_events",
            )
        ]


class EmailTemplate(models.Model):
    name = models.CharField(max_length=255)
    subject = models.CharField(max_length=255)
    message_html = models.TextField()

    def __str__(self):
        return self.name


class EmailSendingRecord(models.Model):
    name_sending = models.TextField(max_length=30, blank=True, null=True)
    template = models.ForeignKey(EmailTemplate, on_delete=models.CASCADE)
    recipients = models.ManyToManyField(UserPublic, related_name="email_records")
    cc = models.TextField(blank=True, null=True)
    bcc = models.TextField(blank=True, null=True)
    date_sent = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20, choices=EmailSendingStatus.choices)
    error_message = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name_sending or "Unnamed Email Sending Record"


class Unsubscribe(models.Model):
    user = models.ForeignKey(
        "users.UserPublic",
        on_delete=models.CASCADE,
        related_name="unsubscribed",
        null=False,
        blank=False,
    )
    unsubscribed_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f"{self.user.username} unsubscribe request"
