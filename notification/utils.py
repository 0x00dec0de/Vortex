import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.conf import settings

logger = logging.getLogger(__name__)


def send_email_sync(
    sender, user_to, subject, message, html_message=None, cc=None
) -> list[str]:
    """Send email using SMTP in a synchronous manner."""
    emails = []
    for recipient in user_to:
        email = MIMEMultipart("alternative")
        email["Subject"] = subject
        email["From"] = sender
        email["To"] = str(recipient.email)
        if cc:
            email["Cc"] = ",".join(cc)

        email.attach(MIMEText(message, "plain"))

        if html_message:
            # Replace %%username%% and %%link_for_unsubscribe%% in html_message
            personalized_html_message = html_message.replace(
                "%%username%%", recipient.username
            )
            unsubscribe_url = (
                f"http://{settings.MONOLITH_URL}/v1/notifications/"
                f"unsubscribe/?email={recipient.email}"
            )
            personalized_html_message = personalized_html_message.replace(
                "%%link_for_unsubscribe%%", unsubscribe_url
            )

            email.attach(MIMEText(personalized_html_message, "html"))

        emails.append(email)

    success = []
    with smtplib.SMTP(settings.SMTP_HOST, settings.SMTP_PORT) as smtp:
        smtp.starttls()
        smtp.login(settings.SMTP_USERNAME, settings.SMTP_PASSWORD)
        for email in emails:
            try:
                smtp.sendmail(sender, email["To"], email.as_string())
                success.append(email["To"])
            except Exception as ex:
                logger.error(f"Failed to send email to {email['To']}: {ex}")

    return success
